<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| User Account Language File
	|--------------------------------------------------------------------------
	|
	| These language lines apply to user account management.
	|
	*/

	"header-register"	=> "Or register a new Megamall account",
	"header-login"		=> "Or log in to your Megamall account",
	"header-social"		=> "Sign in automatically using any of the following services",
	"header-forgotpass"	=> "Enter the email address associated with your account:",
	"header-resetpass"	=> "Reset your password",
	"forgot-pw-link"	=> "Forgot your password?",
	"username"			=> "Username",
	"firstname"			=> "First name",
	"lastname"			=> "Last name",
	"fullname"			=> "Full name",
	"email"				=> "E-mail address",
	"password"			=> "Password",
	"confirm-password"	=> "Confirm password",
	"repeat-password"	=> "Repeat password",
	"user-level"		=> "User Level",
	"level-admin"		=> "Administrator",
	"level-user"		=> "User",
	"telephone"			=> "Phone",
	"fax"				=> "Fax",
	"company"			=> "Company name",
	"company-id"		=> "Company ID",
	"tax_id"			=> "Tax ID",
	"address"			=> "Address",
	"address-1"			=> "Address line 1",
	"address-2"			=> "Address line 2",
	"city"				=> "City",
	"postcode"			=> "Postal code",
	"country"			=> "Country",
	"zone"				=> "Zone",
	"signup-accept"		=> "By creating an account, you accept Megamall.ph's :terms and :privacy.",
	"signup-terms"		=> "Terms and Conditions",
	"signup-privacy"	=> "Privacy Policy",
	"password-recovery"	=> "Password Recovery",
	"reset-password"	=> "Reset Password",
	"my-profile"		=> "My Profile",
	"profile-of"		=> "Profile of :name",
	"no-address"		=> "No address specified",
	"no-telephone"		=> "No phone number listed",
	"report-user"		=> "Report this user",
	"chat-on-yahoo"		=> "Chat on Yahoo! Messenger (:id)",
	"call-on-skype"		=> "Call on Skype (:id)",
	"delete-address-confirm" => "Are you sure you want to delete this address?",

	// settings
	"dashboard"			=> "Dashboard",
	"user-profile"		=> "User Profile",
	"edit-profile"		=> "Edit Profile",
	"edit-profile-store"=> "Edit Profile / Store Information",
	"manage-addresses"	=> "Address Book",
	"manage-bookmarks"	=> "Bookmarked Items",
	"user-options"		=> "User Options",
	"settings"			=> "Settings",
	"basic-info"		=> "Basic Information",
	"current-password"	=> "Current password",
	"new-password"		=> "New password",
	"credentials"		=> "Credentials",
	"cred-warning"		=> "When changing credentials, you must provide your current password.",
	"default-address"	=> "Default address",
	"no-addresses"		=> "You have no addresses listed. :add-one now.",
	"add-address"		=> "Add one",
	"add-new-address"	=> "Add new address",
	"new-address"		=> "New Address",
	"select-country"	=> "Select a country",
	"no-zones-country"	=> "No zones available",
	"yahoo-id"			=> "Yahoo! Messenger ID",
	"skype-id"			=> "Skype ID",

	// messaging
	"messages"			=> "Messages",
	"message-from"		=> "From",
	"message-to"		=> "To",
	"with"				=> "With",
	"mark-as-read"		=> "Mark as Read",
	"sent-on"			=> "Sent on",
	"read-on"			=> "Read on",
	"send-message"		=> "Send a Message",
	"unread-messages"	=> "Unread messages",
	"reply"				=> "Reply",
	"subject"			=> "Subject",
	"enter-subject"		=> "Enter conversation subject",
	"no-subject"		=> "No subject",
	"no-conversations"	=> "No conversations to display",
	"view-conversation"	=> "View Conversation",
	"compose-message"	=> "Compose Message",
	"conversation-with"	=> "Conversation with :user",
	"type-message-here"	=> "Type your message here",
	"load-old-messages" => "Load previous messages",
	"delete-confirm"	=> "Are you sure you want to delete the selected conversations?",
	"delete-confirm-one"=> "Are you sure you want to delete this conversation?",

	// feedback
	"my-feedback"		=> "My Feedback",
	"all-feedback"		=> "All Feedback",
	"sell-feedback"		=> "As Merchant",
	"buy-feedback"		=> "As Buyer",
	"given-feedback"	=> "Feedback Given",
	"post-feedback"		=> "Post Feedback",
	"no-feedback"		=> "No feedback posted.",
	"no-sell-feedback"	=> "No merchant feedback.",
	"no-buy-feedback"	=> "No buyer feedback.",
	"no-given-feedback"	=> "No feedback given yet.",
	"add-yours"			=> "Add yours!",
	"unread-feedback"	=> "Unread feedback",
	"feedback-rating"	=> "Your rating",
	"feedback-type"		=> "Type",
	"feedback-type-buy" => "Buyer",
	"feedback-type-sell"=> "Merchant",
	"feedback-comments"	=> "Comments",
	"report-feedback"	=> "Report this feedback",
	"based-off-feedback"=> "Based off :num feedback",
	"buyer-rating"		=> "Buyer Rating",
	"merchant-rating"	=> "Merchant Rating",
	"feedback-warning"	=> "Do not post feedback if you have not transacted with this user.",
	"feedback-success"	=> "Your feedback has been posted successfully!",
	"feedback-approval"	=> "Your feedback has been submitted for approval. Thanks for sharing!",

	// info icons
	"info-pw-length"	=> "&middot; At least :num characters long.",
	"info-pw-not-email"	=> "&middot; Not an email address.",
	"info-pw-not-name"	=> "&middot; Not similar to your name.",
	"info-pw-patterns"	=> "&middot; No predictable patterns.",
	"info-username"		=> "Will be shown instead of your full name wherever it is on display publicly.",
	"info-change-email"	=> "If changed, you will need to authenticate your new email address.",

);
