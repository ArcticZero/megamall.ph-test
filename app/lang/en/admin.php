<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Admin Language File
	|--------------------------------------------------------------------------
	|
	| These language lines are related to the administration control panel.
	|
	*/

	// navigation
	"admin-panel"			=> "Administration Panel",
	"dashboard"				=> "Dashboard",
	"users"					=> "Users",
	"storefronts"			=> "Storefronts",
		"store-list"		=> "Store List",
		"payment-gateways"	=> "Payment Gateways",
		"shipping-methods"	=> "Shipping Methods",
	"catalog"				=> "Catalog",
		"categories"		=> "Categories",
		"products"			=> "Products",
	"location"				=> "Location",
		"countries"			=> "Countries",
		"provinces"			=> "Provinces",
		"cities"			=> "Cities",
	"site-management"		=> "Site Management",
		"reported-items"	=> "Reported Items",
		"analytics"			=> "Analytics",
		"ad-spaces"			=> "Ad Spaces",
		"payment-partners"	=> "Payment Partners",
		"settings"			=> "Settings",

	// common
	"modal-close"			=> "Close",
	"char-range"			=> ":min-:max chars",
	"chars-max"				=> ":num chars max",
	"chars-min"				=> ":num chars min",
	"maximum-abbr"			=> "max",
	"optional"				=> "Optional",
	"valid-url"				=> "Enter a valid URL",
	"valid-slug"			=> "A-Z, 0-9 and hyphens only",
	"form-view-title"		=> "Viewing",
	"edit"					=> "Edit",
	"delete"				=> "Delete",
	"ban"					=> "Ban",
	"active"				=> "Active",
	"inactive"				=> "Inactive",
	"changes-saved"			=> "Changes have been saved.",
	"modal-no"				=> "No",
	"modal-yes"				=> "Yes",
	"slug"					=> "Slug",

	// errors
	"error"					=> "Error",
	"notice"				=> "Notice",
	"invalid-row"			=> "Invalid row specified",
	"no-rows"				=> "No rows to display",
	"not-found"				=> "The requested item was not found in the database.",

	// users
	"user-management"		=> "User Management",
	"pw-no-change"			=> "Leave blank for unchanged",
	"user-no-addresses"		=> "No addresses listed",
	"default-address"		=> "Default Address",
	"remove-address"		=> "Remove this Address",
	"user-activation"		=> "User Activation",
	"user-deactivation"		=> "User Deactivation",
	"user-ban"				=> "Ban User",
	"user-activate-q"		=> "Are you sure you want to activate",
	"user-deactivate-q"		=> "Are you sure you want to deactivate",
	"user-ban-q"			=> "Are you sure you want to ban",
	"user-activated"		=> "User account has been activated.",
	"user-deactivated"		=> "User account has been deactivated.",
	"user-banned"			=> "User account has been banned.",

	// storefronts
	"store-management"		=> "Store Management",
	"last-sync-date"		=> "Last sync",
	"merchant-username"		=> "Merchant username",
	"total-products"		=> "Total products",
	"store-activate-q"		=> "Are you sure you want to activate the store",
	"store-deactivate-q"	=> "Are you sure you want to deactivate the store",
	"store-activation"		=> "Store Activation",
	"store-deactivation"	=> "Store Deactivation",
	"store-activated"		=> "Store has been activated.",
	"store-deactivated"		=> "Store has been deactivated.",
	"store-sync"			=> "Synchronize",
	"store-synchronizing"	=> "Synchronizing",
	"store-sync-never"		=> "Never",
	"store-sync-done"		=> ":store sync completed.",
	"store-custom-url"		=> "Custom?",
	"store-deployment"		=> "Deploy Store",
	"store-deploy"			=> "Deploy",
	"store-deploy-q"		=> "Deploy the store",
	"store-deployed"		=> ":store has been deployed and should be up and running.",

	// payment gateways
	"payment-gateways"		=> "Payment Gateways",
	"gateway"				=> "Gateway",
);
