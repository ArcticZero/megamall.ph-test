<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Product Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines deal with product display.
	|
	*/

	'featured' 			=> "What's Hot",
	'new-products'		=> "Recently Added",
	'recently-viewed'	=> "Your Recently Viewed Items",
	'updated-products'	=> "Recently Updated",
	'all-products'		=> "All Items",
	'all-categories'	=> "All Categories",
	'search-for'		=> 'Search results for ":term"',
	'search-for-in'		=> 'Search results for ":term" in ":cat"',
	'search-results'	=> "Search Results",
	'no-results'		=> "No products found",
	'brand-by'			=> "by",
	'based-off-review'	=> "Based off :num review",
	'based-off-reviews'	=> "Based off :num reviews",
	'price-ex-tax'		=> "Excluding Tax:",
	'tax-rates-notice'	=> "Rates may vary per customer/area as per the merchant.",
	'product-info'		=> "Product Information",
	'product-specs'		=> "Specifications",
	'buy-from-store'	=> "Buy from Store",
	'inquire'			=> "Inquire",
	'inquiry-on-product'=> "Inquiry on :product",
	'visit-store'		=> "Visit Store",
	'more-from-store'	=> "View more products from :store",
	'report-this-item'	=> "Report this item",
	'is-your-product'	=> "This is your product listing.",
	'bookmark-this-item'=> "Bookmark this item",
	'remove-bookmark'	=> "Remove bookmark",

	// reviews
	'user-reviews'		=> "User Reviews",
	'write-review'		=> "Write a review",
	'no-reviews'		=> "No reviews for this product.",
	'write-one'			=> "Write one!",
	'review-rating'		=> "Your rating",
	'review-title'		=> "Title",
	'review-comments'	=> "Comments",
	'review-success'	=> "Your review has been posted successfully!",
	'review-approval'	=> "Your review has been submitted for approval. Thanks for sharing!",
	"report-review"		=> "Report this review",

	// grids
	"display-options"	=> "Display options",
	"sort-by"			=> "Sort by",
	"show"				=> "Show",
	"items-per-page"	=> "items per page",
	"display-as"		=> "Display as:",
	"display-list"		=> "List",
	"display-grid"		=> "Grid",

	// sorting
	"sort-relevance"	=> "Relevance",
	"sort-date"			=> "Most recent",
	"sort-rating"		=> "Highest rated",
	"sort-lowprice"		=> "Lowest price",
	"sort-highprice"	=> "Highest price",
);