<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| SEO Details
	|--------------------------------------------------------------------------
	|
	| I'd rather this be kept the same for each language..
	| But I'll keep it in a language file anyway.
	|
	*/
	
	"megamall"			=> "Megamall.ph",
	"title"     		=> "Electronics, gifts, fashion and home improvement items in the Philippines",
	"description"		=> "Electronics, gifts, fashion and home improvement items in the Philippines"

);
