<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" 		=> "Passwords must be at least eight characters and match the confirmation.",
	"user" 			=> "We couldn't find that email address.",
	"token" 		=> "This password reset token is invalid.",
	"sent" 			=> "You will receive an email shortly with instructions on how to reset your password.",
	"complete"		=> "Your password has been changed successfully.",

);
