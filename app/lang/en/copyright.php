<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Copyright Info
	|--------------------------------------------------------------------------
	|
	| Copyright information in footer.
	|
	*/

	"info"     		    => "&copy; " . date('Y') . " Philippine Online Shopping by Megamall.ph",
	"dev-link"			=> "Website Design and Development",
	"dev-link-title"	=> "Custom Website and Development",
	"dev-name"			=> "by Arctic Studios"

);
