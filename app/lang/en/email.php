<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Email
	|--------------------------------------------------------------------------
	|
	| All text contained within emails sent by the system are in this file.
	|
	*/

	// common
	"alt-link"				=> "If the button doesn't work, copy and paste this link in your browser window:",
	"greeting"				=> "Hi :firstname",
	"thanks"				=> "Cheers!",
	"sender"				=> "The Megamall team",
	
	// user email confirmation
	"user-confirm-subject"	=> "Please confirm your email address",
	"user-confirm-body"		=> "Thank you for using Megamall! To finish your registration, let's confirm your email is correct.",
	"user-confirm-btn-text"	=> "Confirm :email",

	// email change confirmation
	"user-email-change-body"=> "You have recently changed your email address. Let's confirm your new email is correct.",

	// password reminder
	"pw-reminder-subject"	=> "Password reset on Megamall",
	"pw-reminder-body"		=> "You're receiving this email because you requested a password reset for your Megamall account. If you did not request this change, you can safely ignore this email.",
	"pw-reminder-btn-text"	=> "Reset my Password",
);
