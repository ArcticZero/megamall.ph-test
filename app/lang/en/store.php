<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Store Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines deal with store information.
	|
	*/

	'store-directory'		=> "Store Directory",
	'no-stores'				=> "No stores found",
	'store-info'			=> "Store Information",
	'store-name'			=> "Store name",
	'view-portal'			=> "View store portal",
	'visit-store'			=> "Visit this store",
	'store-admin'			=> "Store Administration",
	'identification'		=> "Store Identification",
	'contact-info'			=> "Contact Information",
	'personal-id'			=> "Personal Identification",
	'physical-address'		=> "Physical address",
	'store-name'			=> "Name of store",
	'store-url'				=> "Unique store URL",
	'store-description'		=> "Store Description",
	'no-description-given'	=> "No description given",
	'enable-id-change'		=> "Unlock store info",
	'related-categories'	=> "Related Categories",
	'select-categories'		=> "Select any product categories relevant to what you will be selling on your store",
	'create-your-store'		=> "Create your store now!",
	'create-store-head'		=> "Create your Store",
	'create-accept-terms'	=> "By applying for a store front, you accept Megamall.ph's :terms.",
	'store-terms'			=> "Merchant Terms and Conditions",
	'average-rating'		=> "Average Rating",

	'proof-label'			=> "Proof of identity/address",
	'proof-warning'			=> "To apply your own store front, we require electronic copies/scans of at least one (1) of the following documents from you:",
	'proof-of-identity'		=> "Valid proof of your identity",
	'proof-identity-note'	=> "any ID with your name and picture on it",
	'proof-of-address'		=> "Valid proof of your address",
	'proof-address-note'	=> "last 3 months utility billings",

	'id-change-warning'		=> "Any changes to store identification will require administrator approval before taking effect.",
);