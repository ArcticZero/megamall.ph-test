<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Help Language File
	|--------------------------------------------------------------------------
	|
	| These language lines pertain to the help center.
	|
	*/

	'help-center'		=> "Help Center",

	// reports
	"send-a-report"		=> "Send a Report",
	"submit-report"		=> "Submit Report",
	"user"				=> "User",
	"product"			=> "Product",
	"store" 			=> "Store",
	"feedback"			=> "Feedback",
	"review"			=> "Review",
	"item-type"			=> "Type",
	"item-description"	=> "Description",
	"details"			=> "Details",
	"feedback-name"		=> "Feedback by ':poster' to ':user'",
	"review-name"		=> "Review of ':product' by ':user'",
);
