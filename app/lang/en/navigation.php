<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Navigation Language File
	|--------------------------------------------------------------------------
	|
	| These language lines pertain to common navigation panels and all elements
	| contained within.
	|
	*/

	"home"				=> "Home",
	"account"			=> "My Account",
	"products"			=> "Products",
	"stores" 			=> "Stores",
	"categories" 		=> "Categories",
	"more-categories"	=> "More Categories...",
	"brands" 			=> "Brands",
	"welcome-guest"		=> 'Buy and sell right away. :login or :register now for free!',
	"welcome-user"		=> 'Welcome back, :firstname!',
	"login" 			=> "Sign in",
	"register"			=> "create an account",
	"logout"			=> "Logout",
	"or"				=> "or",
	"help"				=> "Help",
	"terms"				=> "Terms",
	"privacy"			=> "Privacy",
	"contact-us" 		=> "Get in Touch",
	"toggle"			=> "Toggle Navigation",
	"under-construction"=> "This page is currently under construction. Stay tuned, we'll be up soon.",
	"megamall-subdomain"=> ".megamall.ph",
);
