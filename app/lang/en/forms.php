<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Form Labels
	|--------------------------------------------------------------------------
	|
	| This file covers common form labels and placeholder text.
	|
	*/
	
	"login_to"				=> "Sign in to",
	"email"					=> "Email",
	"password"				=> "Password",
	"remember-me"			=> "Remember me on this computer.",
	"login"					=> "Sign In",
	"register"				=> "Create an Account",
	"submit"				=> "Submit",
	"submit-application"	=> "Submit Application",
	"cancel"				=> "Cancel",
	"save-changes"			=> "Save Changes",
	"search"				=> "Search",
	"delete"				=> "Delete",
	"delete-selected"		=> "Delete selected",
	"apply"					=> "Apply",
	"subject"				=> "Subject",
	"updated"				=> "Updated",
	"all-categories"		=> "All Categories",
	"not-applicable"		=> "Not Applicable",
	"none"					=> "None",
	"send"					=> "Send",
	"leave-blank-unchanged"	=> "Leave blank for unchanged",
	"optional"				=> "(optional)",
	"click-to-expand"		=> "Click to expand/hide",

	"email-ph"				=> "Email address",
	"password-ph"			=> "Password",
	"email-username-ph"		=> "Email or username",
	"terms-ph"				=> "Looking for something?",

	"login-fb"				=> "Sign in with Facebook",
	"login-google"			=> "Sign in with Google",

	// password rating
	"pw-strength"			=> "Strength",
	"pw-weak"				=> "Weak",
	"pw-fair"				=> "Fair",
	"pw-medium"				=> "Medium",
	"pw-good"				=> "Good",
	"pw-strong"				=> "Strong",

	// error messages
	"error-header"			=> "Whoops! Hang on a sec:",
	"error-pw-strength"		=> "Your password is too weak",
	"error-required" 		=> "This field is required",
	"error-remote"			=> "Please fix this field",
	"error-email" 			=> "Please enter a valid email address",
	"error-email-taken" 	=> "This email address is already in use",
	"error-email-not-found"	=> "Email address not found",
	"error-username-taken"	=> "This username is already in use",
	"error-add-not-found"	=> "Invalid address specified",
	"error-url" 			=> "Please enter a valid URL",
	"error-store-url-taken"	=> "This store URL is already in use",
	"error-store-name-taken"=> "This store name is already in use",
	"error-date" 			=> "Please enter a valid date",
	"error-dateISO" 		=> "Please enter a valid date (ISO)",
	"error-number" 			=> "Please enter a valid number",
	"error-digits" 			=> "Please enter only digits",
	"error-creditcard" 		=> "Please enter a valid credit card number",
	"error-equalTo" 		=> "Must match the password field",
	"error-maxlength" 		=> "Please enter no more than :length characters",
	"error-minlength" 		=> "Please enter at least :length characters",
	"error-rangelength"		=> "Please enter a value between :minlength and :maxlength characters long",
	"error-range" 			=> "Please enter a value between :min and :max",
	"error-max" 			=> "Please enter a value less than or equal to :max",
	"error-min" 			=> "Please enter a value greater than or equal to :min",
	"error-oldpw-required"	=> "Required if you want to change your credentials",
	"error-oldpw-wrong"		=> "Incorrect password",
	"error-too-many-files"	=> "You many only upload a maximum of :num files",

	"error-attr-taken"		=> "This :attribute is already in use",

	"error-maxlength-js" 	=> "Please enter no more than {0} characters",
	"error-minlength-js" 	=> "Please enter at least {0} characters",
	"error-rangelength-js"	=> "Please enter a value between {0} and {1} characters long",
	"error-range-js" 		=> "Please enter a value between {0} and {1}",
	"error-max-js" 			=> "Please enter a value less than or equal to {0}",
	"error-min-js" 			=> "Please enter a value greater than or equal to {0}",

	"error-head"			=> "Whoops!",

	"error-login-db"		=> "Something went wrong while trying to log you in. Please try again in a few minutes.",
	"error-login-user"		=> "You entered an invalid email or password.",
	"error-login-status"	=> "The account you are trying to log in is currently inactive.",
	"error-login-confirm"	=> "This account has not yet been confirmed. Please check your email for the confirmation message.",
	"error-login-banned"	=> "This account has been suspended for one or more violations of our terms of service.",

	"error-confirm"			=> "An invalid confirmation code has been specified.",

	// success messages
	"success-head"			=> "Success!",
	"success-reg-head"		=> "Thank you for signing up!",
	"success-reg"			=> "You will receive an email shortly with instructions on how to activate your account.",
	"success-edit-profile"	=> "Your profile has been updated.",
	"success-edit-addresses"=> "Your address book has been saved.",

	"success-confirm-head"	=> "Your account has been activated!",
	"success-confirm"		=> "You may proceed to log in now. Thank you!",

	"success-report"		=> "Your report has been submitted for review.",

	"success-create-store"	=> "Your store front application has been submitted for approval. We will be contacting you soon. Thank you!",
);
