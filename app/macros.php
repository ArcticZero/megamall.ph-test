<?php

/*
|--------------------------------------------------------------------------
| HTML Macros
|--------------------------------------------------------------------------
|
| Registered here are all HTML macros for quick view creation.
|
*/

// Banner
HTML::macro('banner', function($banner) {
	$html = '<div class="slider-wrapper theme-default">' .
			'<div class="ribbon"></div>' .
			'<div id="' . $banner['name'] . '" class="nivoSlider">';

	$locale = Config::get('app.locale');
	$images = $banner->images;

	if(count($images)) {
		foreach($images as $img) {
			$caption = BannerImageCaption::where('language', $locale)
										 ->where('banner_image_id', $img['id'])
										 ->pluck('caption');

			$html .= '<a href="' . url($img['link']) . '" style="height:' . $banner['height'] . 'px;width:' . $banner['width'] . 'px;">' .
					 HTML::image('img/promo/' . $img['image'], $alt=$img['alt'], array('title' => $caption)) .
					 '</a>';
		}

		$html .= '</div></div>';

		return $html;
	} else {
		return false;
	}
});

// Format currency
HTML::macro('currency', function($val) {
	$c = Currency::where('code', Session::get('currency', Config::get('settings.currency')))->first();

	return ($c['symbol_left'] ? $c['symbol_left'] : '') . ' ' . number_format($val * $c['value'], $c['decimal_places']) . ' ' . ($c['symbol_right'] ? $c['symbol_right'] : '');
});

// Product image grid
HTML::macro('productGrid', function($products, $sidebar = false) {
	$html = '';
	$width = Config::get("image.sizes.small.0");
	$height = Config::get("image.sizes.small.1");
	$i = 1;

	foreach($products as $row) {
		$path = pathinfo($row['image_url']);
		$image_url = $row['image_url'] ? 'img/products/' . $row['permalink'] . '/' . $path['filename'] . '-' . $width . 'x' . $height . '.jpg' : 'img/no-image-small.png';


		$html .= '<article class="product-box ' . ($sidebar ? 'col-xs-12 col-ms-6 col-sm-4 col-md-4 col-lg-3' : 'col-xs-12 col-ms-6 col-sm-4 col-md-2') . (is_null($row['avg_rating']) ? ' rating-filler' : '') . '">' .
			     '<div class="product-image" style="width: ' . $width . 'px; height:' . $height . 'px;"><div class="inner"><a href="' . url('products/' . $row['product_permalink']) . '">' . HTML::image($image_url , $row['name']) .'</a></div></div>' .
			     '<div class="product-name"><a href="' . url('products/' . $row['product_permalink']) . '">' . $row['name'] . '</a></div>' .
			     '<div class="product-price">' . ($row['special'] ? '<div class="old-price">' . HTML::currency($row['price']) . '</div>' . HTML::currency($row['special']) : HTML::currency($row['price'])) . '</div>' .
			     (!is_null($row['avg_rating']) ? '<div class="star-rating" data-score="' . $row['avg_rating'] . '"></div>' : '') .
			     '</article>';
		
		if($sidebar) {
			if($i % 4 == 0) $html .= '<div class="clearfix visible-lg"></div>';
			if($i % 3 == 0) $html .= '<div class="clearfix visible-md visible-sm"></div>';
			if($i % 2 == 0) $html .= '<div class="clearfix visible-ms"></div>';
		} else {
			if($i % 6 == 0) $html .= '<div class="clearfix visible-md visible-lg"></div>';
			if($i % 3 == 0) $html .= '<div class="clearfix visible-sm"></div>';
			if($i % 2 == 0) $html .= '<div class="clearfix visible-ms"></div>';
		}

		$i++;
	}

	return $html;
});

// Encode base64 (safe) URL
HTML::macro('encodeUrl', function($url) {
	return strtr(base64_encode($url), '+/=', '-_,');
});

// Decode base64 (safe) URL
HTML::macro('decodeUrl', function($url) {
	return base64_decode(strtr($url, '-_,', '+/='));
});
