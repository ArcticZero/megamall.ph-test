<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ProductSync extends Command {
	protected $name = 'sync:products';
	protected $description = 'Sync all store products to mall.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$filter = $this->option('store');
		$lang = $this->option('lang') ? $this->option('lang') : 'en';
		$total = array('new' => 0, 'updated' => 0, 'del' => 0);
		$new = array('products' => array(), 'descriptions' => array(), 'images' => array());
		
		// get stores
		$stores = !empty($filter) ? Store::whereIn('id', $filter)->where('status', 1)->get() : Store::where('status', 1)->get();
		
		// begin database transaction
		DB::disableQueryLog();
		DB::beginTransaction();
		
		foreach($stores as $store) {
			$this->info("Synchronizing store `" . $store->store_name . "`");

			// set to local connection
			Config::set('database.connections.store', Config::get('stores/' . $store->id . '.connection'));

			$ids = array();

			if(isset($db)) {
				$db = DB::reconnect('store');
			} else {
				$db = DB::Connection('store');
			}

			$this->info("Connecting to database: `" . $db->getDatabaseName() . "`@`" . Config::get('stores/' . $store->id . '.connection.host') . "`");
			
			// get products
			$products = $db->table('product')
						   ->select('product_id', 'mall_category', 'sku', 'model', 'product.image', 'price', 'manufacturer.name AS brand', 'status', 'date_added', 'date_modified', 'status')
						   ->leftJoin('manufacturer', 'product.manufacturer_id', '=', 'manufacturer.manufacturer_id')
						   ->get();
				   
			foreach($products as $row) {
				$ids[] = $row->product_id;
				
				// check if product has been updated since last sync
				$info = DB::table('products')
						  ->select('updated_at', 'status', 'avg_rating')
						  ->where('store_id', $store->id)
						  ->where('product_id', $row->product_id)
					      ->first();
							  
				if(!isset($info->updated_at) || (isset($info->updated_at) && strtotime($info->updated_at) < strtotime($row->date_modified))) {	
					// show memory usage
					$this->info(number_format(memory_get_usage() / 1024, 2, '.', '') . 'KB - Store "' . $store->store_name . '", Start of product "' . $row->product_id . '"');   
					
					// determine product URL
					$seo = $db->table('url_alias')
							  ->select('keyword')
							  ->where('query', 'product_id=' . $row->product_id)
							  ->first();
						  
					$product_url = isset($seo->keyword) ? $seo->keyword : 'index.php?route=product/product&product_id=' . $row->product_id;
					$buy_url = $product_url;
					$image_url = $row->image ? 'image/' . $row->image : NULL;
					
					// keep status if admin-disabled
					if(isset($info->status) && $info->status == '2') {
						$status = $info->status;
					} else {
						$status = $row->status;
					}

					// get product price
					$ch = curl_init($store->store_url . 'index.php?route=megamall/getInfo&product_id=' . $row->product_id);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					$pp = json_decode(curl_exec($ch));
					
					// wipe existing product info
					Product::productKey($store->id, $row->product_id)->delete();
					
					DB::table('product_descriptions')
					  ->where('store_id', $store->id)
					  ->where('product_id', $row->product_id)
					  ->delete();
					  
					DB::table('product_images')
					  ->where('store_id', $store->id)
					  ->where('product_id', $row->product_id)
					  ->delete();
					
					// collate info
					try {
						$product = array(
										 'store_id' => $store->id,
										 'product_id' => $row->product_id,
										 'category_id' => $row->mall_category,
										 'sku' => $row->sku,
										 'model' => $row->model,
										 'image_url' => $image_url,
										 'buy_url' => $buy_url,
										 'price' => $pp->price,
										 'special' => $pp->special ? $pp->special : NULL,
										 'tax_free' => $pp->tax_free,
										 'brand' => $row->brand,
										 'created_at' => $row->date_added,
										 'updated_at' => $row->date_modified,
										 'status' => $status
										);
					} catch(Exception $e) {
						print_r($pp);
						die("Product ID:" . $row->product_id);
						echo $e->getMessage() . ', ' . $e->getFile() . ' ' . $e->getLine();
					}
					
					// import descriptions
					$descs = $db->table('product_description')
							    ->select('language.code', 'product_description.name', 'description', 'meta_description', 'meta_keyword', 'tag')
							    ->where('product_id', $row->product_id)
							    ->join('language', 'product_description.language_id', '=', 'language.language_id')
							    ->get();
					
					foreach($descs as $desc) {
						// create permalink from specified language
						if($desc->code == $lang) {
							$product['permalink'] = BaseController::productSlug($desc->name);
						}

						$data = array(
									  'store_id' => $store->id,
									  'product_id' => $row->product_id,
									  'language' => $desc->code,
									  'name' => $desc->name,
									  'description' => $desc->description,
									  'meta_description' => $desc->meta_description,
									  'meta_keyword' => $desc->meta_keyword,
									  'tag' => $desc->tag
									 );
						
						$new['descriptions'][] = $data;
					}

					// show memory usage
					$this->info(number_format(memory_get_usage() / 1024, 2, '.', '') . 'KB - Store "' . $store->store_name . '", Product "' . $row->product_id . '", after descriptions');

					// create image dir
					$path = public_path() . '\\img\\products\\' . $product['permalink'];
					File::deleteDirectory($path);
					File::makeDirectory($path);

					if($row->image) {
						// generate thumbnail of main image
						ImageController::thumb($store->store_url . $image_url, 'tiny', $path);
						ImageController::thumb($store->store_url . $image_url, 'small', $path);
						ImageController::thumb($store->store_url . $image_url, 'medium', $path);
						ImageController::thumb($store->store_url . $image_url, 'large', $path);

						// show memory usage
						$this->info(number_format(memory_get_usage() / 1024, 2, '.', '') . 'KB - Store "' . $store->store_name . '", Product "' . $row->product_id . '", after main image');
					}
					
					// import images
					$imgs = $db->table('product_image')
							   ->select('image', 'sort_order')
							   ->where('product_id', $row->product_id)
							   ->get();
							   
					foreach($imgs as $img) {
						if($img->image) {
							// generate thumbnails
							$imgpath = $store->store_url . 'image/' . $img->image;
							ImageController::thumb($imgpath, 'tiny', $path);
							ImageController::thumb($imgpath, 'small', $path);
							ImageController::thumb($imgpath, 'medium', $path);
							ImageController::thumb($imgpath, 'large', $path);

							$data = array(
										  'store_id' => $store->id,
										  'product_id' => $row->product_id,
										  'image_url' => $imgpath,
										  'sort_order' => $img->sort_order
										 );
							
							$new['images'][] = $data;
						}
					}

					if(count($imgs) > 0) {
						// show memory usage
						$this->info(number_format(memory_get_usage() / 1024, 2, '.', '') . 'KB - Store "' . $store->store_name . '", Product "' . $row->product_id . '", after other images (' . count($imgs) . ' images).');
					}

					// count and remember rating
					if(isset($info->updated_at)) {
						$total['updated']++;
						$product['avg_rating'] = $info->avg_rating;
					} else {
						$total['new']++;
					}

					$new['products'][] = $product;

					// do garbage collection
					gc_collect_cycles();
				}
			}
			
			// delete products that didn't make the list
			if(!empty($ids)) {
				$del = Product::where('store_id', $store->id)
							  ->whereNotIn('product_id', $ids)
							  ->delete();
					
				DB::table('product_descriptions')
				  ->where('store_id', $store->id)
				  ->whereNotIn('product_id', $ids)
				  ->delete();
						  
				DB::table('product_images')
				  ->where('store_id', $store->id)
				  ->whereNotIn('product_id', $ids)
				  ->delete();
				  
				DB::table('product_reviews')
				  ->where('store_id', $store->id)
				  ->whereNotIn('product_id', $ids)
				  ->delete();

				DB::table('user_product_views')
				  ->where('store_id', $store->id)
				  ->whereNotIn('product_id', $ids)
				  ->delete();
				  
				$total['del'] += $del;
			}

			// change last sync date
			$store->last_sync = date("Y-m-d H:i:s");
			$store->save();
		}
		
		// insert product rows
		if(!empty($new['products'])) {
			try {	 
				DB::table('products')
				  ->insert($new['products']);
			} catch(ValidationException $e) {
				DB::rollback();
				throw($e);
			} catch(\Exception $e) {
				DB::rollback();
				throw($e);
			}
		}
		
		// insert descriptions
		if(!empty($new['descriptions'])) {
			try {	 
				DB::table('product_descriptions')
				  ->insert($new['descriptions']);
			} catch(ValidationException $e) {
				DB::rollback();
				throw($e);
			} catch(\Exception $e) {
				DB::rollback();
				throw($e);
			}
		}
		
		// insert images
		if(!empty($new['images'])) {
			try {	 
				DB::table('product_images')
				  ->insert($new['images']);
			} catch(ValidationException $e) {
				DB::rollback();
				throw($e);
			} catch(\Exception $e) {
				DB::rollback();
				throw($e);
			}
		}
		
		// commit changes
		DB::commit();
		
		// output
		$this->info('Done! Added ' . $total['new'] . ' product(s), updated ' . $total['updated'] . ', and deleted ' . $total['del'] . '.');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('store', 's', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'Only sync specified store ID(s).'),
			array('lang', 'l', InputOption::VALUE_OPTIONAL, 'Specifies default language with which to generate URL slug.'),
		);
	}

}
