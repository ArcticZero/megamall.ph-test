<?php 

return array( 
	
	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => 'Session', 

	/**
	 * Consumers
	 */
	'consumers' => array(

        'Facebook' => array(
            'client_id'     => '140378766069896',
            'client_secret' => 'fd672ab8fd337d5a1d556c67046da528',
            'scope'         => array('email'),
			'request_str'   => '/me',
			'firstname_var' => 'first_name',
			'lastname_var'  => 'last_name',
        ),	
		
		'Google' => array(
			'client_id'     => '189797744195.apps.googleusercontent.com',
			'client_secret' => 'seonT6ElB5iawKHRzbW_F4h8',
			'scope'         => array('userinfo_email', 'userinfo_profile'),
			'request_str'   => 'https://www.googleapis.com/oauth2/v1/userinfo',
			'firstname_var' => 'given_name',
			'lastname_var'  => 'family_name',
		),  

	)

);