<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Cached Image Lifetime
	|--------------------------------------------------------------------------
	|
	| Number of minutes to keep a cached product image.
	|
	*/

	'lifetime' => 10,

	/*
	|--------------------------------------------------------------------------
	| Product Image Sizes
	|--------------------------------------------------------------------------
	|
	| Thumbnail dimensions for product images (w,h).
	|
	*/

	'sizes' => array(
		'tiny' => array(95, 95),
        'small' => array(150, 150),
        'medium' => array(550, 550),
        'large' => array(1024, 1024)
    ),

);
