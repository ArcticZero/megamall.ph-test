<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Home
Route::get('/', 									'HomeController@home');

// Stores
Route::get('stores', 								'StoreController@show_all');
Route::get('stores/create',							'StoreController@create');
Route::post('stores/create',						'StoreController@do_create');
Route::get('stores/{slug?}',						'StoreController@view')->where('slug', '.+');

// Categories
Route::get('categories',							'CategoryController@show_all');

// Products
Route::get('products',								'ProductController@show_all');
Route::get('products/inquire/{slug}',				'MessagingController@product_inquiry')->where('slug', '.+');	
Route::get('products/{slug}',						'ProductController@view')->where('slug', '.+');
Route::post('bookmark/{slug}',						'ProductController@toggle_bookmark')->where('slug', '.+');

// User management
Route::get('signup',                 				'UserController@signup');
Route::get('signup/inline',                		 	'UserController@inline_signup');
Route::post('signup',                 				array('as' => 'signup', 'uses' => 'UserController@do_signup'));
Route::get('login',                  				'UserController@login');
Route::post('login',                  				array('as' => 'login', 'uses' => 'UserController@do_login'));
Route::get('login/{provider}', 						'UserController@oauth_login');
Route::get('signup/confirm/{code}',         		'UserController@confirm');
Route::get('profile/edit',							'UserController@edit_profile');
Route::post('profile/edit',							'UserController@save_profile');
Route::get('profile/addresses',						'UserController@address_book');
Route::post('profile/addresses',					'UserController@save_addresses');
Route::post('profile/addresses/zones',				'UserController@get_zones');
Route::get('profile/bookmarks',						'UserController@bookmarks');
Route::get('profile/{id?}',							'UserController@profile')->where('id', '[0-9]+');
Route::get('forgot-password',        				'UserController@forgot_password');
Route::post('forgot-password',        				'UserController@do_forgot_password');
Route::get('reset-password/{token}', 				'UserController@reset_password');
Route::post('reset-password',        				'UserController@do_reset_password');
Route::get('logout',                				array('as' => 'logout', 'uses' => 'UserController@logout'));

// Messaging
Route::get('messages',								'MessagingController@conversations');
Route::get('messages/new/{id}',						'MessagingController@new_conversation')->where('id', '[0-9]+');
Route::post('messages/load-previous',				'MessagingController@load_previous_messages');
Route::post('messages/load-next',					'MessagingController@load_next_messages');
Route::post('messages/send',						'MessagingController@send_message');
Route::post('messages/send/new',					'MessagingController@do_new_conversation');
Route::post('messages/read/{ids}',					'MessagingController@read_messages')->where('ids', '[0-9,]+');
Route::post('messages/delete/{ids}',				'MessagingController@delete_messages')->where('ids', '[0-9,]+');
Route::post('messages/count-unread',				'MessagingController@count_unread_messages');
Route::get('messages/{id}',							'MessagingController@view')->where('id', '[0-9]+');

// Reviews
Route::post('review/product/save',					'ProductController@save_review');
Route::post('review/product/load',					'ProductController@load_reviews');
Route::post('review/user/save',						'UserController@save_feedback');
Route::post('review/user/load',						'UserController@load_feedback');
Route::post('review/user/read/{ids}',				'UserController@read_feedback')->where('ids', '[0-9,]+');
Route::post('review/user/count-unread',				'UserController@count_unread_feedback');

// Help center
Route::get('help',									'HelpController@home');
Route::get('help/report/{type}/{id}', 				'HelpController@report')->where('type', '.+')->where('id', '.+');
Route::post('help/report',							'HelpController@do_report');

// Search
Route::get('search', 								array('as' => 'search', 'uses' => 'ProductController@search'));

// Misc
Route::get('contact',								'HelpController@contact_us');
Route::post('contact',								'HelpController@do_contact_us');
Route::get('terms',									'HelpController@terms');
Route::get('privacy',								'HelpController@privacy');

// Admin login
Route::get(Config::get('app.admin') . '/login',		'AdminUserController@login');
Route::post(Config::get('app.admin') . '/login',	array('as' => 'admin-login', 'uses' => 'AdminUserController@do_login'));
Route::get(Config::get('app.admin') . '/logout',	array('as' => 'admin-logout', 'uses' => 'AdminUserController@logout'));

// Admin
Route::group(array('prefix' => Config::get('app.admin'), 'before' => 'admin'), function() {
    Route::get('',	                             	'AdminController@dashboard');

    // Users
    Route::group(array('prefix' => '/users'), function() {
    	Route::get('',								'AdminUserController@index');
    	Route::post('save',							'AdminUserController@save');
    	Route::post('info',							'AdminUserController@info');
        Route::post('refresh',                      'AdminUserController@do_list');
    	Route::post('activate',						'AdminUserController@activate');
    	Route::post('deactivate',					'AdminUserController@deactivate');
        Route::post('ban',                          'AdminUserController@ban');
    });

    // Store Fronts
    Route::group(array('prefix' => '/storefronts'), function() {
        Route::get('',                              'AdminStoreController@index');
        Route::post('save',                         'AdminStoreController@save');
        Route::post('info',                         'AdminStoreController@info');
        Route::post('refresh',                      'AdminStoreController@do_list');
        Route::post('activate',                     'AdminStoreController@activate');
        Route::post('deactivate',                   'AdminStoreController@deactivate');
        Route::post('sync',                         'AdminStoreController@sync');
        Route::post('deploy',                       'AdminStoreController@deploy');

        // Payment Gateways
        Route::group(array('prefix' => '/payment'), function() {
            Route::get('',                          'AdminStoreController@payment');
            Route::post('refresh',                  'AdminStoreController@do_payment_list');
            Route::post('activate',                 'AdminStoreController@activate_payment');
            Route::post('deactivate',               'AdminStoreController@deactivate_payment');
        });
          
        // Shipping Extensions
        Route::group(array('prefix' => '/shipping'), function() {
            Route::get('',                          'AdminStoreController@shipping');
            Route::post('refresh',                  'AdminStoreController@do_shipping_list');
            Route::post('activate',                 'AdminStoreController@activate_shipping');
            Route::post('deactivate',               'AdminStoreController@deactivate_shipping');
        });
    });

    // Ajax Calls
    Route::post('get-zones',                        'AdminController@get_zones');
});


// Form validation
Route::controller('validate',						'ValidationController');

// Catch all category route
App::before(function($request) {
	Route::any('{slug}', 							'CategoryController@products')->where('slug', '.+');
});