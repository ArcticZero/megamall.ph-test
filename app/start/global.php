<?php

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

ClassLoader::addDirectories(array(

	app_path().'/commands',
	app_path().'/controllers',
	app_path().'/controllers/admin',
	app_path().'/models',
	app_path().'/database/seeds',

));

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

Log::useFiles(storage_path().'/logs/laravel.log');

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

App::error(function(Exception $exception, $code)
{
	Log::error($exception);
});

/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function()
{
	return Response::make("Be right back!", 503);
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/

require app_path().'/filters.php';

/*
|--------------------------------------------------------------------------
| Require The Macros File
|--------------------------------------------------------------------------
|
| Let's load up that HTML Macros file.
|
*/

require app_path().'/macros.php';

/*
|--------------------------------------------------------------------------
| Set default language (or user language)
|--------------------------------------------------------------------------
|
| Set app language based on user preference first, and site default second.
|
*/

App::setLocale(Session::get('lang', Config::get('settings.lang')));

/*
|--------------------------------------------------------------------------
| Layout globals
|--------------------------------------------------------------------------
|
| Quick globals used in every view
|
*/

View::composer('*', function($view) {
	// get categories
	$view->with('cat_bar', Category::get(NULL, 9));
	$view->with('search_cats', Category::get(NULL));
	$view->with('navtop', array('home' => '/', 'stores' => 'stores', 'faq' => 'faq', 'help' => 'help'));
	$view->with('navbottom', array('terms' => 'terms', 'privacy' => 'privacy', 'contact-us' => 'contact', 'help' => 'help'));
    $view->with('active', ' class="active"');
});

/*
|--------------------------------------------------------------------------
| Additional validation rules
|--------------------------------------------------------------------------
|
| Custom rules for validation class.
|
*/

Validator::extend('blank', function($field, $value, $parameters) {
	return $value == '';
});

Validator::extend('authenticate', function($field, $value, $parameters) {
	$user = User::find(Auth::user()->id);
	if(Hash::check($value, $user->password)) {
		return true;
	} else {
		return false;
	}
});

Validator::extend('max_clean', function($field, $value, $parameters) {
	return strlen(strip_tags($value)) <= $parameters[0];
});

Validator::extend('alpha_spaces', function($attribute, $value) {
    return preg_match('/^([-a-z0-9_-\s])+$/i', $value);
});

Validator::extend('slug', function($attribute, $value) {
    return preg_match('/^[a-z][-a-z0-9]*$/', $value);
});

Validator::extend('has_files', function($field, $value) {
	if(!is_array($value)) {
		return false;
	}

	if($value[0]) {
		return true;
	} else {
		return false;
	}
});

Validator::extend('file_sizes', function($field, $value, $parameters) {
	foreach($value as $row) {
		if($row && $row->isValid()) {
			if(($row->getSize() / 1024) > $parameters[0]) {
				return false;
			}
		}
	}

	return true;
});

/*
|--------------------------------------------------------------------------
| For setting simple variables in Blade templates
|--------------------------------------------------------------------------
|
| Extend the templating engine a bit. {? ?} tags.
|
*/

Blade::extend(function($value) {
    return preg_replace('/\{\?(.+)\?\}/', '<?php ${1} ?>', $value);
});

/*
|--------------------------------------------------------------------------
| Start native PHP session
|--------------------------------------------------------------------------
|
| For use with store sessions (single site login)
|
*/

session_start();
