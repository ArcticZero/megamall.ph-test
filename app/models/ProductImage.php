<?php

class ProductImage extends Eloquent {
	public $timestamps = false;
	protected $table = "product_images";
	
	public function product() {
        return $this->belongsTo('Product');
    }
}