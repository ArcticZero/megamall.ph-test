<?php

class Banner extends Eloquent {
	public $timestamps = false;
	
	public function images() {
		return $this->hasMany('BannerImage');
    }
}