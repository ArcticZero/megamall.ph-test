<?php

class City extends Eloquent {
	public $timestamps = false;

	public function zone() {
		return $this->belongsTo('Zone');
	}
}