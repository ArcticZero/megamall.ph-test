<?php

class CategoryDescription extends Eloquent {
	public $timestamps = false;
	protected $table = 'category_descriptions';

	public function category() {
		return $this->belongsTo('Category');
	}
}