<?php

class BannerImage extends Eloquent {
	public $timestamps = false;
	protected $table = "banner_images";
	
	public function banner() {
        return $this->belongsTo('Banner');
    }

    public function description() {
        return $this->hasOne('BannerImageCaption');
    }
}