<?php

class Store extends Eloquent {
	public $timestamps = false;

	public function user() {
		return $this->belongsTo('User');
	}

	public function products() {
		return $this->hasMany('Product');
	}

	public function address() {
		return $this->belongsTo('Address');
	}

	public function productViews() {
		return $this->hasMany('ProductView');
	}

	public function bookmarks() {
		return $this->hasMany('Bookmark');
	}

	public function categories() {
		return $this->belongsToMany('Category', 'store_categories');
	}
}