<?php

class Country extends Eloquent {
	public $timestamps = false;
	protected $primaryKey = 'country_id';

	public function zones() {
		return $this->hasMany('Zone');
	}

	public function provinces() {
		return $this->hasMany('Province');
	}
}