<?php

class Feedback extends Eloquent {
	public $timestamps = false;
	protected $table = 'feedback';
	
	public function user() {
        return $this->belongsTo('User');
    }

    public static function getFeedback($user_id, $type = false, $skip = false, $given = false, $approved = true) {
    	$query = static::select('feedback.*', 'users.firstname', 'users.lastname', 'users.username')
        			   ->where(($given !== false ? 'feedback.posted_by' : 'feedback.user_id'), $user_id)
    				   ->where('feedback.status', $approved ? 1 : 0);

    	// specific type of feedback is selected
    	if($type) $query->where('feedback.type', $type);

    	$query->join('users', ($given !== false ? 'feedback.user_id' : 'feedback.posted_by'), '=', 'users.id')
              ->orderBy('feedback.posted_on', 'desc')
              ->take(Config::get('settings.feedback_per_load'));

        // for dynamic loading
        if($skip) $query->skip($skip);
        
        return $query->get();
    }

    public static function getGiven($user_id, $type = false, $skip = false, $approved = true) {
        return self::getFeedback($user_id, $type, $skip, true, $approved);
    }
}