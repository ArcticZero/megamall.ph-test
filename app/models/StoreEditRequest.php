<?php

class StoreEditRequest extends Eloquent {
	public $timestamps = false;
	protected $table = 'store_edit_requests';
	
	public function store() {
        return $this->belongsTo('Store');
    }
}