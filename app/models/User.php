<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {
	protected $fillable = array('firstname', 'lastname', 'email', 'password', 'confirmed');
	protected $hidden = array('password');

	public function store() {
		return $this->hasOne('Store');
	}
	
	public function addresses() {
		return $this->hasMany('Address');
	}
	
	public function providers() {
        return $this->hasMany('Provider');
    }

    public function conversations() {
    	return $this->belongsToMany('Conversation', 'conversation_users')->withPivot('deleted');
    }

    public function feedback() {
    	return $this->hasMany('Feedback');
    }

    public function settings() {
    	return $this->hasMany('Setting');
    }

    public function productViews() {
		return $this->hasMany('ProductView');
	}

	public function bookmarks() {
		return $this->hasMany('Bookmark');
	}

    public function getAuthIdentifier() {
		return $this->getKey();
	}

	public function getAuthPassword() {
		return $this->password;
	}

	public function getReminderEmail() {
		return $this->email;
	}

	public function getRememberToken() {
	    return $this->remember_token;
	}

	public function setRememberToken($value) {
	    $this->remember_token = $value;
	}

	public function getRememberTokenName() {
	    return 'remember_token';
	}

	public static function computeRating($user_id, $type) {
		// get average rating
		$avg = Feedback::where('user_id', $user_id)
					   ->where('type', $type)
					   ->avg('rating');

		$field = "avg_" . $type . "_rating";

		// update
		$user = User::find($user_id);
		$user->$field = $avg;
		$user->save();
	}
}