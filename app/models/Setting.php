<?php

class Setting extends Eloquent {
	public $timestamps = false;
	protected $table = 'user_providers';
	
	public function user() {
        return $this->belongsTo('User');
    }
}