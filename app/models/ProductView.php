<?php

class ProductView extends Eloquent {
	public $timestamps = false;
	protected $table = 'user_product_views';
	
	public function user() {
        return $this->belongsTo('User');
    }

    public function product() {
        return $this->belongsTo('Product');
    }

    public function store() {
        return $this->belongsTo('Store');
    }
}