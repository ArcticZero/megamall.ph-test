<?php

class BannerImageCaption extends Eloquent {
	public $timestamps = false;
	protected $table = "banner_image_captions";
	
	public function banner() {
        return $this->belongsTo('BannerImage', 'banner_image_id');
    }
}