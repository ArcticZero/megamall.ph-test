<?php

class Bookmark extends Eloquent {
	public $timestamps = false;
	protected $table = "user_product_bookmarks";
	
	public function user() {
        return $this->belongsTo('User');
    }

    public function store() {
        return $this->belongsTo('Store');
    }
}