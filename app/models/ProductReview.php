<?php

class ProductReview extends Eloquent {
	public $timestamps = false;
	protected $table = "product_reviews";
	
	public function product() {
        return $this->belongsTo('Product');
    }

    public function user() {
    	return $this->belongsTo('User', 'posted_by');
    }

    public static function getReviews($product_id, $store_id, $skip = false, $approved = true) {
    	$query = static::select('product_reviews.*', 'users.firstname', 'users.lastname', 'users.username')
        			   ->where('product_reviews.product_id', $product_id)
        			   ->where('product_reviews.store_id', $store_id)
    				   ->where('product_reviews.status', $approved ? 1 : 0)
    				   ->join('users', 'product_reviews.posted_by', '=', 'users.id')
                       ->orderBy('product_reviews.posted_on', 'desc')
                       ->take(Config::get('settings.reviews_per_load'));

        // for dynamic loading
        if($skip) {
            $query->skip($skip);
        }
        
        return $query->get();
    }
}