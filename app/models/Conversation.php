<?php

class Conversation extends Eloquent {
	public $timestamps = false;

	public function users() {
        return $this->belongsToMany('User', 'conversation_users')->withPivot('deleted');
    }

    public function messages() {
        return $this->hasMany('Message');
    }
}