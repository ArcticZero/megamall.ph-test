<?php

class Address extends Eloquent {
	public $timestamps = false;
	protected $table = 'user_addresses';

	public function user() {
		return $this->belongsTo('User');
	}

	public function country() {
		return $this->belongsTo('Country');
	}

	public function zone() {
		return $this->belongsTo('Zone');
	}
}