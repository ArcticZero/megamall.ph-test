<?php

class SearchHistory extends Eloquent {
	public $timestamps = false;
	protected $table = 'search_history';
}