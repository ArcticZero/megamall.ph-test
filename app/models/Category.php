<?php

class Category extends Eloquent {
	public function products() {
		return $this->hasMany('Product');
	}

	public function children() {
		return $this->hasMany('Category', 'parent_id')
					->join('category_descriptions', function($join) {
						$join->on('categories.id', '=', 'category_descriptions.category_id')
							 ->on('category_descriptions.language', '=', DB::raw('"' . Config::get('app.locale') . '"'));
					});
	}

	public function parent() {
		return $this->belongsTo('Category', 'parent_id')
					->join('category_descriptions', function($join) {
						$join->on('categories.id', '=', 'category_descriptions.category_id')
							 ->on('category_descriptions.language', '=', DB::raw('"' . Config::get('app.locale') . '"'));
					});
	}

	public function stores() {
		return $this->belongsToMany('Store', 'store_categories');
	}

	public function descriptions() {
		return $this->hasMany('CategoryDescription');
	}

	public static function all($locale = false) {
		// set locale if not given
		$locale = $locale ? $locale : Config::get('app.locale');

		$query = static::select('categories.*', 'category_descriptions.name')
					   ->where('category_descriptions.language', $locale)
		               ->join('category_descriptions', 'categories.id', '=', 'category_descriptions.category_id');
        
        return $query->get();
	}

	public static function get($parent_id = false, $limit = false, $locale = false) {
		// set locale if not given
		$locale = $locale ? $locale : Config::get('app.locale');

		$query = static::select('categories.id', 'permalink', 'category_descriptions.name')
					   ->where('category_descriptions.language', $locale);

		if($parent_id !== false) {
			$query->where('parent_id', $parent_id);
		}
		
	   	$query->orderBy('sort_order', 'asc')
		   	  ->orderBy('category_descriptions.name', 'asc')
	       	  ->join('category_descriptions', 'categories.id', '=', 'category_descriptions.category_id');

        if($limit) {
        	$query->take($limit);
        }
        
        return $query->get();
	}

	public static function info($id, $locale = false) {
		// set locale if not given
		$locale = $locale ? $locale : Config::get('app.locale');

		return static::where('categories.id', $id)
					 ->where('category_descriptions.language', $locale)
					 ->join('category_descriptions', 'categories.id', '=', 'category_descriptions.category_id')
					 ->first();
	}

	public static function getBySlug($permalink, $locale = false) {
		$locale = $locale ? $locale : Config::get('app.locale');

		$query = static::select('categories.id', 'categories.parent_id', 'categories.permalink', 'categories.sort_order', 'category_descriptions.name')
					   ->where('categories.permalink', $permalink)
					   ->join('category_descriptions', function($join) use ($locale) {
					   		$join->on('categories.id', '=', 'category_descriptions.category_id')
					   			 ->on('category_descriptions.language', '=', DB::raw('"' . $locale . '"'));
					   });

		return $query->first();
	}
}