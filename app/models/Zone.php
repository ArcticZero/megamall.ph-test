<?php

class Zone extends Eloquent {
	public $timestamps = false;

	public function country() {
		return $this->belongsTo('Country');
	}

	public function cities() {
		return $this->hasMany('City');
	}
}