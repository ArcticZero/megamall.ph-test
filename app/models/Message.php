<?php

class Message extends Eloquent {
	public $timestamps = false;

	public function user() {
        return $this->belongsTo('User', 'user_id');
    }
}