<?php

class Provider extends Eloquent {
	public $timestamps = false;
	protected $table = 'user_providers', $fillable = array('user_id', 'provider', 'provider_id');
	
	public function user() {
        return $this->belongsTo('User');
    }
}