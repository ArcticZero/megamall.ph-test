<?php

class Product extends Eloquent {
	public $timestamps = false;
	protected $primaryKey = 'product_id';
	
	public function scopeProductKey($query, $store_id, $product_id) {
        return $query->where('store_id', '=', $store_id)->where('product_id', '=', $product_id);
    }

    public function category() {
		return $this->belongsTo('Category');
	}

	public function store() {
		return $this->belongsTo('Store');
	}

	public function images() {
		return $this->hasMany('ProductImage');
	}

	public function reviews() {
		return $this->hasMany('ProductReview');
	}

	public static function grab($limit = false, $filter = false, $permalink = false, $categories = false, $paginate = false, $str = false, $locale = false, $sort = 'products.created_at', $order = 'desc', $cache_key = false, $views_of = false, $views_type = false) {
		// set locale if not given
		$locale = $locale !== false ? $locale : Config::get('app.locale');

		// set cache key if not given
		$cache_key = $cache_key !== false ? $cache_key : 'recent-products-' . ($limit);

		// build query
		$query = static::select('products.*', 'image_url', 'buy_url', 'product_descriptions.name', 'product_descriptions.description', 'products.permalink AS product_permalink', 'category_descriptions.name AS category', 'categories.permalink AS category_permalink', 'stores.store_name', 'stores.store_url', 'stores.permalink AS store_permalink')
					   ->where('products.status', '1');

		// apply id filter if given
		if($filter !== false) {
			$i = 0;

			if(!empty($filter)) {
				foreach($filter as $row) {
					if($i == 0) {
						$query->where(function($q) use ($row) {
							$q->where('products.store_id', $row['store_id'])
							  ->where('products.product_id', $row['product_id']);
						});
					} else {
						$query->orWhere(function($q) use ($row) {
							$q->where('products.store_id', $row['store_id'])
							  ->where('products.product_id', $row['product_id']);
						});
					}

					$i++;
				}
			} else {
				// no results
				$query->where('products.product_id', 0);
			}
		}

		// get permalink if given
		if($permalink !== false) {
			$query->where('products.permalink', $permalink);
		}

		// apply category filter if given
		if($categories !== false) {
			$query->whereIn('products.category_id', $categories);
		}

		// apply search filter if given
		if($str !== false) {
			$dbp = DB::getTablePrefix();
			$query->addSelect(DB::raw("MATCH ({$dbp}product_descriptions.name, {$dbp}product_descriptions.description) AGAINST ('$str') AS dscore"))
				  ->addSelect(DB::raw("MATCH ({$dbp}products.brand) AGAINST ('$str') AS bscore"))
				  ->where(function($q) use ($dbp, $str) {
				      $q->whereRaw("MATCH ({$dbp}product_descriptions.name, {$dbp}product_descriptions.description) AGAINST ('$str')")
				        ->orWhereRaw("MATCH ({$dbp}products.brand) AGAINST ('$str')");
				  });
		}

		// apply joins
		$query->join('product_descriptions', function($join) use ($locale) {
		     	  	$join->on('products.product_id', '=', 'product_descriptions.product_id')
		     		     ->on('products.store_id', '=', 'product_descriptions.store_id')
		     		     ->on('product_descriptions.language', '=', DB::raw('"' . $locale . '"'));
		      })
		      ->leftJoin('categories', 'products.category_id', '=', 'categories.id')
		      ->leftJoin('stores', function($join) {
		      		$join->on('products.store_id', '=', 'stores.id')
		      			 ->on('stores.status', '=', DB::raw(1));
		      })
		      ->leftJoin('category_descriptions', function($join) use ($locale) {
		      		$join->on('categories.id', '=', 'category_descriptions.category_id')
		      			 ->on('category_descriptions.language', '=', DB::raw('"' . $locale . '"'));
		      });

		// additional join if getting product views
		if($views_of !== false) {
			$query->join('user_product_views', function($join) use ($views_of, $views_type) {
				$join->on('products.product_id', '=', 'user_product_views.product_id')
		     		 ->on('products.store_id', '=', 'user_product_views.store_id');

				if($views_type == 'user') {
					$join->on('user_product_views.user_id', '=', DB::raw($views_of));
				} else {
					$join->on('user_product_views.ip_address', '=', DB::raw("'" . $views_of . "'"));
				}
			});

			$query->groupBy('user_product_views.product_id')
				  ->groupBy('user_product_views.store_id');
		}

		// ordering
		if($sort == 'relevance') {
		    $query->orderBy(DB::raw("dscore + bscore"), $order);
		} else {
		    $query->orderBy($sort, $order);
		}

		// set limit if given
		if($limit !== false && $paginate === false) {
        	$query->take($limit);
        }

        // execute
        if($permalink) {
        	return $query->first();
        	//return $query->remember(Config::get('settings.product_cache_time'), 'product-' . $permalink)->first();
        } else if($paginate) {
        	return $query->paginate($limit ? $limit : Config::get('settings.products_per_page'));
        } else {
        	return $query->get();
        	//return $query->remember(Config::get('settings.product_cache_time'), $cache_key)->get();
        }
	}

	public static function getBookmarks($sort = 'products.created_at', $order = 'desc', $show = false) {
		if(Auth::check()) {
			$filter = array();

			foreach(Auth::user()->bookmarks as $row) {
				$filter[] = array('store_id' => $row->store_id, 'product_id' => $row->product_id);
			}

			$cache_key = 'bookmarks-' . Auth::user()->id . '-' . $sort . '-' . $order . '-' . $show;

			// get products based on filter
        	return self::grab($show, $filter, false, false, true, false, false, $sort, $order, $cache_key);
		}

		return false;
	}

	public static function getFeatured($limit = false) {
		$now = date("Y-m-d H:i:s");

		$query = DB::table('featured_products')
				   ->select('product_id', 'store_id')
				   ->where('start_date', '<=', $now)
	               ->where('end_date', '>', $now)
	               ->orderBy('sort_order', 'asc')
	               ->orderBy('end_date', 'asc');


		// set limit if given
        if($limit) {
        	$query->take($limit);
        }
        
        // execute
        $result = $query->get();
        $filter = array();

        foreach($result as $row) {
        	$filter[] = array('store_id' => $row->store_id, 'product_id' => $row->product_id);
        }

        $cache_key = 'featured-products-' . $limit;

        // get products based on filter
        return self::grab($limit, $filter, false, false, false, false, false, 'products.created_at', 'desc', $cache_key);
	}

	public static function getLastViewed($val, $type, $limit = false) {
		$cache_key = 'user-product-views-' . $type . '-' . $val . ($limit !== false ? '-' . $limit : '');
        return self::grab($limit, false, false, false, false, false, false, 'user_product_views.viewed_at', 'desc', $cache_key, $val, $type);
	}

	public static function getByStore($store_id, $sort = 'products.created_at', $order = 'desc', $show = false) {
		$store = Store::find($store_id);

		if($store) {
			$filter = array();

			foreach($store->products as $row) {
				$filter[] = array('store_id' => $row->store_id, 'product_id' => $row->product_id);
			}

			$cache_key = 'store-products-' . $store_id . '-' . $sort . '-' . $order . '-' . $show;

			// get products based on filter
	    	return self::grab($show, $filter, false, false, true, false, false, $sort, $order, $cache_key);
	    }

	    return false;
	}

	public static function getBySlug($slug) {
		return self::grab(false, false, $slug);
	}

	public static function getByIds($store_id, $product_id) {
		$slug = static::where('store_id', $store_id)
					  ->where('product_id', $product_id)
					  ->pluck('permalink');

		return self::getBySlug($slug);
	}

	public static function search($str = false, $category = false, $sort = 'relevance', $order = 'desc', $show = false) {
		if($category) {
			$categories = array($category);

			// get children
			$children = Category::select('id')
								->where('parent_id', $category)
								->get();

			foreach($children as $row) {
				$categories[] = $row['id'];
			}
		} else {
			$categories = false;
		}

		$cache_key = 'product-search-' . $str . '-' . $category . '-' . $sort . '-' . $order . '-' . $show;

		return self::grab($show, false, false, $categories, true, $str, false, $sort, $order, $cache_key);
	}

	public static function computeRating($store_id, $product_id) {
		// get average rating
		$avg = ProductReview::where('store_id', $store_id)
						    ->where('product_id', $product_id)
						    ->avg('rating');

		// update
		$product = Product::where('store_id', $store_id)
						  ->where('product_id', $product_id)
						  ->update(array('avg_rating' => $avg));
	}

	public static function all($columns = array('*')) {
		$cache_key = 'all-products';

		return self::grab(false, false, false, false, true, false, false, 'products.created_at', 'desc', $cache_key);
	}
}