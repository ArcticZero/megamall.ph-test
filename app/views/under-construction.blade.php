@extends('layouts.master')

@section('content')
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="alert alert-danger">
              <strong>{{ Lang::get('forms.error-head') }}</strong> {{ Lang::get('navigation.under-construction') }}
            </div>
          </div>
        </div>
      </div>
@stop