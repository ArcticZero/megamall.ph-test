                <div class="panel panel-primary{{ !isset($row) ? ' new-row' . ($hidden ? ' hidden' : '') : '' }}">
                  <div class="panel-heading clearfix" title="{{ Lang::get('forms.click-to-expand') }}" data-parent="#address-list" data-target="{{ '#collapse' . (isset($row) ? $row->id : '-new' . (isset($x) ? $x : '0')) }}">
                    <div class="option-left">
                      @if(isset($row))
                      {{ Lang::get('user.address') . ' ' . $rownum }}
                      <input type="hidden" name="old[]" value="{{ $row->id }}">
                      @else
                      {{ Lang::get('user.new-address') }}
                      @endif
                    </div>
                    <div class="option-right">
                      <button class="btn btn-danger btn-xs btn-delete{{ !isset($row) ? '-new' : '' }}" type="button" title="{{ Lang::get('forms.delete') }}"><i class="fa fa-fw fa-trash-o"></i>{{ Lang::get('forms.delete') }}</button>
                    </div>
                  </div>
                  <div id="{{ 'collapse' . (isset($row) ? $row->id : '-new' . (isset($x) ? $x : '0')) }}" class="panel-collapse collapse{{ $open ? ' in' : '' }}">
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-4 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ isset($row) ? ($errors->has('firstname.' . $row->id) ? ' has-errors' : '') : (isset($x) ? ($errors->has('new.firstname.' . $x) ? ' has-errors' : '') : '') }}">
                            <label for="{{ 'firstname-' . (isset($row) ? $row->id : (isset($x) ? $x : 0)) }}">{{ Lang::get('user.firstname') }}</label>
                            <input type="text" name="{{ isset($row) ? 'firstname[' . $row->id . ']' : 'new[firstname][' . (isset($x) ? $x : '') . ']' }}" id="{{ 'firstname-' . (isset($row) ? $row->id : (isset($x) ? $x : '0')) }}" class="form-control" value="{{ isset($row) ? Input::old('firstname.' . $row->id , $row->firstname) : (isset($x) ? Input::old('new.firstname.' . $x) : Auth::user()->firstname) }}" required{{ !isset($row) && !isset($x) && !$editable ? ' readonly' : '' }}>
                            @if((isset($row) && $errors->has('firstname.' . $row->id)) || (isset($x) && $errors->has('new.firstname.' . $x)))
                              <div for="{{ 'firstname-' . (isset($row) ? $row->id : (isset($x) ? $x : 0)) }}" class="label label-danger">{{ $errors->first(isset($row) ? 'firstname.' . $row->id : 'new.firstname.' . $x) }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ isset($row) ? ($errors->has('lastname.' . $row->id) ? ' has-errors' : '') : (isset($x) ? ($errors->has('new.lastname.' . $x) ? ' has-errors' : '') : '') }}">
                            <label for="{{ 'lastname-' . (isset($row) ? $row->id : (isset($x) ? $x : 0)) }}">{{ Lang::get('user.lastname') }}</label>
                            <input type="text" name="{{ isset($row) ? 'lastname[' . $row->id . ']' : 'new[lastname][' . (isset($x) ? $x : '') . ']' }}" id="{{ 'lastname-' . (isset($row) ? $row->id : (isset($x) ? $x : '0')) }}" class="form-control" value="{{ isset($row) ? Input::old('lastname.' . $row->id , $row->lastname) : (isset($x) ? Input::old('new.lastname.' . $x) : Auth::user()->lastname) }}" required{{ !isset($row) && !isset($x) && !$editable ? ' readonly' : '' }}>
                            @if((isset($row) && $errors->has('lastname.' . $row->id)) || (isset($x) && $errors->has('new.lastname.' . $x)))
                              <div for="{{ 'lastname-' . (isset($row) ? $row->id : (isset($x) ? $x : 0)) }}" class="label label-danger">{{ $errors->first(isset($row) ? 'lastname.' . $row->id : 'new.lastname.' . $x) }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ isset($row) ? ($errors->has('company.' . $row->id) ? ' has-errors' : '') : (isset($x) ? ($errors->has('new.company.' . $x) ? ' has-errors' : '') : '') }}">
                            <label for="{{ 'company-' . (isset($row) ? $row->id : (isset($x) ? $x : 0)) }}">{{ Lang::get('user.company') }}</label>
                            <input type="text" name="{{ isset($row) ? 'company[' . $row->id . ']' : 'new[company][' . (isset($x) ? $x : '') . ']' }}" id="{{ 'company-' . (isset($row) ? $row->id : (isset($x) ? $x : '0')) }}" class="form-control" value="{{ isset($row) ? Input::old('company.' . $row->id , $row->company) : (isset($x) ? Input::old('new.company.' . $x) : '') }}"{{ !isset($row) && !isset($x) && !$editable ? ' readonly' : '' }}>
                            @if((isset($row) && $errors->has('company.' . $row->id)) || (isset($x) && $errors->has('new.company.' . $x)))
                              <div for="{{ 'company-' . (isset($row) ? $row->id : (isset($x) ? $x : 0)) }}" class="label label-danger">{{ $errors->first(isset($row) ? 'company.' . $row->id : 'new.company.' . $x) }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6 col-sm-8 col-sm-offset-2 col-md-offset-0">
                          <div class="form-group has-feedback{{ isset($row) ? ($errors->has('address_1.' . $row->id) ? ' has-errors' : '') : (isset($x) ? ($errors->has('new.address_1.' . $x) ? ' has-errors' : '') : '') }}">
                            <label for="{{ 'address_1-' . (isset($row) ? $row->id : (isset($x) ? $x : 0)) }}">{{ Lang::get('user.address-1') }}</label>
                            <input type="text" name="{{ isset($row) ? 'address_1[' . $row->id . ']' : 'new[address_1][' . (isset($x) ? $x : '') . ']' }}" id="{{ 'address_1-' . (isset($row) ? $row->id : (isset($x) ? $x : '0')) }}" class="form-control" value="{{ isset($row) ? Input::old('address_1.' . $row->id , $row->address_1) : (isset($x) ? Input::old('new.address_1.' . $x) : '') }}"{{ !isset($row) && !isset($x) && !$editable ? ' readonly' : '' }}>
                            @if((isset($row) && $errors->has('address_1.' . $row->id)) || (isset($x) && $errors->has('new.address_1.' . $x)))
                              <div for="{{ 'address_1-' . (isset($row) ? $row->id : (isset($x) ? $x : 0)) }}" class="label label-danger">{{ $errors->first(isset($row) ? 'address_1.' . $row->id : 'new.address_1.' . $x) }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-8 col-sm-offset-2 col-md-offset-0">
                          <div class="form-group has-feedback{{ isset($row) ? ($errors->has('address_2.' . $row->id) ? ' has-errors' : '') : (isset($x) ? ($errors->has('new.address_2.' . $x) ? ' has-errors' : '') : '') }}">
                            <label for="{{ 'address_2-' . (isset($row) ? $row->id : (isset($x) ? $x : 0)) }}">{{ Lang::get('user.address-2') }}</label>
                            <input type="text" name="{{ isset($row) ? 'address_2[' . $row->id . ']' : 'new[address_2][' . (isset($x) ? $x : '') . ']' }}" id="{{ 'address_2-' . (isset($row) ? $row->id : (isset($x) ? $x : '0')) }}" class="form-control" value="{{ isset($row) ? Input::old('address_2.' . $row->id , $row->address_2) : (isset($x) ? Input::old('new.address_2.' . $x) : '') }}"{{ !isset($row) && !isset($x) && !$editable ? ' readonly' : '' }}>
                            @if((isset($row) && $errors->has('address_2.' . $row->id)) || (isset($x) && $errors->has('new.address_2.' . $x)))
                              <div for="{{ 'address_2-' . (isset($row) ? $row->id : (isset($x) ? $x : 0)) }}" class="label label-danger">{{ $errors->first(isset($row) ? 'address_2.' . $row->id : 'new.address_2.' . $x) }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6 col-sm-8 col-sm-offset-2 col-md-offset-0">
                          <div class="form-group has-feedback{{ isset($row) ? ($errors->has('city.' . $row->id) ? ' has-errors' : '') : (isset($x) ? ($errors->has('new.city.' . $x) ? ' has-errors' : '') : '') }}">
                            <label for="{{ 'city-' . (isset($row) ? $row->id : (isset($x) ? $x : 0)) }}">{{ Lang::get('user.city') }}</label>
                            <input type="text" name="{{ isset($row) ? 'city[' . $row->id . ']' : 'new[city][' . (isset($x) ? $x : '') . ']' }}" id="{{ 'city-' . (isset($row) ? $row->id : (isset($x) ? $x : '0')) }}" class="form-control" value="{{ isset($row) ? Input::old('city.' . $row->id , $row->city) : (isset($x) ? Input::old('new.city.' . $x) : '') }}"{{ !isset($row) && !isset($x) && !$editable ? ' readonly' : '' }}>
                            @if((isset($row) && $errors->has('city.' . $row->id)) || (isset($x) && $errors->has('new.city.' . $x)))
                              <div for="{{ 'city-' . (isset($row) ? $row->id : (isset($x) ? $x : 0)) }}" class="label label-danger">{{ $errors->first(isset($row) ? 'city.' . $row->id : 'new.city.' . $x) }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-8 col-sm-offset-2 col-md-offset-0">
                          <div class="form-group has-feedback{{ isset($row) ? ($errors->has('postcode.' . $row->id) ? ' has-errors' : '') : (isset($x) ? ($errors->has('new.postcode.' . $x) ? ' has-errors' : '') : '') }}">
                            <label for="{{ 'postcode-' . (isset($row) ? $row->id : (isset($x) ? $x : 0)) }}">{{ Lang::get('user.postcode') }}</label>
                            <input type="text" name="{{ isset($row) ? 'postcode[' . $row->id . ']' : 'new[postcode][' . (isset($x) ? $x : '') . ']' }}" id="{{ 'postcode-' . (isset($row) ? $row->id : (isset($x) ? $x : '0')) }}" class="form-control" value="{{ isset($row) ? Input::old('postcode.' . $row->id , $row->postcode) : (isset($x) ? Input::old('new.postcode.' . $x) : '') }}"{{ !isset($row) && !isset($x) && !$editable ? ' readonly' : '' }}>
                            @if((isset($row) && $errors->has('postcode.' . $row->id)) || (isset($x) && $errors->has('new.postcode.' . $x)))
                              <div for="{{ 'postcode-' . (isset($row) ? $row->id : (isset($x) ? $x : 0)) }}" class="label label-danger">{{ $errors->first(isset($row) ? 'postcode.' . $row->id : 'new.postcode.' . $x) }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6 col-sm-8 col-sm-offset-2 col-md-offset-0">
                          <div class="form-group has-feedback{{ isset($row) ? ($errors->has('country_id.' . $row->id) ? ' has-errors' : '') : (isset($x) ? ($errors->has('new.country_id.' . $x) ? ' has-errors' : '') : '') }}">
                            <label for="{{ 'country_id-' . (isset($row) ? $row->id : (isset($x) ? $x : 0)) }}">{{ Lang::get('user.country') }}</label>
                            <select name="{{ isset($row) ? 'country_id[' . $row->id . ']' : 'new[country_id][' . (isset($x) ? $x : '') . ']' }}" id="{{ 'country_id-' . (isset($row) ? $row->id : (isset($x) ? $x : '0')) }}" class="country-select form-control" required{{ !isset($row) && !isset($x) && !$editable ? ' readonly' : '' }}>
                              <option value="">{{ Lang::get('user.select-country') }}</option>
                              @foreach($countries as $country)
                              <option value="{{ $country->country_id }}"{{ (isset($row) && $country->country_id == Input::old('country_id.' . $row->id, $row->country_id)) || (isset($x) && $country->country_id == Input::old('new.country_id.' . $x)) ? ' selected="selected"' : '' }}>{{ $country->name }}</option>
                              @endforeach
                            </select>
                            @if((isset($row) && $errors->has('country_id.' . $row->id)) || (isset($x) && $errors->has('new.country_id.' . $x)))
                              <div for="{{ 'country_id-' . (isset($row) ? $row->id : (isset($x) ? $x : 0)) }}" class="label label-danger">{{ $errors->first(isset($row) ? 'country_id.' . $row->id : 'new.country_id.' . $x) }}</div>
                            @endif
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-8 col-sm-offset-2 col-md-offset-0">
                          <div class="form-group has-feedback{{ isset($row) ? ($errors->has('zone_id.' . $row->id) ? ' has-errors' : '') : (isset($x) ? ($errors->has('new.zone_id.' . $x) ? ' has-errors' : '') : '') }}">
                            <label for="{{ 'zone_id-' . (isset($row) ? $row->id : (isset($x) ? $x : 0)) }}">{{ Lang::get('user.zone') }}</label>
                            <select name="{{ isset($row) ? 'zone_id[' . $row->id . ']' : 'new[zone_id][' . (isset($x) ? $x : '') . ']' }}" id="{{ 'zone_id-' . (isset($row) ? $row->id : (isset($x) ? $x : '0')) }}" class="zone-select form-control" data-value="{{ isset($row) ? Input::old('zone_id.' . $row->id , $row->zone_id) : (isset($x) ? Input::old('new.zone_id.' . $x) : '') }}" disabled="disabled"{{ !isset($row) && !isset($x) && !$editable ? ' readonly' : '' }}>
                              <option value="">{{ Lang::get('user.select-country') }}</option>
                            </select>
                            @if((isset($row) && $errors->has('zone_id.' . $row->id)) || (isset($x) && $errors->has('new.zone_id.' . $x)))
                              <div for="{{ 'zone_id-' . (isset($row) ? $row->id : (isset($x) ? $x : 0)) }}" class="label label-danger">{{ $errors->first(isset($row) ? 'zone_id.' . $row->id : 'new.zone_id.' . $x) }}</div>
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>