@extends('layouts.master')

@section('scripts')
  @include('includes.validator')
  {? $add_js[] = '/js/vendor/jquery.validate.min.js' ?}
  {? $add_js[] = '/js/forgotpass.js' ?}
@stop

@section('breadcrumbs')
  @include('includes.breadcrumbs')
@stop

@section('content')
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
              <div class="panel-heading">{{ Lang::get('user.header-forgotpass') }}</div>
              <div class="panel-body">
                {{ Form::open(array('url' => 'forgot-password', 'id' => 'form-forgotpass')) }}
                  <div class="row">
                    <div class="col-md-6 col-md-offset-2 col-sm-8 col-sm-offset-2">
                      <div class="form-group has-feedback{{ $errors->has('email') ? ' has-errors' : '' }}">
                        <label class="sr-only" for="forgotpass-email">{{ Lang::get('user.email') }}</label>
                        <input type="email" name="email" id="forgotpass-email" class="form-control" value="{{ Input::old('email') }}" placeholder="{{ Lang::get('user.email') }}" required>
                        @if($errors->has('email'))
                          <div for="forgotpass-email" class="label label-danger">{{ $errors->first('email') }}</div>
                          <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                        @endif
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-8 col-md-offset-0 col-sm-offset-2">
                      <input class="hide" type="text" name="honeypot" value="">
                      <button type="submit" name="submit" class="btn btn-primary btn-block">{{ Lang::get('forms.submit') }}</button>
                    </div>
                  </div>
                {{ Form::close() }}
              </div>
            </div>
          </div>
        </div>
      </div>
@stop