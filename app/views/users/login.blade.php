@extends('layouts.master')

@section('scripts')
  @include('includes.validator')
  {? $add_js[] = '/js/vendor/jquery.validate.min.js' ?}
@stop

@section('breadcrumbs')
  @include('includes.breadcrumbs')
@stop

@section('content')
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="panel panel-primary">
              <div class="panel-heading">{{ Lang::get('user.header-social') }}</div>
              <div class="panel-body text-center">
                <div class="row">
                  <div class="social-btn-box col-md-6 col-md-offset-3 col-ms-6 col-ms-offset-0 col-sm-6 col-sm-offset-0">
                    <a href="{{ url('login/facebook') }}" class="btn btn-facebook btn-block"><i class="fa fa-facebook fa-fw"></i> | {{ Lang::get('forms.login-fb') }}</a>
                  </div>
                  <div class="social-btn-box col-md-6 col-md-offset-3 col-ms-6 col-ms-offset-0 col-sm-6 col-sm-offset-0">
                    <a href="{{ url('login/google') }}" class="btn btn-google-plus btn-block"><i class="fa fa-google-plus fa-fw"></i> | {{ Lang::get('forms.login-google') }}</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="panel panel-danger">
              <div class="panel-heading">{{ Lang::get('user.header-login') }}</div>
              <div class="panel-body">
                {{ Form::open(array('route' => 'login', 'id' => 'form-login')) }}
                  <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                      <div class="form-group has-feedback{{ $errors->has('auth') ? ' has-errors' : '' }}">
                        <label for="login-auth">{{ Lang::get('forms.email-username-ph') }}</label>
                        <input type="text" name="auth" id="login-auth" class="form-control" value="{{ Input::old('auth') }}" required>
                        @if($errors->has('auth'))
                          <div for="login-auth" class="label label-danger">{{ $errors->first('auth') }}</div>
                          <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                      <div class="form-group has-feedback{{ $errors->has('password') ? ' has-errors' : '' }}">
                        <label for="login-password">{{ Lang::get('user.password') }}</label>
                        <input type="password" minlength="8" name="password" id="login-password" class="form-control" required>
                        <div><small><a href="{{ url('forgot-password') }}">{{ Lang::get('user.forgot-pw-link') }}</a></small></div>
                        @if($errors->has('password'))
                          <div for="login-password" class="label label-danger">{{ $errors->first('password') }}</div>
                          <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                      <div class="form-group">
                        <label>
                          <input type="checkbox" name="remember"><small> {{ Lang::get('forms.remember-me') }}</small>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3 col-sm-8 col-md-offset-2 col-sm-offset-2">
                      <input class="hide" type="text" name="honeypot" value="">
                      <button type="submit" name="submit" class="btn btn-danger btn-block">{{ Lang::get('forms.login') }}</button>
                    </div>
                  </div>
                {{ Form::close() }}
              </div>
            </div>
          </div>
        </div>
      </div>
@stop