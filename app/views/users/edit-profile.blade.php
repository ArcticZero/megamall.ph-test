@extends('layouts.master')

@section('styles')
  @if($info->store)
  {? $add_css[] = '/css/summernote.css' ?}
  @endif
@stop

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {
        pwlabels = ["{{ Lang::get('forms.pw-weak') }}", "{{ Lang::get('forms.pw-fair') }}", "{{ Lang::get('forms.pw-medium') }}", "{{ Lang::get('forms.pw-good') }}", "{{ Lang::get('forms.pw-strong') }}"];
        msgWeakPw = "{{ Lang::get('forms.error-pw-strength') }}";
        msgOldPwRequired = "{{ Lang::get('forms.error-oldpw-required') }}";
    });
  </script>
  @if($info->store && $info->store->status == 1)
  {? $add_js[] = '/js/vendor/summernote.min.js' ?}
  @endif
  @include('includes.validator')
  {? $add_js[] = '/js/vendor/jquery.validate.min.js' ?}
  {? $add_js[] = '/js/vendor/pwstrength-bootstrap-1.1.5.js' ?}
  {? $add_js[] = '/js/edit-profile.js' ?}
@stop

@section('breadcrumbs')
  @include('includes.breadcrumbs')
@stop

@section('content')
      <div class="container" itemscope itemtype="http://schema.org/Person">
        <div class="row">
          <div class="col-md-12">
            <h3 itemprop="name">{{ Lang::get('user.edit-profile') }}</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            @include('users.sidebar')
          </div>
          <div class="col-md-8">
            {{ Form::open(array('url' => 'profile/edit', 'id' => 'form-edit-profile')) }}
              <ul class="nav nav-tabs">
                <li class="active"><a href="#user-info" data-toggle="tab">{{ Lang::get('user.my-profile') }}</a></li>
                @if($info->store && $info->store->status == 1)
                <li><a href="#store-info" data-toggle="tab">{{ Lang::get('store.store-info') }}</a></li>
                @endif
                <li class="pull-right hidden-xs">
                  <input type="submit" class="btn btn-danger" value="{{ Lang::get('forms.save-changes') }}">
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane fade in active" id="user-info">
                  <div class="panel panel-primary">
                    <div class="panel-heading">{{ Lang::get('user.basic-info') }}</div>
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ $errors->has('firstname') ? ' has-errors' : '' }}">
                            <label for="profile-firstname">{{ Lang::get('user.firstname') }}</label>
                            <input type="text" name="firstname" id="profile-firstname" class="form-control" value="{{ Input::old('firstname', $info->firstname) }}" required>
                            @if($errors->has('firstname'))
                              <div for="profile-firstname" class="label label-danger">{{ $errors->first('firstname') }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ $errors->has('lastname') ? ' has-errors' : '' }}">
                            <label for="profile-lastname">{{ Lang::get('user.lastname') }}</label>
                            <input type="text" name="lastname" id="profile-lastname" class="form-control" value="{{ Input::old('lastname', $info->lastname) }}" required>
                            @if($errors->has('lastname'))
                              <div for="profile-lastname" class="label label-danger">{{ $errors->first('lastname') }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12 col-sm-8 col-sm-offset-2 col-md-offset-0">
                          <div class="form-group has-feedback{{ $errors->has('address_id') ? ' has-errors' : '' }}">
                            <label for="profile-address-id">{{ Lang::get('user.default-address') }}</label>
                            @if(count($addresses) > 0)
                            <select name="address_id" id="profile-address-id" class="form-control selectpicker show-menu-arrow">
                              <option value="">[{{ Lang::get('forms.none') }}]</option>
                              @foreach($addresses as $id => $address)
                              {{ '<option value="' . $id . '"' . ($id == Input::old('address_id', $info->address_id) ? ' selected="selected"' : '') . '>' .$address . '</option>' }}
                              @endforeach
                            </select>
                            @else
                            <div class="well">
                              <i class="fa fa-exclamation-triangle fa-fw"></i>
                              {{ Lang::get('user.no-addresses', array('add-one' => link_to('profile/addresses', Lang::get('user.add-address')))) }}
                            </div>
                            @endif
                            @if($errors->has('address_id'))
                              <div for="profile-address-id" class="label label-danger">{{ $errors->first('address_id') }}</div>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ $errors->has('telephone') ? ' has-errors' : '' }}">
                            <label for="profile-telephone">{{ Lang::get('user.telephone') }}</label>
                            <input type="text" name="telephone" id="profile-telephone" class="form-control" value="{{ Input::old('telephone', $info->telephone) }}">
                            @if($errors->has('telephone'))
                              <div for="profile-telephone" class="label label-danger">{{ $errors->first('telephone') }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ $errors->has('fax') ? ' has-errors' : '' }}">
                            <label for="profile-fax">{{ Lang::get('user.fax') }}</label>
                            <input type="text" name="fax" id="profile-fax" class="form-control" value="{{ Input::old('fax', $info->fax) }}">
                            @if($errors->has('fax'))
                              <div for="profile-fax" class="label label-danger">{{ $errors->first('fax') }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ $errors->has('ym_id') ? ' has-errors' : '' }}">
                            <label for="profile-ym-id"><i class="fa fa-yahoo fa-fw yahoo"></i>{{ Lang::get('user.yahoo-id') }}</label>
                            <input type="text" name="ym_id" id="profile-ym-id" class="form-control" value="{{ Input::old('ym_id', $info->ym_id) }}">
                            @if($errors->has('ym_id'))
                              <div for="profile-ym-id" class="label label-danger">{{ $errors->first('ym_id') }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ $errors->has('skype_id') ? ' has-errors' : '' }}">
                            <label for="profile-skype-id"><i class="fa fa-skype fa-fw skype"></i>{{ Lang::get('user.skype-id') }}</label>
                            <input type="text" name="skype_id" id="profile-skype-id" class="form-control" value="{{ Input::old('skype_id', $info->skype_id) }}">
                            @if($errors->has('skype_id'))
                              <div for="profile-skype-id" class="label label-danger">{{ $errors->first('skype_id') }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-primary">
                    <div class="panel-heading">{{ Lang::get('user.credentials') }}</div>
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ $errors->has('current_password') ? ' has-errors' : '' }}">
                            <label for="profile-current-password">{{ Lang::get('user.current-password') }}</label>
                            <input type="password" name="current_password" id="profile-current-password" class="form-control" readonly>
                            @if($errors->has('current_password'))
                              <div for="profile-password" class="label label-danger">{{ $errors->first('current_password') }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ $errors->has('email') ? ' has-errors' : '' }}">
                            <label for="profile-email">{{ Lang::get('user.email') }} <span title="{{ Lang::get('user.info-change-email') }}" data-toggle="tooltip" data-placement="right" data-html="true" class="glyphicon glyphicon-info-sign"></span></label>
                            <input type="email" name="email" data-current="{{ $info->email }}" id="profile-email" class="form-control" value="{{ Input::old('email', $info->email) }}" required>
                            @if($errors->has('email'))
                              <div for="profile-email" class="label label-danger">{{ $errors->first('email') }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ $errors->has('username') ? ' has-errors' : '' }}">
                            <label for="profile-username">{{ Lang::get('user.username') }} <span title="{{ Lang::get('user.info-username') }}" data-toggle="tooltip" data-placement="right" data-html="true" class="glyphicon glyphicon-info-sign"></span></label>
                            <input type="text" name="username" placeholder="{{ Lang::get('forms.optional') }}" minlength="4" data-current="{{ $info->username }}" id="profile-username" class="form-control" value="{{ Input::old('username', $info->username) }}">
                            @if($errors->has('username'))
                              <div for="profile-password" class="label label-danger">{{ $errors->first('username') }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ $errors->has('password') ? ' has-errors' : '' }}">
                            <label for="profile-password">{{ Lang::get('user.new-password') }} <span title="{{ Lang::get('user.info-pw-length', array('num' => 8)) . '<br>' . Lang::get('user.info-pw-not-email') . '<br>' . Lang::get('user.info-pw-not-name') . '<br>' . Lang::get('user.info-pw-patterns') }}" data-toggle="tooltip" data-placement="right" data-html="true" class="glyphicon glyphicon-info-sign"></span></label>
                            <input type="password" placeholder="{{ Lang::get('forms.leave-blank-unchanged') }}" minlength="8" name="password" id="profile-password" class="form-control">
                            @if($errors->has('password'))
                              <div for="profile-password" class="label label-danger">{{ $errors->first('password') }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2" id="pw-feedback">
                          <small>{{ Lang::get('forms.pw-strength') }}</small>
                          <div class="pw-strength"></div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback">
                            <label for="profile-confirm-password">{{ Lang::get('user.confirm-password') }}</label>
                            <input type="password" placeholder="{{ Lang::get('forms.leave-blank-unchanged') }}" name="password_confirmation" id="profile-confirm-password" class="form-control">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="well">
                            <i class="fa fa-exclamation-triangle fa-fw"></i>
                            {{ Lang::get('user.cred-warning') }}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                @if($info->store && $info->store->status == 1)
                <div class="tab-pane fade" id="store-info">
                  <div class="panel panel-primary">
                    <div class="panel-heading clearfix">
                      {{ Lang::get('store.identification') }}
                      <div class="pull-right">
                        <button type="button" class="btn btn-danger btn-xs" id="btn-unlock-store-fields">
                          <i class="fa fa-lock fa-fw"></i>
                          {{ Lang::get('store.enable-id-change') }}
                        </button>
                      </div>
                    </div>
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ $errors->has('store_name') ? ' has-errors' : '' }}">
                            <label for="profile-store-name">{{ Lang::get('store.store-name') }}</label>
                            <input type="text" name="store_name" id="profile-store-name" class="form-control locked" minlength="4" value="{{ Input::old('store_name', $info->store->store_name) }}" required readonly>
                            @if($errors->has('store_name'))
                              <div for="profile-store-name" class="label label-danger">{{ $errors->first('store_name') }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ $errors->has('store_url') ? ' has-errors' : '' }}">
                            <label for="profile-store-url">{{ Lang::get('store.store-url') }}</label>
                            <div class="input-group">
                              <input type="text" name="store_url" id="profile-store-url" class="form-control locked" minlength="4" value="{{ Input::old('store_url', $info->store->store_url) }}" required readonly>
                              <span class="input-group-addon"><strong>{{ Lang::get('navigation.megamall-subdomain') }}</strong></span>
                            </div>
                            @if($errors->has('store_url'))
                              <div for="profile-store-url" class="label label-danger">{{ $errors->first('store_url') }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="well">
                            <i class="fa fa-exclamation-triangle fa-fw"></i>
                            {{ Lang::get('store.id-change-warning') }}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-primary">
                    <div class="panel-heading">{{ Lang::get('store.store-description') }}</div>
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-12 col-sm-8 col-sm-offset-2 col-md-offset-0">
                          <div class="form-group has-feedback{{ $errors->has('store_description') ? ' has-errors' : '' }}">
                            <textarea name="store_description" class="form-control" id="store-description" rows="8">{{ Input::old('store_description', $info->store->description) }}</textarea>
                            @if($errors->has('store_description'))
                              <div for="store-description" class="label label-danger">{{ $errors->first('store_description') }}</div>
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-primary">
                    <div class="panel-heading">{{ Lang::get('store.related-categories') }}</div>
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-12 col-sm-8 col-sm-offset-2 col-md-offset-0">
                          <div class="form-group has-feedback{{ $errors->has('categories') ? ' has-errors' : '' }}">
                            <label for="store-categories">{{ Lang::get('store.select-categories') }}</label>
                            @if(count($search_cats) > 0)
                            <select name="categories[]" id="store-categories" class="selectpicker show-menu-arrow form-control" data-live-search="true" multiple>
                              @foreach($search_cats as $cat)
                              <optgroup label="{{ $cat['name'] }}">
                                @foreach($cat->children as $subcat)
                                <option value="{{ $subcat['id'] }}"{{ is_array(Input::old('categories', $store_categories)) && in_array($subcat['id'], Input::old('categories', $store_categories)) ? ' selected="selected"' : '' }}>{{ $subcat['name'] }}</option>
                                @endforeach
                              </optgroup>
                              @endforeach
                            </select>
                            @endif
                            @if($errors->has('categories'))
                              <div for="store-categories" class="label label-danger">{{ $errors->first('categories') }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-primary">
                    <div class="panel-heading">{{ Lang::get('store.contact-info') }}</div>
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-12 col-sm-8 col-sm-offset-2 col-md-offset-0">
                          <div class="form-group has-feedback{{ $errors->has('store_address_id') ? ' has-errors' : '' }}">
                            <label for="profile-store-address-id">{{ Lang::get('store.physical-address') }}</label>
                            @if(count($addresses) > 0)
                            <select name="store_address_id" id="profile-store-address-id" class="form-control selectpicker show-menu-arrow">
                              <option value="">[{{ Lang::get('forms.not-applicable') }}]</option>
                              @foreach($addresses as $id => $address)
                              {{ '<option value="' . $id . '"' . ($id == Input::old('store_address_id', $info->store->address_id) ? ' selected="selected"' : '') . '>' .$address . '</option>' }}
                              @endforeach
                            </select>
                            @else
                            <div class="well">
                              <i class="fa fa-exclamation-triangle fa-fw"></i>
                              {{ Lang::get('user.no-addresses', array('add-one' => link_to('profile/addresses', Lang::get('user.add-address')))) }}
                            </div>
                            @endif
                            @if($errors->has('store_address_id'))
                              <div for="profile-address-id" class="label label-danger">{{ $errors->first('address_id') }}</div>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ $errors->has('store_telephone') ? ' has-errors' : '' }}">
                            <label for="profile-store-telephone">{{ Lang::get('user.telephone') }}</label>
                            <input type="text" name="store_telephone" id="profile-store-telephone" class="form-control" value="{{ Input::old('store_telephone', $info->store->telephone) }}">
                            @if($errors->has('store_telephone'))
                              <div for="profile-store-telephone" class="label label-danger">{{ $errors->first('store_telephone') }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ $errors->has('store_fax') ? ' has-errors' : '' }}">
                            <label for="profile-store-fax">{{ Lang::get('user.fax') }}</label>
                            <input type="text" name="store_fax" id="profile-store-fax" class="form-control" value="{{ Input::old('store_fax', $info->store->fax) }}">
                            @if($errors->has('store_fax'))
                              <div for="profile-store-fax" class="label label-danger">{{ $errors->first('store_fax') }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ $errors->has('store_email') ? ' has-errors' : '' }}">
                            <label for="profile-store-email">{{ Lang::get('user.email') }}</label>
                            <input type="email" name="store_email" id="profile-store-email" class="form-control" value="{{ Input::old('store_email', $info->store->email) }}">
                            @if($errors->has('store_email'))
                              <div for="profile-store-email" class="label label-danger">{{ $errors->first('store_email') }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="visible-xs">
                  <input type="submit" class="btn btn-danger btn-block" value="{{ Lang::get('forms.save-changes') }}">
                  <input class="hide" type="text" name="honeypot" value="">
                </div>
                @endif
              </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>
@stop