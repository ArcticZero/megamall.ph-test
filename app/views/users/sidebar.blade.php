            <div class="list-group-collapse hidden-xs">
              <div class="list-group-navbar navbar-default">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#user-sidebar">
                  <span class="sr-only">{{ Lang::get('navigation.toggle') }}</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand hidden-sm hidden-md hidden-lg" href="#">{{ Lang::get('user.user-options') }}</a>
              </div>
              @if(!Auth::user()->store && Auth::user()->store->status == 1 && Route::currentRouteAction() != 'StoreController@create')
              <div class="list-group collapse navbar-collapse">
                <a href="{{ url('stores/create') }}" class="list-group-item list-group-item-danger">
                  <i class="fa fa-tags fa-fw"></i>{{ Lang::get('store.create-your-store') }}
                </a>
              </div>
              @endif
              <div class="list-group collapse navbar-collapse" id="user-sidebar">
                <a href="{{ url('profile') }}" class="list-group-item">
                  <i class="fa fa-dashboard fa-fw red"></i>{{ Lang::get('user.dashboard') }}
                  @if($unread['fb'] > 0)
                  <span class="badge badge-danger unread-fb">{{ $unread['fb'] }}</span>
                  @endif
                </a>
                <a href="{{ url('profile/edit') }}" class="list-group-item">
                  <i class="fa fa-user fa-fw red"></i>{{ Auth::user()->store && Auth::user()->store->status == 1 ? Lang::get('user.edit-profile-store') : Lang::get('user.edit-profile') }}
                </a>
                <a href="{{ url('messages') }}" class="list-group-item">
                  <i class="fa fa-envelope fa-fw red"></i>{{ Lang::get('user.messages') }}
                  @if($unread['pm'] > 0)
                  <span class="badge badge-danger unread-pm">{{ $unread['pm'] }}</span>
                  @endif
                </a>
                <a href="{{ url('profile/addresses') }}" class="list-group-item">
                  <i class="fa fa-book fa-fw red"></i>{{ Lang::get('user.manage-addresses') }}
                </a>
                <a href="{{ url('profile/bookmarks') }}" class="list-group-item">
                  <i class="fa fa-bookmark fa-fw red"></i>{{ Lang::get('user.manage-bookmarks') }}
                </a>
                <!--
                <a href="{{ url('profile/settings') }}" class="list-group-item">
                  <i class="fa fa-cog fa-spin fa-fw red"></i>{{ Lang::get('user.settings') }}
                </a>
                -->
                <a href="{{ url('logout') }}" class="list-group-item">
                  <i class="fa fa-sign-out fa-fw red"></i>{{ Lang::get('navigation.logout') }}
                </a>
              </div>
            </div>