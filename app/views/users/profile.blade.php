@extends('layouts.master')

@section('styles')
  @if(Auth::check() && $info->id != Auth::user()->id)
  {? $add_css[] = '/css/summernote.css' ?}
  @endif
@stop

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {
        feedback_pages = {{ $feedback_pages }};
        sell_feedback_pages = {{ $sell_feedback_pages }};
        buy_feedback_pages = {{ $buy_feedback_pages }};
        given_feedback_pages = {{ $given_feedback_pages }};
        feedback_ids = '{{ $idstring }}';
        userid = {{ $info->id }};
    });
  </script>
  {? $add_js[] = '/js/vendor/jquery.raty.js' ?}
  @if(Auth::check() && $info->id != Auth::user()->id)
  {? $add_js[] = '/js/vendor/summernote.min.js' ?}
  @endif
  {? $add_js[] = '/js/user-profile.js' ?}
@stop

@section('breadcrumbs')
  @include('includes.breadcrumbs')
@stop

@section('content')
      <div class="container" itemscope itemtype="http://schema.org/Person">
        <div class="row">
          <div class="col-md-12">
            <h3 itemprop="name">{{ $info->username ? $info->username : $info->firstname . ' ' . $info->lastname }}</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="list-group item-info">
              <a class="list-group-item no-link">
                <i class="fa fa-home fa-fw blue" title="{{ Lang::get('user.address') }}"></i>
                {{ isset($address) ? implode(", ", array_filter(array($address->city, $address->zone->name, $address->country->name))) : '<em>' . Lang::get('user.no-address') . '</em>' }}
              </a>
              @if(isset($address) && $address->lat && $address->lng)
              <a class="list-group-item no-link hidden">
                Ze map
              </a>
              @endif
              <a class="list-group-item no-link" title="{{ Lang::get('user.telephone') }}">
                <i class="fa fa-phone fa-fw blue"></i>
                {{ $info->telephone ? $info->telephone : '<em>' . Lang::get('user.no-telephone') . '</em>' }}
              </a>
              @if($info->fax)
              <a class="list-group-item no-link" title="{{ Lang::get('user.fax') }}">
                <i class="fa fa-fax fa-fw blue"></i>
                {{ $info->fax }}
              </a>
              @endif
              @if((Auth::check() && Auth::user()->id != $info->id) || Auth::guest())
              <a href="{{ url('messages/new/' . $info->id) }}" class="list-group-item list-group-item-danger">
                <i class="fa fa-envelope fa-fw"></i>{{ Lang::get('user.send-message') }}
              </a>
              @if($info->ym_id)
              <a href="ymsgr:sendim?{{ $info->ym_id }}" class="list-group-item list-group-item-yahoo">
                <i class="fa fa-yahoo fa-fw"></i>{{ Lang::get('user.chat-on-yahoo', array('id' => '<small>' . $info->ym_id . '</small>')) }}
              </a>
              @endif
              @if($info->skype_id)
              <a href="skype:{{ $info->skype_id }}?call" class="list-group-item list-group-item-skype">
                <i class="fa fa-skype fa-fw"></i>{{ Lang::get('user.call-on-skype', array('id' => '<small>' . $info->skype_id . '</small>')) }}
              </a>
              @endif
              @endif
            </div>
            @if($info->store && $info->store->status == 1)
            <h4>{{ Lang::get('store.store-info') }}</h4>
            @include('stores.sidebar', array('store' => $info->store))
            @endif
            @if(Auth::check() && Auth::user()->id == $info->id)
            <h4 class="hidden-xs">{{ Lang::get('user.user-options')}}</h4>
            @include('users.sidebar')
            @endif
            @if((Auth::check() && Auth::user()->id != $info->id) || Auth::guest())
            <div class="list-group">
              <a data-type="user" data-item="{{ $info->id }}" href="{{ url('help/report/user/' . $info->id) }}" class="list-group-item list-group-item-danger btn-report">
                <i class="fa fa-exclamation-circle fa-fw"></i>
                {{ Lang::get('user.report-user') }}
              </a>
            </div>
            @endif
          </div>
          <div id="user-feedback" class="col-md-8">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#all-feedback" data-toggle="tab">{{ Lang::get('user.all-feedback') }} <span class="badge">{{ $total_feedback }}</span></a></li>
              <li><a href="#sell-feedback" data-toggle="tab">{{ Lang::get('user.sell-feedback') }} <span class="badge">{{ $total_sell_feedback }}</span></a></li>
              <li><a href="#buy-feedback" data-toggle="tab">{{ Lang::get('user.buy-feedback') }} <span class="badge">{{ $total_buy_feedback }}</span></a></li>
              <li><a href="#given-feedback" data-toggle="tab">{{ Lang::get('user.given-feedback') }} <span class="badge">{{ $total_given_feedback }}</span></a></li>
              @if(Auth::check() && $info->id != Auth::user()->id)
              <li class="pull-right hidden-xs"><button class="btn btn-primary btn-post-feedback"><span class="glyphicon glyphicon-pencil"></span>{{ Lang::get('user.post-feedback') }}</button></li>
              @endif
            </ul>
            <div class="tab-content">
              <div class="tab-pane fade in active" id="all-feedback">
                @if($total_feedback == 0 || ($total_feedback > 0 && Auth::check()))
                <div id="feedback-notice" class="alert {{ $total_feedback == 0 ? 'alert-danger' : 'hidden' }}">
                  @if($total_feedback == 0)
                    <strong>{{ Lang::get('user.no-feedback') }}</strong>
                    @if(Auth::check() && $info->id != Auth::user()->id)
                    &nbsp;<a href="#" class="link-post-feedback">{{ Lang::get('user.add-yours') }}</a>
                    @endif
                  @endif
                </div>
                @endif
                @if(Auth::check() && $info->id != Auth::user()->id)
                <div id="form-post-feedback" class="hidden">
                  {{ Form::open(array('url' => 'review/user/save')) }}
                  <div class="panel panel-danger user-feedback">
                    <div class="panel-heading">
                      {{ Lang::get('user.post-feedback') }}
                    </div>
                    <div class="panel-body">
                      <div id="error-box" class="alert alert-danger hidden">
                        <strong>{{ Lang::get('forms.error-header') }}</strong>
                        <ul></ul>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="#feedback-rating">{{ Lang::get('user.feedback-rating') }}:</label>
                            <div id="feedback-rating" class="rating"></div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>{{ Lang::get('user.feedback-type') }}:</label>
                            <div>
                              <label class="radio-inline">
                                <input type="radio" name="type" value="buy" id="feedback-type-buy"{{ Input::old('type') != "sell" ? ' checked="checked"' : '' }}>{{ Lang::get('user.feedback-type-buy') }}
                              </label>
                              <label class="radio-inline">
                                <input type="radio" name="type" value="sell" id="feedback-type-sell"{{ Input::old('type') == "sell" ? ' checked="checked"' : '' }}>{{ Lang::get('user.feedback-type-sell') }}
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="#feedback-comments">{{ Lang::get('user.feedback-comments') }}:</label>
                            <textarea name="comments" class="form-control" id="feedback-comments" rows="8" placeholder="{{ Lang::get('user.feedback-comments') }}">{{ Input::old('feedback-comments') }}</textarea>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <div class="well pull-left">
                            <i class="fa fa-exclamation-triangle fa-fw"></i>
                            {{ Lang::get('user.feedback-warning') }}
                          </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 pull-right">
                          <div class="form-group">
                            <input class="hide" type="text" name="honeypot" value="">
                            <input type="submit" class="btn btn-danger btn-block" value="{{ Lang::get('forms.submit') }}">
                          </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 pull-right">
                          <button type="button" id="btn-feedback-cancel" class="btn btn-primary btn-block">{{ Lang::get('forms.cancel') }}</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  {{ Form::close() }}
                </div>
                <button class="btn btn-danger btn-block btn-lg btn-post-feedback visible-xs"><span class="glyphicon glyphicon-pencil"></span>{{ Lang::get('user.post-feedback') }}</button>
                @endif
                @if($info->avg_buy_rating || $info->avg_sell_rating)
                <div class="item-info info-line clearfix">
                  @if($info->avg_buy_rating)
                  <div class="pull-left">
                    <small>{{ Lang::get('user.buyer-rating') }}:</small> <div class="star-rating" data-score="{{ $info->avg_buy_rating }}"></div>
                  </div>
                  @endif
                  @if($info->avg_sell_rating)
                  <div class="pull-left">
                    <small>{{ Lang::get('user.merchant-rating') }}:</small> <div class="star-rating" data-score="{{ $info->avg_sell_rating }}"></div>
                  </div>
                  @endif
                </div>
                @endif
                <div id="feedback-list">
                @if($total_feedback > 0)
                  @foreach($feedback as $row)
                  @include('users.single-feedback', array('feedback' => $row))
                  @endforeach
                @endif
                </div>
                @if($total_feedback > 0 && $total_feedback > count($feedback))
                <div class="text-center">
                  <button type="button" id="btn-load-more" class="btn btn-danger">{{ Lang::get('pagination.load-more-feedback') }}</button>
                  {{ HTML::image('img/loading-med.gif', '', array('id' => 'feedback-loader', 'class' => 'hidden')) }}
                </div>
                @endif
              </div>
              <div class="tab-pane fade" id="sell-feedback">
                @if($total_sell_feedback == 0)
                <div class="alert {{ $total_sell_feedback == 0 ? 'alert-danger' : 'hidden' }}">
                  <strong>{{ Lang::get('user.no-sell-feedback') }}</strong>
                  @if(Auth::check() && $info->id != Auth::user()->id)
                  &nbsp;<a href="#" class="link-post-feedback">{{ Lang::get('user.add-yours') }}</a>
                  @endif
                </div>
                @endif
                @if(Auth::check() && $info->id != Auth::user()->id)
                <button class="btn btn-danger btn-block btn-lg btn-post-feedback visible-xs"><span class="glyphicon glyphicon-pencil"></span>{{ Lang::get('user.post-feedback') }}</button>
                @endif
                @if($info->avg_sell_rating)
                <div class="item-info info-line clearfix">
                  <div class="pull-left">
                    <small>{{ Lang::get('user.merchant-rating') }}:</small> <div class="star-rating" data-score="{{ $info->avg_sell_rating }}"></div>
                  </div>
                </div>
                @endif
                <div id="sell-feedback-list">
                @if($total_sell_feedback > 0)
                  @foreach($sell_feedback as $row)
                  @include('users.single-feedback', array('feedback' => $row))
                  @endforeach
                @endif
                </div>
                @if($total_sell_feedback > 0 && $total_sell_feedback > count($sell_feedback))
                <div class="text-center">
                  <button type="button" id="btn-load-more-sell" class="btn btn-danger">{{ Lang::get('pagination.load-more-feedback') }}</button>
                  {{ HTML::image('img/loading-med.gif', '', array('id' => 'sell-feedback-loader', 'class' => 'hidden')) }}
                </div>
                @endif
              </div>
              <div class="tab-pane fade" id="buy-feedback">
                @if($total_buy_feedback == 0)
                <div class="alert {{ $total_buy_feedback == 0 ? 'alert-danger' : 'hidden' }}">
                  <strong>{{ Lang::get('user.no-buy-feedback') }}</strong>
                  @if(Auth::check() && $info->id != Auth::user()->id)
                  &nbsp;<a href="#" class="link-post-feedback">{{ Lang::get('user.add-yours') }}</a>
                  @endif
                </div>
                @endif
                @if(Auth::check() && $info->id != Auth::user()->id)
                <button class="btn btn-danger btn-block btn-lg btn-post-feedback visible-xs"><span class="glyphicon glyphicon-pencil"></span>{{ Lang::get('user.post-feedback') }}</button>
                @endif
                @if($info->avg_buy_rating)
                <div class="item-info info-line clearfix">
                  <div class="pull-left">
                    <small>{{ Lang::get('user.buyer-rating') }}:</small> <div class="star-rating" data-score="{{ $info->avg_buy_rating }}"></div>
                  </div>
                </div>
                @endif
                <div id="buy-feedback-list">
                @if($total_buy_feedback > 0)
                  @foreach($buy_feedback as $row)
                  @include('users.single-feedback', array('feedback' => $row))
                  @endforeach
                @endif
                </div>
                @if($total_buy_feedback > 0 && $total_buy_feedback > count($buy_feedback))
                <div class="text-center">
                  <button type="button" id="btn-load-more-buy" class="btn btn-danger">{{ Lang::get('pagination.load-more-feedback') }}</button>
                  {{ HTML::image('img/loading-med.gif', '', array('id' => 'buy-feedback-loader', 'class' => 'hidden')) }}
                </div>
                @endif
              </div>
              <div class="tab-pane fade" id="given-feedback">
                @if($total_given_feedback == 0)
                <div class="alert {{ $total_given_feedback == 0 ? 'alert-danger' : 'hidden' }}">
                  <strong>{{ Lang::get('user.no-given-feedback') }}</strong>
                </div>
                @endif
                <div id="given-feedback-list">
                @if($total_given_feedback > 0)
                  @foreach($given_feedback as $row)
                  @include('users.single-feedback', array('feedback' => $row, 'given' => true))
                  @endforeach
                @endif
                </div>
                @if($total_given_feedback > 0 && $total_given_feedback > count($given_feedback))
                <div class="text-center">
                  <button type="button" id="btn-load-more-given" class="btn btn-danger">{{ Lang::get('pagination.load-more-feedback') }}</button>
                  {{ HTML::image('img/loading-med.gif', '', array('id' => 'given-feedback-loader', 'class' => 'hidden')) }}
                </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
@stop