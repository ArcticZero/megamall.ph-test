@extends('layouts.master')

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {
      msg_delete_confirm = "{{ Lang::get('user.delete-address-confirm') }}";
      select_ph = '{{ Lang::get('user.select-country') }}';
      no_zones = '{{ Lang::get('user.no-zones-country') }}';
      fn = '{{ Auth::user()->firstname }}';
      ln = '{{ Auth::user()->lastname }}';
      new_adds = {{ count(Input::old('new.firstname')) }};
    });
  </script>
  @include('includes.validator')
  {? $add_js[] = '/js/vendor/jquery.validate.min.js' ?}
  {? $add_js[] = '/js/vendor/jquery.easing.1.3.js' ?}
  {? $add_js[] = '/js/address-book.js' ?}
@stop

@section('breadcrumbs')
  @include('includes.breadcrumbs')
@stop

@section('content')
      <div class="container">
        {{ Form::open(array('id' => 'form-address-book')) }}
          <div class="row clearfix controls">
            <div class="col-md-12">
              <div class="option-left">
                <h3>{{ Lang::get('user.manage-addresses') }}</h3>
              </div>
              <div class="option-right text-right hidden-xs">
                <button class="btn btn-primary" id="btn-add-new" type="button"><i class="fa fa-fw fa-plus"></i>{{ Lang::get('user.add-new-address') }}</button>
                <input type="submit" class="btn btn-danger" value="{{ Lang::get('forms.save-changes') }}">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              @include('users.sidebar')
            </div>
            <div class="col-md-8">
              <div class="panel-group" id="address-list">
                <div id="address-notice" class="alert alert-danger{{ count($user->addresses) > 0 ? ' hidden' : '' }}"><strong>{{ Lang::get('user.no-addresses', array('add-one' => link_to('#', Lang::get('user.add-address'), array('class' => 'link-add-new')))) }}</strong></div>
                @if(count($user->addresses) > 0)
                {? $i = 0; ?}
                @foreach($user->addresses as $index => $address)
                {? $i++; ?}
                @include('users.single-address', array('row' => $address, 'rownum' => $i, 'open' => ($index === 0 ? true : false), 'hidden' => false, 'editable' => true))
                @endforeach
                @endif
                @if(count(Input::old('new.firstname')) > 0)
                @for($i = 0; $i < count(Input::old('new.firstname')); $i++)
                @include('users.single-address', array('x' => $i, 'open' => true, 'hidden' => false, 'editable' => true))
                @endfor
                @else
                @include('users.single-address', array('open' => (count($user->addresses) > 0 ? false : true), 'hidden' => true, 'editable' => false))
                @endif
              </div>
              <div class="visible-xs">
                <input type="submit" class="btn btn-danger btn-block" value="{{ Lang::get('forms.save-changes') }}">
                <input class="hide" type="text" name="honeypot" value="">
              </div>
            </div>
          </div>
        {{ Form::close() }}
      </div>
@stop