@extends('layouts.master')

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {
        pwlabels = ["{{ Lang::get('forms.pw-weak') }}", "{{ Lang::get('forms.pw-fair') }}", "{{ Lang::get('forms.pw-medium') }}", "{{ Lang::get('forms.pw-good') }}", "{{ Lang::get('forms.pw-strong') }}"];
        msgWeakPw = "{{ Lang::get('forms.error-pw-strength') }}";
    });
  </script>
  @include('includes.validator')
  {? $add_js[] = '/js/vendor/jquery.validate.min.js' ?}
  {? $add_js[] = '/js/vendor/pwstrength-bootstrap-1.1.5.js' ?}
  {? $add_js[] = '/js/signup.js' ?}
@stop

@section('breadcrumbs')
  @include('includes.breadcrumbs')
@stop

@section('content')
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="panel panel-primary">
              <div class="panel-heading">{{ Lang::get('user.header-social') }}</div>
              <div class="panel-body text-center">
                <div class="row">
                  <div class="social-btn-box col-md-6 col-md-offset-3 col-ms-6 col-ms-offset-0 col-sm-6 col-sm-offset-0">
                    <a href="{{ url('login/facebook') }}" class="btn btn-facebook btn-block"><i class="fa fa-facebook fa-fw"></i> | {{ Lang::get('forms.login-fb') }}</a>
                  </div>
                  <div class="social-btn-box col-md-6 col-md-offset-3 col-ms-6 col-ms-offset-0 col-sm-6 col-sm-offset-0">
                    <a href="{{ url('login/google') }}" class="btn btn-google-plus btn-block"><i class="fa fa-google-plus fa-fw"></i> | {{ Lang::get('forms.login-google') }}</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="panel panel-danger">
              <div class="panel-heading">{{ Lang::get('user.header-register') }}</div>
              <div class="panel-body">
                {{ Form::open(array('route' => 'signup', 'id' => 'form-signup')) }}
                  <div class="row">
                    <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                      <div class="form-group has-feedback{{ $errors->has('firstname') ? ' has-errors' : '' }}">
                        <label for="signup-firstname">{{ Lang::get('user.firstname') }}</label>
                        <input type="text" name="firstname" id="signup-firstname" class="form-control" value="{{ Input::old('firstname') }}" required>
                        @if($errors->has('firstname'))
                          <div for="signup-firstname" class="label label-danger">{{ $errors->first('firstname') }}</div>
                          <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                        @endif
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                      <div class="form-group has-feedback{{ $errors->has('lastname') ? ' has-errors' : '' }}">
                        <label for="signup-lastname">{{ Lang::get('user.lastname') }}</label>
                        <input type="text" name="lastname" id="signup-lastname" class="form-control" value="{{ Input::old('lastname') }}" required>
                        @if($errors->has('lastname'))
                          <div for="signup-lastname" class="label label-danger">{{ $errors->first('lastname') }}</div>
                          <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                      <div class="form-group has-feedback{{ $errors->has('email') ? ' has-errors' : '' }}">
                        <label for="signup-email">{{ Lang::get('user.email') }}</label>
                        <input type="email" name="email" id="signup-email" class="form-control" value="{{ Input::old('email') }}" required>
                        @if($errors->has('email'))
                          <div for="signup-email" class="label label-danger">{{ $errors->first('email') }}</div>
                          <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                        @endif
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                      <div class="form-group has-feedback{{ $errors->has('username') ? ' has-errors' : '' }}">
                        <label for="signup-username">{{ Lang::get('user.username') }} <span title="{{ Lang::get('user.info-username') }}" data-toggle="tooltip" data-placement="right" data-html="true" class="glyphicon glyphicon-info-sign"></span></label>
                        <input type="text" name="username" id="signup-username" minlength="4" class="form-control" placeholder="{{ Lang::get('forms.optional') }}" value="{{ Input::old('username') }}">
                        @if($errors->has('username'))
                          <div for="signup-username" class="label label-danger">{{ $errors->first('username') }}</div>
                          <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                      <div class="form-group has-feedback{{ $errors->has('password') ? ' has-errors' : '' }}">
                        <label for="signup-password">{{ Lang::get('user.password') }} <span title="{{ Lang::get('user.info-pw-length', array('num' => 8)) . '<br>' . Lang::get('user.info-pw-not-email') . '<br>' . Lang::get('user.info-pw-not-name') . '<br>' . Lang::get('user.info-pw-patterns') }}" data-toggle="tooltip" data-placement="right" data-html="true" class="glyphicon glyphicon-info-sign"></span></label>
                        <input type="password" minlength="8" name="password" id="signup-password" class="form-control" required>
                        @if($errors->has('password'))
                          <div for="signup-password" class="label label-danger">{{ $errors->first('password') }}</div>
                          <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                        @endif
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2" id="pw-feedback">
                      <small>{{ Lang::get('forms.pw-strength') }}</small>
                      <div class="pw-strength"></div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                      <div class="form-group has-feedback">
                        <label for="signup-confirm-password">{{ Lang::get('user.confirm-password') }}</label>
                        <input type="password" name="password_confirmation" id="signup-confirm-password" class="form-control" required>
                      </div>
                    </div>
                  </div>
                  <div class="row form-note">
                    <div class="col-md-12 col-sm-8 col-md-offset-0 col-sm-offset-2">
                      {{ Lang::get('user.signup-accept', array('terms' => link_to('terms', Lang::get('user.signup-terms'), array('target' => '_blank')), 'privacy' => link_to('privacy', Lang::get('user.signup-privacy'), array('target' => '_blank')))) }}
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-2 col-sm-8 col-md-offset-0 col-sm-offset-2">
                      <input class="hide" type="text" name="honeypot" value="">
                      <button type="submit" name="submit" class="btn btn-primary btn-block">{{ Lang::get('forms.submit') }}</button>
                    </div>
                  </div>
                {{ Form::close() }}
              </div>
            </div>
          </div>
        </div>
      </div>
@stop