                  <div class="panel panel-{{ Auth::check() && Auth::user()->id == $feedback->user_id && !isset($given) && !$feedback->read ? 'primary' : 'default' }} review-panel fb-{{ $feedback->id }}" data-fb-id="{{ $feedback->id }}">
                    <div class="panel-heading clearfix">
                      <div class="pull-left">
                        {{ isset($given) ? '<small>' . Lang::get('user.message-to') . ':</small> ' . link_to('profile/' . $feedback->user_id, $feedback->username ? $feedback->username : implode(' ', array($feedback->firstname, $feedback->lastname))) : link_to('profile/' . $feedback->posted_by, $feedback->username ? $feedback->username : implode(' ', array($feedback->firstname, $feedback->lastname))) }}
                        <span class="label label-{{ Auth::check() && Auth::user()->id == $feedback->user_id && !isset($given) && !$feedback->read ? 'danger' : 'default' }}"><span class="fa fa-clock-o fa-fw"></span>{{ date(Config::get('settings.date_format'), strtotime($feedback->posted_on)) }}</span>
                      </div>
                      <div class="star-rating" data-score="{{ $feedback->rating }}"></div>
                    </div>
                    <div class="panel-body">
                      {{ $feedback->comments }}
                      <div class="report">
                        <a class="link-report" data-type="feedback" data-item="{{ $feedback->id }}" href="{{ url('help/report/feedback/' . $feedback->id) }}"><i class="fa fa-exclamation-circle fa-fw red"></i></a><small><a class="link-report" data-type="feedback" data-item="{{ $feedback->id }}" href="{{ url('help/report/feedback/' . $feedback->id) }}">{{ Lang::get('user.report-feedback') }}</a></small>
                      </div>
                    </div>
                  </div>