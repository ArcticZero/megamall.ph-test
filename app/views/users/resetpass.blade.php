@extends('layouts.master')

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {
        pwlabels = ["{{ Lang::get('forms.pw-weak') }}", "{{ Lang::get('forms.pw-fair') }}", "{{ Lang::get('forms.pw-medium') }}", "{{ Lang::get('forms.pw-good') }}", "{{ Lang::get('forms.pw-strong') }}"];
        msgWeakPw = "{{ Lang::get('forms.error-pw-strength') }}";
    });
  </script>
  @include('includes.validator')
  {? $add_js[] = '/js/vendor/jquery.validate.min.js' ?}
  {? $add_js[] = '/js/vendor/pwstrength-bootstrap-1.1.5.js' ?}
  {? $add_js[] = '/js/resetpass.js' ?}
@stop

@section('breadcrumbs')
  @include('includes.breadcrumbs')
@stop

@section('content')
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-danger">
              <div class="panel-heading">{{ Lang::get('user.header-resetpass') }}</div>
              <div class="panel-body">
                {{ Form::open(array('url' => 'reset-password', 'id' => 'form-resetpass')) }}
                  <div class="row">
                    <div class="col-md-12 col-sm-8 col-md-offset-0 col-sm-offset-2">
                      <div class="form-group has-feedback{{ $errors->has('email') ? ' has-errors' : '' }}">
                        <label for="resetpass-email">{{ Lang::get('user.email') }}</label>
                        <input type="email" name="email" id="resetpass-email" class="form-control" value="{{ Input::old('email') }}" required>
                        @if($errors->has('email'))
                          <div for="resetpass-email" class="label label-danger">{{ $errors->first('email') }}</div>
                          <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                      <div class="form-group has-feedback{{ $errors->has('password') ? ' has-errors' : '' }}">
                        <label for="resetpass-password">{{ Lang::get('user.password') }} <span title="{{ Lang::get('user.info-pw-length') . '<br>' . Lang::get('user.info-pw-not-email') . '<br>' . Lang::get('user.info-pw-not-name') . '<br>' . Lang::get('user.info-pw-patterns') }}" data-toggle="tooltip" data-placement="right" data-html="true" class="glyphicon glyphicon-info-sign"></span></label>
                        <input type="password" minlength="8" name="password" id="resetpass-password" class="form-control" required>
                        @if($errors->has('password'))
                          <div for="resetpass-password" class="label label-danger">{{ $errors->first('password') }}</div>
                          <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                        @endif
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2" id="pw-feedback">
                      <small>{{ Lang::get('forms.pw-strength') }}</small>
                      <div class="pw-strength"></div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                      <div class="form-group has-feedback">
                        <label for="resetpass-confirm-password">{{ Lang::get('user.confirm-password') }}</label>
                        <input type="password" name="password_confirmation" id="resetpass-confirm-password" class="form-control" required>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-2 col-sm-8 col-md-offset-0 col-sm-offset-2">
                      <input class="hide" type="text" name="honeypot" value="">
                      <input type="hidden" name="token" value="{{ $token }}">
                      <button type="submit" name="submit" class="btn btn-danger btn-block">{{ Lang::get('forms.submit') }}</button>
                    </div>
                  </div>
                {{ Form::close() }}
              </div>
            </div>
          </div>
        </div>
      </div>
@stop