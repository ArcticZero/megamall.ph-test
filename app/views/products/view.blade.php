@extends('layouts.master')

@section('styles')
  {? $add_css[] = '/css/flexslider.css' ?}
  @if(Auth::check() && Auth::user()->id != $info->store->user_id)
  {? $add_css[] = '/css/summernote.css' ?}
  @endif
@stop

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {
        review_pages = {{ $review_pages }};
        product_slug = '{{ $info->permalink }}';
    });
  </script>
  {? $add_js[] = '/js/vendor/jquery.raty.js' ?}
  {? $add_js[] = '/js/vendor/jquery.elevateZoom-3.0.8.min.js' ?}
  {? $add_js[] = '/js/vendor/jquery.flexslider-min.js' ?}
  @if(Auth::check() && Auth::user()->id != $info->store->user_id)
  {? $add_js[] = '/js/vendor/summernote.min.js' ?}
  @endif
  {? $add_js[] = '/js/product-view.js' ?}
@stop

@section('breadcrumbs')
  @include('includes.breadcrumbs')
@stop

@section('content')
      <div class="container" itemscope itemtype="http://schema.org/Product">
        <div class="row">
          <div class="item-info col-md-6 col-md-push-6">
            <h3 itemprop="name">{{ $info->name }}</h3>
            @if($info->brand)
            <div class="info-line">{{ Lang::get('product.brand-by') . " " . link_to('search?s=' . htmlentities($info->brand, ENT_COMPAT), $info->brand) }}</div>
            @endif
            @if(!is_null($info->avg_rating))
            <div class="info-line">
              <div class="star-rating" data-score="{{ $info->avg_rating }}" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating"></div>
              <small><a href="#reviews" id="link-show-reviews">{{ Lang::get($total_reviews == 1 ? 'product.based-off-review' : 'product.based-off-reviews', array('num' => $total_reviews)) }}</a></small>
              <meta itemprop="worstRating" content="1">
              <meta itemprop="bestRating" content="5">
              <meta itemprop="ratingValue" content="{{ $info->avg_rating }}" />
              <meta itemprop="reviewCount" content="{{ $total_reviews }}" />
            </div>
            @endif
            <hr />
            <div class="info-line" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
              <div class="product-price">
                @if($info->special)
                <div class="old-price">{{ HTML::currency($info->price) }}</div>
                @endif
                <span itemprop="price">{{ HTML::currency($info->special ? $info->special : $info->price) }}</span>
              </div>
              <div class="product-notice">
                {{ Lang::get('product.price-ex-tax') . ' ' . HTML::currency($info->tax_free) }}
              </div>
              <div class="product-notice">
                {{ Lang::get('product.tax-rates-notice') }}
              </div>
            </div>
            <hr />
            <div class="info-line">
              <div class="share42init" data-title="{{ $info->name }}" data-image="{{ $info->store_url . $info->image_url }}" data-description="{{ htmlentities(strip_tags($info->description), ENT_COMPAT) }}"></div>
              {{ HTML::script('js/vendor/share42.js') }}
            </div>
            <div class="info-line clearfix">
              <div class="pull-left">
                <a class="link-report" data-type="product" data-item="{{ $info->permalink }}" href="{{ url('help/report/product/' . $info->permalink) }}"><i class="fa fa-exclamation-circle fa-fw"></i></a><small><a class="link-report" data-type="product" data-item="{{ $info->permalink }}" href="{{ url('help/report/product/' . $info->permalink) }}">{{ Lang::get('product.report-this-item') }}</a></small>
              </div>
              @if(Auth::check())
              <div class="pull-left">
                <span id="link-remove-bookmark"{{ !$bookmarked ? ' class="hidden"' : '' }}>
                  <a href="{{ url('bookmark/' . $info->permalink) }}" class="link-bookmark"><i class="fa fa-bookmark fa-fw"></i></a><small><a href="#" class="link-bookmark">{{ Lang::get('product.remove-bookmark') }}</a></small>
                </span>
                <span id="link-bookmark"{{ $bookmarked ? ' class="hidden"' : '' }}>
                  <a href="{{ url('bookmark/' . $info->permalink) }}" class="link-bookmark"><i class="fa fa-bookmark-o fa-fw"></i></a><small><a href="#" class="link-bookmark">{{ Lang::get('product.bookmark-this-item') }}</a></small>
                </span>
              </div>
              @endif
            </div>
            <div class="info-line clearfix">
              @if(Auth::guest() || Auth::check() && $info->store->user->id != Auth::user()->id)
              <div class="col-xs-12 col-ms-6 col-sm-6 col-md-6 col-lg-6">
                <a class="btn btn-lg btn-danger btn-block" href="{{ $info->store_url . $info->buy_url }}"><i class="fa fa-shopping-cart fa-fw"></i>{{ Lang::get('product.buy-from-store') }}</a>
              </div>
              <div class="col-xs-12 col-ms-6 col-sm-6 col-md-6 col-lg-6">
                <a class="btn btn-lg btn-primary btn-block" href="{{ url('products/inquire/' . $info->permalink) }}"><i class="fa fa-mail-forward fa-fw"></i>{{ Lang::get('product.inquire') }}</a>
              </div>
              @else
              <div class="col-md-12">
                <div class="alert alert-success"><strong>{{ Lang::get('product.is-your-product') }}</strong></div>
              </div>
              @endif
            </div>
            <div class="info-line product-notice">
              {{ Lang::get('product.more-from-store', array('store' => link_to('stores/' . $info->store_permalink, $info->store_name))) }}
            </div>
            <div class="ga">
              <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
              <!-- megamall-responsive -->
              <ins class="adsbygoogle"
                   style="display:block"
                   data-ad-client="ca-pub-0317550451740718"
                   data-ad-slot="9088660230"
                   data-ad-format="auto"></ins>
              <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
              </script>
            </div>
          </div>
          <div class="product-gallery col-md-6 col-md-pull-6">
            <div class="product-showcase" style="width: 100%; height: {{ $img_height . 'px' }};">
              <div class="loader"><span class="helper"></span>{{ HTML::image(url('img/loading-large.gif')) }}</div>
              {{ HTML::image($info->image_url ? 'img/products/' . $info->permalink . '/' . pathinfo($info->image_url)['filename'] . '-' . $img_width . 'x' . $img_height . '.jpg' : 'img/no-image-medium.png' , false, array('id' => 'img-preview', 'class' => 'img-responsive', 'data-zoom-image' => $info->store_url . $info->image_url)) }}
            </div>
            @if($images)
            <div class="flexslider">
              <ul class="slides">
                @foreach($images as $img)
                <li>{{ HTML::image('img/products/' . $info->permalink . '/' . pathinfo($img)['filename'] . '-' . $thumb_width . 'x' . $thumb_height . '.jpg' , false, array('class' => 'product-thumb img-responsive', 'data-big-image' => url('img/products/' . $info->permalink . '/' . pathinfo($img)['filename'] . '-' . $img_width . 'x' . $img_height . '.jpg'), 'data-zoom-image' => $img)) }}</li>
                @endforeach
              </ul>
            </div>
            @endif
          </div>
          <div id="product-details" class="col-md-12">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#description" data-toggle="tab">{{ Lang::get('product.product-info') }}</a></li>
              <li><a href="#reviews" data-toggle="tab">{{ Lang::get('product.user-reviews') }} <span class="badge">{{ $total_reviews }}</span></a></li>
              @if(Auth::check() && Auth::user()->id != $info->store->user_id)
              <li class="pull-right hidden-xs"><button class="btn btn-primary btn-write-review"><span class="glyphicon glyphicon-pencil"></span>{{ Lang::get('product.write-review') }}</button></li>
              @endif
            </ul>
            <div class="tab-content">
              <div class="tab-pane fade in active" id="description" itemprop="description">
                {{ html_entity_decode($info->description, ENT_QUOTES, 'UTF-8'); }}
              </div>
              <div class="tab-pane fade" id="reviews">
                @if($total_reviews == 0 || ($total_reviews > 0 && Auth::check()))
                <div id="review-notice" class="alert {{ $total_reviews == 0 ? 'alert-danger' : 'hidden' }}">
                  @if($total_reviews == 0)
                    <strong>{{ Lang::get('product.no-reviews') }}</strong>
                    @if(Auth::check() && Auth::user()->id != $info->store->user_id)
                    &nbsp;<a href="#" id="link-write-review">{{ Lang::get('product.write-one') }}</a>
                    @endif
                  @endif
                </div>
                @endif
                @if(Auth::check() && Auth::user()->id != $info->store->user_id)
                <div id="form-write-review" class="hidden">
                  {{ Form::open(array('url' => 'review/product/save')) }}
                  <div class="panel panel-danger review-panel">
                    <div class="panel-heading">
                      {{ Lang::get('product.write-review') }}
                    </div>
                    <div class="panel-body">
                      <div id="error-box" class="alert alert-danger hidden">
                        <strong>{{ Lang::get('forms.error-header') }}</strong>
                        <ul></ul>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="#review-rating">{{ Lang::get('product.review-rating') }}:</label>
                            <div id="review-rating" class="rating"></div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="#review-title">{{ Lang::get('product.review-title') }}:</label>
                            <input type="text" class="form-control" name="title" id="review-title" value="{{ Input::old('title') }}" placeholder="{{ Lang::get('product.review-title') }}" required>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="#review-comments">{{ Lang::get('product.review-comments') }}:</label>
                            <textarea name="comments" class="form-control" id="review-comments" rows="8" placeholder="{{ Lang::get('product.review-comments') }}">{{ Input::old('comments') }}</textarea>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-1 col-md-2 col-sm-2 col-xs-12 pull-right">
                          <div class="form-group">
                            <input class="hide" type="text" name="honeypot" value="">
                            <input type="submit" class="btn btn-danger btn-block" value="{{ Lang::get('forms.submit') }}">
                          </div>
                        </div>
                        <div class="col-lg-1 col-md-2 col-sm-2 col-xs-12 pull-right">
                          <button type="button" id="btn-review-cancel" class="btn btn-primary btn-block">{{ Lang::get('forms.cancel') }}</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  {{ Form::close() }}
                </div>
                <button class="btn btn-danger btn-block btn-lg btn-write-review visible-xs"><span class="glyphicon glyphicon-pencil"></span>{{ Lang::get('product.write-review') }}</button>
                @endif
                <div id="review-list">
                @if($total_reviews > 0)
                  @foreach($reviews as $review)
                  @include('products.single-review', array('review' => $review, 'username' => $review->username, 'firstname' => $review->firstname, 'lastname' => $review->lastname))
                  @endforeach
                @endif
                </div>
                @if($total_reviews > 0 && $total_reviews > count($reviews))
                <div class="text-center">
                  <button type="button" id="btn-load-more" class="btn btn-danger">{{ Lang::get('pagination.load-more-reviews') }}</button>
                  {{ HTML::image('img/loading-med.gif', '', array('id' => 'review-loader', 'class' => 'hidden')) }}
                </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
@stop