                  <div class="panel panel-default review-panel">
                    <div class="panel-heading clearfix">
                      <div class="pull-left">
                        {{ link_to('profile/' . $review->posted_by, $username ? $username : implode(' ', array($firstname, $lastname))) }}
                        <span class="label label-default"><span class="fa fa-clock-o fa-fw"></span>{{ date(Config::get('settings.date_format'), strtotime($review->posted_on)) }}</span>
                      </div>
                      <div class="star-rating" data-score="{{ $review->rating }}"></div>
                    </div>
                    <div class="panel-body">
                      <strong>{{ $review->title }}</strong>{{ $review->comments }}
                      <div class="report">
                        <a class="link-report" data-type="review" data-item="{{ $review->id }}" href="{{ url('help/report/review/' . $review->id) }}"><i class="fa fa-exclamation-circle fa-fw"></i></a><small><a class="link-report" data-type="review" data-item="{{ $review->id }}" href="{{ url('help/report/review/' . $review->id) }}">{{ Lang::get('product.report-review') }}</a></small>
                      </div>
                    </div>
                  </div>