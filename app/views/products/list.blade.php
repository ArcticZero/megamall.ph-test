@extends('layouts.master')

@section('scripts')
  {? $add_js[] = '/js/vendor/jquery.raty.js' ?}
  {? $add_js[] = '/js/product-list.js' ?}
  @if(Route::currentRouteAction() == 'StoreController@view')
  {? $add_js[] = '/js/vendor/jquery.raty.js' ?}
  {? $add_js[] = '/js/store-view.js' ?}
  @endif
@stop

@section('breadcrumbs')
  @include('includes.breadcrumbs')
@stop

@section('content')
      <div class="container">
        <div class="row sub-title">
          <h3 class="col-md-10 col-sm-7 col-xs-12">{{ $name }}</h3>
          @if($total_products > 0)
          <div class="col-md-2 col-sm-5 pull-right hidden-xs">
            <button class="btn btn-danger btn-hidden btn-block" type="button" data-toggle="collapse" data-target="#display-options">{{ Lang::get('product.display-options') }} <span class="caret"></span></button>
          </div>
          @endif
        </div>
        <div class="row">
          @if(isset($sidebar))
          <div class="col-md-4 hidden-sm">
            @include($sidebar, $sidebar_data)
          </div>
          <div class="col-md-8">
          @else
          <div class="col-md-12">
          @endif
            @if($total_products > 0)
            <button class="btn btn-danger btn-hidden btn-block visible-xs" type="button" data-toggle="collapse" data-target="#display-options">{{ Lang::get('product.display-options') }} <span class="caret"></span></button>
            <div class="row collapse" id="display-options">
              <div class="col-md-12">
                <div class="well clearfix">
                  {{ Form::open(array('url' => Request::path(), 'method' => 'get', 'id' => 'form-display-options')) }}
                    <div class="option-left">
                      {{ Lang::get('product.sort-by') }}
                      <select name="sort">
                        @foreach($sort_by as $sort)
                        <option value="{{ $sort }}"{{ $sort == Input::get('sort') ? ' selected="selected"' : '' }}>{{ Lang::get('product.sort-' . $sort) }}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="option-right">
                      {{ Lang::get('product.show') }}
                      <select name="show">
                        @foreach(explode(',', Config::get('settings.products_per_page_options')) as $num)
                        <option value="{{ $num }}"{{ $num == $products_per_page ? ' selected="selected"' : '' }}>{{ $num }}</option>
                        @endforeach
                      </select>
                      {{ Lang::get('product.items-per-page') }}
                      <input type="hidden" name="s" value="{{ Input::get('s') }}">
                      <input type="hidden" name="c" value="{{ Input::get('c') }}">
                    </div>
                    <!--
                    <div class="col-md-4 col-sm-4 col-xs-4 text-right">
                      <span id="show-list" class="btn btn-sm fa fa-list"><span>{{ Lang::get('product.display-list') }}</span></span>
                      <span id="show-grid" class="btn btn-sm fa fa-th"><span>{{ Lang::get('product.display-grid') }}</span></span>
                    </div>
                    -->
                  {{ Form::close() }}
                </div>
              </div>
            </div>
            @endif
            @if(isset($children) && $total_children > 0)
            <div class="row">
              <div class="col-md-12">
                <h5>Subcategories: {{ implode(", ", $children) }}</h5>
              </div>
            </div>
            @endif
            @if($total_products > 0)
            <div class="row">
              {{ isset($sidebar) ? HTML::productGrid($products, true) : HTML::productGrid($products) }}
            </div>
            <div class="row">
              <div class="col-md-12">
                {{ $products->links() }}
              </div>
            </div>
            @else
            <div class="row">
              <div class="col-md-12">
                <div class="alert alert-danger"><strong>{{ Lang::get('product.no-results') }}</strong></div>
              </div>
            </div>
            @endif
          </div>
        </div>
      </div>
@stop