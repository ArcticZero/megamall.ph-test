@extends('layouts.master')

@section('styles')
  {? $add_css[] = '/css/nivo-slider.css' ?}
  {? $add_css[] = '/css/slider/default/default.css' ?}
@stop

@section('scripts')
  {? $add_js[] = '/js/vendor/jquery.nivo.slider.pack.js' ?}
  {? $add_js[] = '/js/vendor/jquery.raty.js' ?}
  {? $add_js[] = '/js/home.js' ?}
@stop

@section('content')
      <div class="container">
        <div class="row">
          <div class="col-md-9">
            {{ HTML::banner($home_banner) }}
          </div>
          <div class="col-md-3 hidden-sm hidden-xs">
            {{ HTML::banner($home_box_1) }}
            {{ HTML::banner($home_box_2) }}
          </div>
        </div>
        @if(count($featured) > 0)
        <div class="row sub-title">
          <h3 class="col-md-12">{{ Lang::get('product.featured') }}</h3>
        </div>
        <div class="row">
          {{ HTML::productGrid($featured) }}
        </div>
        @endif
        @if(count($recent) > 0)
        <div class="row sub-title">
          <h3 class="col-md-12">{{ Lang::get('product.new-products') }}</h3>
        </div>
        <div class="row">
          {{ HTML::productGrid($recent) }}
        </div>
        @endif
        @if(count($viewed) > 0)
        <div class="row sub-title">
          <h3 class="col-md-12">{{ Lang::get('product.recently-viewed') }}</h3>
        </div>
        <div class="row">
          {{ HTML::productGrid($viewed) }}
        </div>
        @endif
      </div>
@stop