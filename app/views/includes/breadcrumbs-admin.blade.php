      @if($breadcrumbs)
      <div class="container" id="crumbs">
        <div class="row">
          <div class="col-md-12">
            <ol class="breadcrumb">
            <?php $crumbs = count($breadcrumbs); ?>
            @for($i = 0; $i < $crumbs; $i++)
              @if($i == $crumbs - 1)
              <li class="active"><a class="label label-primary" href="{{ Request::url() }}">{{ $breadcrumbs[$i][0] }}</a></li>
              @else
              <li><a class="label label-default" href="{{ url($breadcrumbs[$i][1]) }}">{{ $breadcrumbs[$i][0] }}</a></li>
              @endif
            @endfor
            </ol>
          </div>
        </div>
      </div>
      @endif