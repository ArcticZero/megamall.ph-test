@extends('layouts.master')

@section('styles')
  {? $add_css[] = '/css/bootstrap-tree.css' ?}
@stop

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {
        $('.tree li:has(ul)').addClass('parent_li').find(' > span');
        $('.tree li.parent_li > span').on('click', function (e) {
            var children = $(this).parent('li.parent_li').find(' > ul > li');

            if (children.is(":visible")) {
                children.hide('fast');
                $(this).addClass('fa-plus-square').removeClass('fa-minus-square');
            } else {
                children.show('fast');
                $(this).addClass('fa-minus-square').removeClass('fa-plus-square');
            }
            e.stopPropagation();
        });
    });
  </script>
@stop

@section('breadcrumbs')
  @include('includes.breadcrumbs')
@stop

@section('content')
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
              <div class="panel-heading">{{ Lang::get('product.all-categories') }}</div>
              <div class="panel-body">
                <div class="tree">
                  <ul>
                    @foreach($search_cats as $cat)
                    <li>
                      <span class="fa fa-minus-square fa-fw"></span>{{ link_to($cat['permalink'], $cat['name']) }}
                      @if($cat->children->count() > 0)
                      <ul>
                        @foreach($cat->children as $subcat)
                        <li>
                          <span class="fa fa-tag fa-fw red"></span>{{ link_to($subcat['permalink'], $subcat['name']) }}
                        </li>
                        @endforeach
                      </ul>
                      @endif
                    @endforeach
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@stop