@extends('layouts.admin')

@section('scripts')
<script type="text/javascript">
	// refresh the list
	function refresh() {
			var sort = urlData('s') || '';
			var order = urlData('o') || '';
			var page = urlData('page') || '';
			var loading = $("#load-list");
			var table = $("#rows");

			loading.removeClass('hide');

			$.post("{{ url('admin/users/refresh') }}", { s: sort, o: order, page: page }, function(response) {
					// clear table
					table.html("");

					// populate table
					$.each(response.rows.data, function(index, row) {
							table.append(
									'<tr data-id="' + row.id + '"' + (row.admin == 1 ? ' class="info"' : (row.status != 1 ? ' class="danger' + (row.status == 2 ? ' banned' : '') + '"' : '')) + '>' +
									'<td class="text-center">' + (row.admin == 1 ? '<i class="fa fa-legal text-danger" title="{{ Lang::get('user.level-admin') }}"></i>' : (row.status == 1 ? '<button class="btn btn-link btn-xs btn-icon btn-deactivate" title="{{ Lang::get('admin.active') }}"><i class="fa fa-check-square-o fa-fw text-success"></i></button>' : '<button class="btn btn-link btn-xs btn-icon btn-activate" title="{{ Lang::get('admin.inactive') }}"><i class="fa fa-square-o fa-fw text-danger"></i></button>')) + '</td>' +
									'<td>' + row.lastname + '</td>' +
									'<td>' + row.firstname + '</td>' +
									'<td>' + row.username + '</td>' +
									'<td>' + row.email + '</td>' +
									'<td class="no-wrap">' +
									'<button class="btn btn-primary btn-xs btn-icon btn-edit" title="{{ Lang::get('admin.edit') }}"><i class="fa fa-pencil fa-fw"></i></button>' +
									(row.id != {{ Auth::user()->id }} && row.id != 1 && row.status != 2 ? '&nbsp;<button class="btn btn-danger btn-xs btn-icon btn-ban" title="{{ Lang::get('admin.ban') }}"><i class="fa fa-ban fa-fw"></i></button>' : '') +
									'</tr>'
							);
					});

					// update links
					$("#row-pages").html(response.pages);

					loading.addClass('hide');
			}, 'json');
	}

	$(document).ready(function() {
			// edit button
			$(document).on('click', '.btn-edit', function() {
      		var id = $(this).parent().parent().data('id');
      		var btn = $(this);

      		btn.find('i').removeClass('fa-pencil').addClass('fa-spinner fa-spin');

      		// reset all form fields
					$("#modal-form").find('form').trigger("reset");
					$("#row-id").val("");

        	$.post("{{ url('admin/users/info') }}", { id: id }, function(response) {
        			if(!response.error) {
									// set form title
        					$("#form-title").html("{{ Lang::get('admin.form-view-title') }} <strong>" + response.lastname + ', ' + response.firstname + '</strong>');

        					// output form data
        					$.each(response, function(index, value) {
        							var field = $("#row-" + index);

        							// field exists, therefore populate
        							if(field.length > 0) {
        									field.val(value);
        							}
        					});

        					// display addresses
    							if(response.addresses.length > 0) {
    									$("#address-ph").hide();

    									var tab = $("#address-new");
    									var tabbox = $(".tab-content");
    									var tablist = $("#address-list");

    									$.each(response.addresses, function(index, row) {
    											var ctr = index + 1;
    											var newtab = tab.clone();

    											newtab.attr('id', 'address-' + ctr).data('id', row.id).removeClass('hide');
    											newtab.find(".address-id").val(row.id);
    											newtab.find(".address-firstname").val(row.firstname);
    											newtab.find(".address-lastname").val(row.lastname);
    											newtab.find(".address-company").val(row.company);
    											newtab.find(".address-address-1").val(row.address_1);
    											newtab.find(".address-address-2").val(row.address_2);
    											newtab.find(".address-city").val(row.city);
    											newtab.find(".address-postcode").val(row.postcode);
    											newtab.find(".address-country-id").val(row.country_id);

    											newtab.find("input, select").each(function() {
    													var newname = $(this).attr('name').replace(/\[.*?\]/g, '[' + row.id + ']');
            									$(this).attr('name', newname);
    											});

    											tablist.append('<li data-id="' + row.id + '"><a href="#address-' + ctr + '" class="address-tab" role="tab">{{ Lang::get('user.address') }} ' + ctr + (row.id == response.address_id ? ' <i class="fa fa-star text-danger" title="{{ Lang::get('admin.default-address') }}"></i>' : '') + '</a></li>');
    											tabbox.append(newtab);

    											newtab.find(".address-country-id").trigger('change', row.zone_id);
    									});
    							} else {
    									$("#address-ph").show();
    							}

        					// show form
        					$("#modal-form").modal('show');
        			} else {
        					status(false, response.error, 'alert-danger');
        			}

        			btn.find('i').removeClass('fa-spinner fa-spin').addClass('fa-pencil');
        	}, 'json');
      });

			// activate button
      $(document).on('click', '.btn-activate', function() {
      		var id = $(this).parent().parent().data('id');
      		var name = $(this).parent().parent().find('td:nth-child(5)').html();

      		dialog("{{ Lang::get('admin.user-activation') }}", "{{ Lang::get('admin.user-activate-q') }} <strong>" + name + '</strong>?', "{{ url('admin/users/activate') }}", id);
      });

			// deactivate button
      $(document).on('click', '.btn-deactivate', function() {
      		var id = $(this).parent().parent().data('id');
      		var name = $(this).parent().parent().find('td:nth-child(5)').html();

      		dialog("{{ Lang::get('admin.user-deactivation') }}", "{{ Lang::get('admin.user-deactivate-q') }} <strong>" + name + '</strong>?', "{{ url('admin/users/deactivate') }}", id);
      });

      // ban button
      $(document).on('click', '.btn-ban', function() {
      		var id = $(this).parent().parent().data('id');
      		var name = $(this).parent().parent().find('td:nth-child(5)').html();

      		dialog("{{ Lang::get('admin.user-ban') }}", "{{ Lang::get('admin.user-ban-q') }} <strong>" + name + '</strong>?', "{{ url('admin/users/ban') }}", id);
      });

      // enable dropdown tabs
      $(document).on('click', '#form-tabs a', function(e) {
				  e.preventDefault();
				  $(this).tab('show');
			});

			// select default tab on modal open
			$("#modal-form").on('show.bs.modal', function(e) {
					$(this).find('.nav-tabs a[href="#user-profile"]').tab('show');
					$("#form-notice").html("");
			});

			// reset modal on close
			$("#modal-form").on('hidden.bs.modal', function(e) {
					$(this).find(".address-tab-content").not("#address-new").remove();
					$(this).find(".address-tab").remove();
			});

			// change country
			$(document).on('change', '.address-country-id', function(e, initial) {
					var country = $(this).val();
					var zones = $(this).closest('.row').find('.address-zone-id');
					var blank = '<option value="">{{ Lang::get("user.no-zones-country") }}</option>';
					var invalid = '<option value="">{{ Lang::get("user.select-country") }}</option>'

					if(country) {
							$.post("{{ url('admin/get-zones') }}", { country: country }, function(response) {
									if(response.zones && response.zones.length > 0) {
											var html = '';

											$.each(response.zones, function(index, row) {
													html += '<option value="' + row.id + '"' + (row.id == initial ? ' selected="selected"' : '') + '>' + row.name + '</option>';
											});

											zones.html(html).attr('disabled', false);
									} else {
											zones.html(blank).attr('disabled', true);
									}
							});
					} else {
							$(zones).html(invalid).attr('disabled', true);
					}
			});

			// delete child
			$(document).on('click', '.btn-del-child', function(e) {
					var id = $(this).next('.address-id').val();
					var tabbox = $(".tab-content");
					var tablist = $("#address-list");
					var count = tablist.find(".address-tab").length;

					tablist.find("li[data-id='" + id + "']").remove();
					tabbox.find(".address-tab-content[data-id='" + id + "']").remove();

					if(count - 1 > 0) {
							tablist.find(".address-tab:first").tab('show');
					} else {
							tablist.html('<li id="address-ph" class="disabled"><a><i>{{ Lang::get('admin.user-no-addresses') }}</i></a></li>');
							$("#form-tabs").find("a[href='#user-profile']").tab('show');
					}
			});
	});

</script>
@stop

@section('breadcrumbs')
  @include('includes.breadcrumbs-admin')
@stop

@section('content')
		<div class="container admin-content">
			<h2>{{ Lang::get('admin.user-management') }}</h2>
			<div class="row">
				<div class="col-lg-12">
					<div class="table-responsive">
						<div id="load-list" class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-5x centered"></i></div>
            </div>
						<table class="table table-condensed table-bordered">
							<thead>
								<tr>
									<th></th>
									<th class="col-md-3"><a href="{{ url('admin/users') . '?s=lastname&amp;o=' . ($sort == 'lastname' && $order == 'asc' ? 'desc' : 'asc') . '&amp;page=' . $rows->getCurrentPage() }}">{{ Lang::get('user.lastname')}}{{ ($sort == 'lastname' ? ' <i class="fa fa-sort-' . $order . '"></i>' : '') }}</a></th>
									<th class="col-md-3"><a href="{{ url('admin/users') . '?s=firstname&amp;o=' . ($sort == 'firstname' && $order == 'asc' ? 'desc' : 'asc') . '&amp;page=' . $rows->getCurrentPage() }}">{{ Lang::get('user.firstname')}}{{ ($sort == 'firstname' ? ' <i class="fa fa-sort-' . $order . '"></i>' : '') }}</a></th>
									<th class="col-md-3"><a href="{{ url('admin/users') . '?s=username&amp;o=' . ($sort == 'username' && $order == 'asc' ? 'desc' : 'asc') . '&amp;page=' . $rows->getCurrentPage() }}">{{ Lang::get('user.username')}}{{ ($sort == 'username' ? ' <i class="fa fa-sort-' . $order . '"></i>' : '') }}</a></th>
									<th class="col-md-3"><a href="{{ url('admin/users') . '?s=email&amp;o=' . ($sort == 'email' && $order == 'asc' ? 'desc' : 'asc') . '&amp;page=' . $rows->getCurrentPage() }}">{{ Lang::get('user.email') }}{{ ($sort == 'email' ? ' <i class="fa fa-sort-' . $order . '"></i>' : '') }}</a></th>
									<th></th>
								</tr>
							</thead>
							<tbody id="rows">
								@foreach($rows as $row)
								<tr data-id="{{ $row->id }}"{{ $row->admin == 1 ? ' class="info"' : ($row->status != 1 ? ' class="danger' . ($row->status == 2 ? ' banned' : '') . '"' : '') }}>
									<td class="text-center">
										@if($row->admin == 1)
											<i class="fa fa-legal text-danger" title="{{ Lang::get('user.level-admin') }}"></i>
										@else
											@if($row->status == 1)
												<button class="btn btn-link btn-xs btn-icon btn-deactivate" title="{{ Lang::get('admin.active') }}"><i class="fa fa-check-square-o fa-fw text-success"></i></button>
											@else
												<button class="btn btn-link btn-xs btn-icon btn-activate" title="{{ Lang::get('admin.inactive') }}"><i class="fa fa-square-o fa-fw text-danger"></i></button>
											@endif
										@endif
									</td>
									<td>{{ $row->lastname }}</td>
									<td>{{ $row->firstname }}</td>
									<td>{{ $row->username }}</td>
									<td>{{ $row->email }}</td>
									<td class="no-wrap">
										<button class="btn btn-primary btn-xs btn-icon btn-edit" title="{{ Lang::get('admin.edit') }}"><i class="fa fa-pencil fa-fw"></i></button>
										@if($row->id != Auth::user()->id && $row->id != 1 && $row->status != 2)
											<button class="btn btn-danger btn-xs btn-icon btn-ban" title="{{ Lang::get('admin.ban') }}"><i class="fa fa-ban fa-fw"></i></button>
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<div id="row-pages" class="text-center">{{ $pages; }}</div>
			</div>
		</div>

		<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div id="load-form" class="loading-pane hide">
            <div><i class="fa fa-inverse fa-spinner fa-spin fa-5x centered"></i></div>
          </div>
					{{ Form::open(array('url' => 'admin/users/save', 'role' => 'form')) }}
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{{ Lang::get('admin.modal-close') }}</span></button>
	        		<h4 id="form-title" class="modal-title"></h4>
						</div>
						<div class="modal-body">
							<div id="form-notice"></div>
							<div role="tabpanel">
								<ul class="nav nav-tabs" id="form-tabs" role="tablist">
									<li role="presentation" class="active"><a href="#user-profile" aria-controls="user-profile" role="tab" data-toggle="tab">{{ Lang::get('user.user-profile') }}</a></li>
									<li role="presentation" class="dropdown">
										<a href="#" id="address-list-label" class="dropdown-toggle" data-toggle="dropdown" aria-controls="address-list" aria-expanded="false">{{ Lang::get('user.manage-addresses') }} <span class="caret"></span></a>
										<ul class="dropdown-menu" role="menu" aria-labelledby="address-list-label" id="address-list">
											<li id="address-ph" class="disabled"><a><i>{{ Lang::get('admin.user-no-addresses') }}</i></a></li>
										</ul>
									</li>
								</ul>
								<div class="tab-content">
									<div role="tabpanel" class="tab-pane fade in active" id="user-profile">
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<label for="row-firstname">{{ Lang::get('user.firstname') }}</label>
											    <input type="text" name="firstname" class="form-control" id="row-firstname" maxlength="32" placeholder="{{ Lang::get('admin.chars-max', array('num' => 32)) }}">
												</div>
												<div class="form-group">
													<label for="row-lastname">{{ Lang::get('user.lastname') }}</label>
											    <input type="text" name="lastname" class="form-control" id="row-lastname" maxlength="32" placeholder="{{ Lang::get('admin.chars-max', array('num' => 32)) }}">
												</div>
												<div class="form-group">
													<label for="row-username">{{ Lang::get('user.username') }}</label>
											    <input type="text" name="username" class="form-control" id="row-username" maxlength="32" placeholder="{{ Lang::get('admin.optional') . ', ' . Lang::get('admin.char-range', array('min' => 4, 'max' => 32)) }}">
												</div>
												<div class="form-group">
													<label for="row-password">{{ Lang::get('user.new-password') }}</label>
											    <input type="password" name="password" class="form-control" id="row-password" placeholder="{{ Lang::get('admin.pw-no-change') . ', ' . Lang::get('admin.chars-min', array('num' => 8)) }}">
												</div>
												<div class="form-group">
													<label for="row-confirm-password">{{ Lang::get('user.confirm-password') }}</label>
											    <input type="password" name="password_confirmation" class="form-control" id="row-confirm-password" placeholder="{{ Lang::get('user.repeat-password') }}">
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<label for="row-email">{{ Lang::get('user.email') }}</label>
											    <input type="email" name="email" class="form-control" id="row-email" placeholder="{{ Lang::get('user.email') }}">
												</div>
												<div class="form-group">
													<label for="row-telephone">{{ Lang::get('user.telephone') }}</label>
											    <input type="text" name="telephone" class="form-control" id="row-telephone">
												</div>
												<div class="form-group">
													<label for="row-fax">{{ Lang::get('user.fax') }}</label>
											    <input type="text" name="fax" class="form-control" id="row-fax">
												</div>
												<div class="form-group">
													<label for="row-ym-id">{{ Lang::get('user.yahoo-id') }}</label>
											    <input type="text" name="ym_id" class="form-control" id="row-ym_id">
												</div>
												<div class="form-group">
													<label for="row-skype-id">{{ Lang::get('user.skype-id') }}</label>
											    <input type="text" name="skype_id" class="form-control" id="row-skype_id">
												</div>
											</div>
										</div>
										<div class="row" id="set-admin">
											<div class="col-lg-6">
												<div class="form-group">
													<label for="row-admin">{{ Lang::get('user.user-level') }}</label>
											    <select name="admin" id="row-admin" class="form-control">
											    	<option value="0">{{ Lang::get('user.level-user') }}</option>
											    	<option value="1">{{ Lang::get('user.level-admin') }}</option>
											    </select>
												</div>
											</div>
										</div>
									</div>
									<div role="tabpanel" class="tab-pane fade address-tab-content hide" id="address-new" data-id="">
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<label>{{ Lang::get('user.firstname') }}</label>
								    			<input type="text" name="address_firstname[]" class="form-control address-firstname" maxlength="32" placeholder="{{ Lang::get('admin.chars-max', array('num' => 32)) }}">
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<label>{{ Lang::get('user.lastname') }}</label>
								    			<input type="text" name="address_lastname[]" class="form-control address-lastname" maxlength="32" placeholder="{{ Lang::get('admin.chars-max', array('num' => 32)) }}">
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<label>{{ Lang::get('user.company') }}</label>
								    			<input type="text" name="address_company[]" class="form-control address-company" maxlength="32" placeholder="{{ Lang::get('admin.chars-max', array('num' => 32)) }}">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<label>{{ Lang::get('user.address-1') }}</label>
								    			<input type="text" name="address_address_1[]" class="form-control address-address-1" maxlength="128" placeholder="{{ Lang::get('admin.chars-max', array('num' => 128)) }}">
												</div>
												<div class="form-group">
													<label>{{ Lang::get('user.city') }}</label>
								    			<input type="text" name="address_city[]" class="form-control address-city" maxlength="128" placeholder="{{ Lang::get('admin.chars-max', array('num' => 128)) }}">
												</div>
												<div class="form-group">
													<label>{{ Lang::get('user.country') }}</label>
								    			<select name="address_country_id[]" class="form-control address-country-id">
								    				<option value="">{{ Lang::get('user.select-country') }}</option>
								    				@foreach($countries as $country)
								    				<option value="{{ $country->country_id }}">{{ $country->name }}</option>
								    				@endforeach
								    			</select>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<label>{{ Lang::get('user.address-2') }}</label>
								    			<input type="text" name="address_address_2[]" class="form-control address-address-2" maxlength="128" placeholder="{{ Lang::get('admin.chars-max', array('num' => 128)) }}">
												</div>
												<div class="form-group">
													<label>{{ Lang::get('user.postcode') }}</label>
								    			<input type="text" name="address_postcode[]" class="form-control address-postcode" maxlength="10" placeholder="{{ Lang::get('admin.chars-max', array('num' => 10)) }}">
												</div>
												<div class="form-group">
													<label>{{ Lang::get('user.zone') }}</label>
								    			<select name="address_zone_id[]" class="form-control address-zone-id" disabled>
								    				<option value="">{{ Lang::get('user.select-country') }}</option>
								    			</select>
												</div>
												<div class="form-group text-right">
													<button type="button" class="btn btn-danger btn-del-child btn-responsive"><i class="fa fa-trash-o fa-fw"></i>{{ Lang::get('admin.remove-address') }}</button>
													<input type="hidden" name="address_id[]" class="address-id">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="modal-footer">
							<input type="hidden" name="id" id="row-id" value="">
							<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-fw fa-close"></i>{{ Lang::get('admin.modal-close') }}</button>
	        		<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-save"></i>{{ Lang::get('forms.save-changes') }}</button>
						</div>
					{{ Form::close() }}
				</div>
			</div>
		</div>
@stop

