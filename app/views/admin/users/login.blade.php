@extends('layouts.admin')

@section('scripts')
  @include('includes.validator')
  {? $add_js[] = '/js/vendor/jquery.validate.min.js' ?}
  <script type="text/javascript">
    $(document).ready(function() {
        $("#login-auth").focus();
    });
  </script>
@stop

@section('breadcrumbs')
  @include('includes.breadcrumbs-admin')
@stop

@section('content')
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12">
            <div class="panel panel-danger">
              <div class="panel-heading"><span class="fa fa-puzzle-piece fa-fw"></span>{{ Lang::get('admin.admin-panel') }}</div>
              <div class="panel-body">
                {{ Form::open(array('route' => 'admin-login', 'id' => 'form-login')) }}
                  <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                      <div class="form-group has-feedback{{ $errors->has('auth') ? ' has-errors' : '' }}">
                        <label for="login-auth">{{ Lang::get('forms.email-username-ph') }}</label>
                        <input type="text" name="auth" id="login-auth" class="form-control" value="{{ Input::old('auth') }}" required>
                        @if($errors->has('auth'))
                          <div for="login-auth" class="label label-danger">{{ $errors->first('auth') }}</div>
                          <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                      <div class="form-group has-feedback{{ $errors->has('password') ? ' has-errors' : '' }}">
                        <label for="login-password">{{ Lang::get('user.password') }}</label>
                        <input type="password" minlength="8" name="password" id="login-password" class="form-control" required>
                        @if($errors->has('password'))
                          <div for="login-password" class="label label-danger">{{ $errors->first('password') }}</div>
                          <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3 col-sm-8 col-md-offset-2 col-sm-offset-2">
                      <input class="hide" type="text" name="honeypot" value="">
                      <button type="submit" name="submit" class="btn btn-danger btn-block">{{ Lang::get('forms.login') }}</button>
                    </div>
                  </div>
                {{ Form::close() }}
              </div>
            </div>
          </div>
        </div>
      </div>
@stop