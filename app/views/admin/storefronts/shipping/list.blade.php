@extends('layouts.admin')

@section('styles')
  {? $add_css[] = '/css/summernote.css' ?}
@stop

@section('scripts')
{? $add_js[] = '/js/vendor/moment.min.js' ?}
{? $add_js[] = '/js/vendor/summernote.min.js' ?}
<script type="text/javascript">
	// refresh the list
	function refresh() {
			var sort = urlData('s') || '';
			var order = urlData('o') || '';
			var page = urlData('page') || '';
			var loading = $("#load-list");
			var table = $("#rows");

			loading.removeClass('hide');

			$.post("{{ url('admin/storefronts/refresh') }}", { s: sort, o: order, page: page }, function(response) {
					// clear table
					table.html("");

					// populate table
					$.each(response.rows.data, function(index, row) {
							var last_sync = moment(row.last_sync);
							
							table.append(
									'<tr data-id="' + row.id + '"' + (row.deployed == 0 ? 'class="warning"' : (row.status == 0 ? ' class="danger"' : '')) + '>' +
									'<td class="text-center">' + (row.deployed == 1 ? (row.status == 1 ? '<button class="btn btn-link btn-xs btn-icon btn-deactivate" title="{{ Lang::get('admin.active') }}"><i class="fa fa-check-square-o fa-fw text-success"></i></button>' : '<button class="btn btn-link btn-xs btn-icon btn-activate" title="{{ Lang::get('admin.inactive') }}"><i class="fa fa-square-o fa-fw text-danger"></i></button>') : '') + '</td>' +
									'<td>' + row.store_name + '</td>' +
									'<td' + (row.total_products == 0 ? ' class="text-danger"' : '') + '>' + row.total_products + '</td>' +
									'<td>' + row.store_url + '</td>' +
									'<td>' + row.username + '</td>' +
									'<td>' + (row.last_sync ? last_sync.format("YYYY-MM-DD h:mm A") : "{{ Lang::get('admin.store-sync-never') }}") + '</td>' +
									'<td class="no-wrap">' +
									'<button class="btn btn-primary btn-xs btn-icon btn-edit" title="{{ Lang::get('admin.edit') }}"><i class="fa fa-pencil fa-fw"></i></button>' +
									(row.deployed == 0 ? '&nbsp;<button class="btn btn-success btn-xs btn-icon btn-deploy" title="{{ Lang::get('admin.store-deploy') }}"><i class="fa fa-rocket fa-fw"></i></button>' : (row.status == 1 ? '&nbsp;<button class="btn btn-info btn-xs btn-icon btn-sync" title="{{ Lang::get('admin.store-sync') }}"><i class="fa fa-refresh fa-fw"></i></button>' : '')) +
									'</tr>'
							);
					});

					// update links
					$("#row-pages").html(response.pages);

					loading.addClass('hide');
			}, 'json');
	}

	$(document).ready(function() {
			// edit button
			$(document).on('click', '.btn-edit', function() {
      		var id = $(this).parent().parent().data('id');
      		var btn = $(this);

      		btn.find('i').removeClass('fa-pencil').addClass('fa-spinner fa-spin');

      		// reset all form fields
					$("#modal-form").find('form').trigger("reset");
					$("#row-id").val("");

        	$.post("{{ url('admin/storefronts/info') }}", { id: id }, function(response) {
        			if(!response.error) {
									// set form title
        					$("#form-title").html("{{ Lang::get('admin.form-view-title') }} <strong>" + response.row.store_name +  '</strong>');

        					// populate addresses
        					var addresses = $("#row-address_id");

        					if(response.addresses.length > 0) {
        							var html = '<option value="">{{ Lang::get('forms.none') }}</option>';

											$.each(response.addresses, function(index, address) {
													html += '<option value="' + address.id + '">' + address.address + '</option>';
											});

											addresses.html(html).attr('disabled', false);
        					} else {
        							addresses.html('<option value="">{{ Lang::get('admin.user-no-addresses') }}</option>').attr('disabled', true);
        					}
		        					
        					// output form data
        					$.each(response.row, function(index, value) {
        							var field = $("#row-" + index);

        							// field exists, therefore populate
        							if(field.length > 0) {
        									// properly format store URL if not custom
        									if(index == 'store_url') {
        											if(response.row.custom_url == 0) {
        													var store_url = value.replace('http://', '').split('.');
		    													field.val(store_url[0]);
        											} else {
        													field.val(null);
        											}
        									} else if(index == 'description') {
        											$("#row-description").code(value);
    											} else {
    													if(index == 'custom_url') {
    															var toggle_box = $("#use-custom-url");

    															if(value == 1) {
		    															value = response.row.store_url;
		    															toggle_box.prop('checked', true);
    															} else {
    																	value = null;
    																	toggle_box.prop('checked', false);
    															}

    															toggle_box.trigger('change');
		        									}

        											field.val(value);
        									}
        							}
        					});

        					// show form
        					$("#modal-form").modal('show');
        			} else {
        					status(false, response.error, 'alert-danger');
        			}

        			btn.find('i').removeClass('fa-spinner fa-spin').addClass('fa-pencil');
        	}, 'json');
      });

			// activate button
      $(document).on('click', '.btn-activate', function() {
      		var id = $(this).parent().parent().data('id');
      		var name = $(this).parent().parent().find('td:nth-child(2)').html();

      		dialog("{{ Lang::get('admin.store-activation') }}", "{{ Lang::get('admin.store-activate-q') }} <strong>" + name + '</strong>?', "{{ url('admin/storefronts/activate') }}", id);
      });

			// deactivate button
      $(document).on('click', '.btn-deactivate', function() {
      		var id = $(this).parent().parent().data('id');
      		var name = $(this).parent().parent().find('td:nth-child(2)').html();

      		dialog("{{ Lang::get('admin.store-deactivation') }}", "{{ Lang::get('admin.store-deactivate-q') }} <strong>" + name + '</strong>?', "{{ url('admin/storefronts/deactivate') }}", id);
      });

      // sync button
      $(document).on('click', '.btn-sync', function() {
      		var id = $(this).parent().parent().data('id');
      		var icon = $(this).find('.fa');
      		var last_sync_box = $(this).parent().parent().find('td:nth-child(6)');
      		var button = $(this);

      		button.attr('title', "{{ Lang::get('admin.store-synchronizing') }}").attr("disabled", true);
      		icon.addClass('fa-spin');

      		$.post("{{ url('admin/storefronts/sync') }}", { id: id }, function(response) {
      				if(response.error) {
      						status(null, response.error, 'alert-danger');
      				} else {
      						var last_sync = moment(response.last_sync);

      						last_sync_box.html(last_sync.format("YYYY-MM-DD h:mm A"));
      						status(null, response.body + '<br /><br /><samp>' + response.output + '</samp>', 'alert-success');
      				}

      				button.attr('title', "{{ Lang::get('admin.store-sync') }}").attr("disabled", false);
      				icon.removeClass('fa-spin');
      		});
      });

      // deploy button
      $(document).on('click', '.btn-deploy', function() {
      		var id = $(this).parent().parent().data('id');
      		var name = $(this).parent().parent().find('td:nth-child(2)').html();

      		dialog("{{ Lang::get('admin.store-deployment') }}", "{{ Lang::get('admin.store-deploy-q') }} <strong>" + name + '</strong>?', "{{ url('admin/storefronts/deploy') }}", id);
      });

      // custom URL switch
      $(document).on('click, change', '#use-custom-url', function() {
      		var store_url = $("#store-url-group");
      		var custom_url = $("#row-custom_url");

      		if($(this).is(':checked')) {
      				store_url.addClass('hide');
      				custom_url.removeClass('hide');
      		} else {
      				store_url.removeClass('hide');
      				custom_url.addClass('hide');
      		}
      });

      $('#row-description').summernote({
          height: 200,
          toolbar: [
              ['style', ['bold', 'italic', 'underline', 'clear']],
              ['fontsize', ['fontsize']],
              ['color', ['color']],
              ['para', ['ul', 'ol', 'paragraph']],
              ['height', ['height']],
              ['fontname', ['fontname']]
          ]
      });
	});

</script>
@stop

@section('content')
		<div class="container admin-content">
			<h2>{{ Lang::get('admin.store-management') }}</h2>
			<div class="row">
				<div class="col-lg-12">
					<div class="table-responsive">
						<div id="load-list" class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-5x centered"></i></div>
            </div>
						<table class="table table-condensed table-bordered">
							<thead>
								<tr>
									<th></th>
									<th class="col-md-3"><a href="{{ url('admin/storefronts') . '?s=store_name&amp;o=' . ($sort == 'store_name' && $order == 'asc' ? 'desc' : 'asc') . '&amp;page=' . $rows->getCurrentPage() }}">{{ Lang::get('store.store-name')}}{{ ($sort == 'store_name' ? ' <i class="fa fa-sort-' . $order . '"></i>' : '') }}</a></th>
									<th class="col-md-2"><a href="{{ url('admin/storefronts') . '?s=total_products&amp;o=' . ($sort == 'total_products' && $order == 'asc' ? 'desc' : 'asc') . '&amp;page=' . $rows->getCurrentPage() }}">{{ Lang::get('admin.total-products')}}{{ ($sort == 'total_products' ? ' <i class="fa fa-sort-' . $order . '"></i>' : '') }}</a></th>
									<th class="col-md-3"><a href="{{ url('admin/storefronts') . '?s=store_url&amp;o=' . ($sort == 'store_url' && $order == 'asc' ? 'desc' : 'asc') . '&amp;page=' . $rows->getCurrentPage() }}">{{ Lang::get('store.store-url')}}{{ ($sort == 'store_url' ? ' <i class="fa fa-sort-' . $order . '"></i>' : '') }}</a></th>
									<th class="col-md-2"><a href="{{ url('admin/storefronts') . '?s=users.username&amp;o=' . ($sort == 'users.username' && $order == 'asc' ? 'desc' : 'asc') . '&amp;page=' . $rows->getCurrentPage() }}">{{ Lang::get('admin.merchant-username')}}{{ ($sort == 'users.username' ? ' <i class="fa fa-sort-' . $order . '"></i>' : '') }}</a></th>
									<th class="col-md-2"><a href="{{ url('admin/storefronts') . '?s=last_sync&amp;o=' . ($sort == 'last_sync' && $order == 'asc' ? 'desc' : 'asc') . '&amp;page=' . $rows->getCurrentPage() }}">{{ Lang::get('admin.last-sync-date') }}{{ ($sort == 'last_sync' ? ' <i class="fa fa-sort-' . $order . '"></i>' : '') }}</a></th>
									<th></th>
								</tr>
							</thead>
							<tbody id="rows">
								@foreach($rows as $row)
								<tr data-id="{{ $row->id }}"{{ $row->deployed == 0 ? 'class="warning"' : ($row->status == 0 ? 'class="danger"' : '') }}>
									<td class="text-center">
										@if($row->deployed == 1)
											@if($row->status == 1)
												<button class="btn btn-link btn-xs btn-icon btn-deactivate" title="{{ Lang::get('admin.active') }}"><i class="fa fa-check-square-o fa-fw text-success"></i></button>
											@else
												<button class="btn btn-link btn-xs btn-icon btn-activate" title="{{ Lang::get('admin.inactive') }}"><i class="fa fa-square-o fa-fw text-danger"></i></button>
											@endif
										@endif
									</td>
									<td>{{ $row->store_name }}</td>
									<td{{ $row->total_products == 0 ? ' class="text-danger"' : '' }}>{{ $row->total_products }}</td>
									<td>{{ $row->store_url }}</td>
									<td>{{ $row->user->username }}</td>
									<td>{{ $row->last_sync ? date("Y-m-d g:i A", strtotime($row->last_sync)) : Lang::get('admin.store-sync-never') }}</td>
									<td class="no-wrap">
										<button class="btn btn-primary btn-xs btn-icon btn-edit" title="{{ Lang::get('admin.edit') }}"><i class="fa fa-pencil fa-fw"></i></button>
										@if($row->deployed == 0)
											<button class="btn btn-success btn-xs btn-icon btn-deploy" title="{{ Lang::get('admin.store-deploy') }}"><i class="fa fa-rocket fa-fw"></i></button>
										@elseif($row->status == 1)
											<button class="btn btn-info btn-xs btn-icon btn-sync" title="{{ Lang::get('admin.store-sync') }}"><i class="fa fa-refresh fa-fw"></i></button>
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<div id="row-pages" class="text-center">{{ $pages; }}</div>
			</div>
		</div>

		<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div id="load-form" class="loading-pane hide">
            <div><i class="fa fa-inverse fa-spinner fa-spin fa-5x centered"></i></div>
          </div>
					{{ Form::open(array('url' => 'admin/storefronts/save', 'role' => 'form')) }}
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{{ Lang::get('admin.modal-close') }}</span></button>
	        		<h4 id="form-title" class="modal-title"></h4>
						</div>
						<div class="modal-body">
							<div id="form-notice"></div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label for="row-store_name">{{ Lang::get('store.store-name') }}</label>
								    <input type="text" name="store_name" class="form-control" id="row-store_name" maxlength="50" placeholder="{{ Lang::get('admin.chars-max', array('num' => 50)) }}">
									</div>
									<div class="form-group">
										<div>
											<label for="row-store_url">{{ Lang::get('store.store-url') }}</label>
											<div class="pull-right">
												<input type="checkbox" name="use_custom_url" value="1" id="use-custom-url"> {{ Lang::get('admin.store-custom-url') }}
											</div>
										</div>
								    <div class="input-group" id="store-url-group">
	                    <input type="text" name="store_url" class="form-control" id="row-store_url" minlength="4" maxlength="15" placeholder="{{ Lang::get('admin.chars-min', array('num' => 4)) . ', ' . Lang::get('admin.chars-max', array('num' => 15)) }}">
	                    <span class="input-group-addon"><strong>{{ Lang::get('navigation.megamall-subdomain') }}</strong></span>
	                  </div>
										<input type="text" name="custom_url" class="form-control hide" id="row-custom_url" maxlength="100" placeholder="{{ Lang::get('admin.valid-url') . ', ' . Lang::get('admin.chars-max', array('num' => 100)) }}">
									</div>
									<div class="form-group">
										<label for="row-permalink">{{ Lang::get('admin.slug') }}</label>
								    <input type="text" name="permalink" class="form-control" id="row-permalink" maxlength="32" placeholder="{{ Lang::get('admin.valid-slug') }}">
									</div>
									<div class="form-group">
										<label for="row-email">{{ Lang::get('user.email') }}</label>
								    <input type="email" name="email" class="form-control" id="row-email" placeholder="{{ Lang::get('user.email') }}">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label for="row-email">{{ Lang::get('user.email') }}</label>
								    <input type="email" name="email" class="form-control" id="row-email" placeholder="{{ Lang::get('user.email') }}">
									</div>
									<div class="form-group">
										<label for="row-telephone">{{ Lang::get('user.telephone') }}</label>
								    <input type="text" name="telephone" class="form-control" id="row-telephone">
									</div>
									<div class="form-group">
										<label for="row-fax">{{ Lang::get('user.fax') }}</label>
								    <input type="text" name="fax" class="form-control" id="row-fax">
									</div>
									<div class="form-group">
										<label for="row-address_id">{{ Lang::get('store.physical-address') }}</label>
								    <select name="address_id" class="form-control" id="row-address_id" disabled>
					    				<option value="">{{ Lang::get('admin.user-no-addresses') }}</option>
					    			</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<label for="row-description">{{ Lang::get('store.store-description') }}</label>
								    <textarea name="description" class="form-control" id="row-description" rows="5"></textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" name="id" id="row-id" value="">
							<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-fw fa-close"></i>{{ Lang::get('admin.modal-close') }}</button>
	        		<button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-save"></i>{{ Lang::get('forms.save-changes') }}</button>
						</div>
					{{ Form::close() }}
				</div>
			</div>
		</div>
@stop

