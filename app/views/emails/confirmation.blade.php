@extends('layouts.email')

@section('content')
    <p>{{ Lang::get('email.greeting', array('firstname' => $firstname)) }}</p>
    <p style="margin: 20px 0;">{{ Lang::get('email.user-confirm-body') }}</p>
    <div>
    <!--[if mso]>
      <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="{{ url('signup/confirm/' . $code) }}" style="height:40px;width:400px;v-text-anchor:middle;padding:10px 15px;" stroke="f" fillcolor="#d9534f">
        <w:anchorlock/>
        <center style="color:#fff;font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:bold;">
          {{ Lang::get('email.user-confirm-btn-text', array('email' => $email)) }}
        </center>
      </v:rect>
      <![endif]-->
      <![if !mso]>
      <table cellspacing="0" cellpadding="0"> <tr> 
      <td align="center" height="40" bgcolor="#d9534f" style="color: #fff; display: block;">
        <a href="{{ url('signup/confirm/' . $code) }}" style="color: #fff; font-size:14px; font-weight: bold; font-family: Helvetica, Arial, sans-serif; text-decoration: none; line-height: 40px; width: 100%; display: inline-block">
          {{ Lang::get('email.user-confirm-btn-text', array('email' => $email)) }}
        </a>
      </td> 
      </tr> </table> 
      <![endif]>
    </div>
    <p style="margin: 20px 0;">{{ Lang::get('email.alt-link') }}
    <p><a href="{{ url('signup/confirm/' . $code) }}" target="_blank">{{ url('signup/confirm/' . $code) }}</a></p>
    <p style="margin-top: 20px;">{{ Lang::get('email.thanks') }}<br>{{ Lang::get('email.sender') }}</p>
@stop