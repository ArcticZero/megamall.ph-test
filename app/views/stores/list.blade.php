@extends('layouts.master')

@section('scripts')
  {? $add_js[] = '/js/vendor/jquery.raty.js' ?}
  {? $add_js[] = '/js/store-list.js' ?}
@stop

@section('breadcrumbs')
  @include('includes.breadcrumbs')
@stop

@section('content')
      <div class="container">
        <div class="row sub-title">
          <h3 class="col-md-12">{{ $name }}</h3>
        </div>
        @if($total_stores > 0)
        <div class="row">
          {{ print_r($stores) }}
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
            
          </div>
        </div>
        @else
        <div class="row">
          <div class="col-md-12">
            <div class="text-center">
              <h4>{{ Lang::get('store.no-stores') }}</h4>
            </div>
          </div>
        </div>
        @endif
      </div>
@stop