@extends('layouts.master')

@section('styles')
  {? $add_css[] = '/css/summernote.css' ?}
  {? $add_css[] = '/css/fileinput.min.css' ?}
@stop

@section('scripts')
  @include('includes.validator')
  {? $add_js[] = '/js/vendor/jquery.validate.min.js' ?}
  {? $add_js[] = '/js/vendor/summernote.min.js' ?}
  {? $add_js[] = '/js/vendor/fileinput.min.js' ?}
  {? $add_js[] = '/js/store-apply.js' ?}
@stop

@section('breadcrumbs')
  @include('includes.breadcrumbs')
@stop

@section('content')
      <div class="container">
        {{ Form::open(array('url' => 'stores/create', 'files' => true, 'id' => 'form-create-store')) }}
          <div class="row">
            <div class="col-md-12">
              <h3>{{ Lang::get('store.create-store-head') }}</h3>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              @include('users.sidebar')
            </div>
            <div class="col-md-8">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#store-info" data-toggle="tab">{{ Lang::get('store.store-info') }}</a></li>
                <li class="pull-right hidden-xs">
                  <input type="submit" class="btn btn-danger" value="{{ Lang::get('forms.submit-application') }}">
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane fade in active" id="store-info">
                  <div class="panel panel-primary">
                    <div class="panel-heading">
                      {{ Lang::get('store.identification') }}
                    </div>
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ $errors->has('store_name') ? ' has-errors' : '' }}">
                            <label for="store-name">{{ Lang::get('store.store-name') }}</label>
                            <input type="text" name="store_name" id="store-name" class="form-control" minlength="4" value="{{ Input::old('store_name') }}" required>
                            @if($errors->has('store_name'))
                              <div for="store-name" class="label label-danger">{{ $errors->first('store_name') }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ $errors->has('store_url') ? ' has-errors' : '' }}">
                            <label for="store-url">{{ Lang::get('store.store-url') }}</label>
                            <div class="input-group">
                              <input type="text" name="store_url" id="store-url" class="form-control" minlength="4" value="{{ Input::old('store_url') }}" required>
                              <span class="input-group-addon"><strong>{{ Lang::get('navigation.megamall-subdomain') }}</strong></span>
                            </div>
                            @if($errors->has('store_url'))
                              <div for="store-url" class="label label-danger">{{ $errors->first('store_url') }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-primary">
                    <div class="panel-heading">{{ Lang::get('store.store-description') }}</div>
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-12 col-sm-8 col-sm-offset-2 col-md-offset-0">
                          <div class="form-group has-feedback{{ $errors->has('description') ? ' has-errors' : '' }}">
                            <textarea name="description" class="form-control" id="store-description" rows="8">{{ Input::old('description') }}</textarea>
                            @if($errors->has('description'))
                              <div for="store-description" class="label label-danger">{{ $errors->first('description') }}</div>
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-primary">
                    <div class="panel-heading">{{ Lang::get('store.related-categories') }}</div>
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-12 col-sm-8 col-sm-offset-2 col-md-offset-0">
                          <div class="form-group has-feedback{{ $errors->has('categories') ? ' has-errors' : '' }}">
                            <label for="store-categories">{{ Lang::get('store.select-categories') }}</label>
                            @if(count($search_cats) > 0)
                            <select name="categories[]" id="store-categories" class="selectpicker show-menu-arrow form-control" data-live-search="true" multiple>
                              @foreach($search_cats as $cat)
                              <optgroup label="{{ $cat['name'] }}">
                                @foreach($cat->children as $subcat)
                                <option value="{{ $subcat['id'] }}"{{ is_array(Input::old('categories')) && in_array($subcat['id'], Input::old('categories')) ? ' selected="selected"' : '' }}>{{ $subcat['name'] }}</option>
                                @endforeach
                              </optgroup>
                              @endforeach
                            </select>
                            @endif
                            @if($errors->has('categories'))
                              <div for="store-categories" class="label label-danger">{{ $errors->first('categories') }}</div>
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-primary">
                    <div class="panel-heading">{{ Lang::get('store.contact-info') }}</div>
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-12 col-sm-8 col-sm-offset-2 col-md-offset-0">
                          <div class="form-group has-feedback{{ $errors->has('store_address_id') ? ' has-errors' : '' }}">
                            <label for="store-address-id">{{ Lang::get('store.physical-address') }}</label>
                            @if(count($addresses) > 0)
                            <select name="store_address_id" id="store-address-id" class="form-control selectpicker show-menu-arrow">
                              <option value="">[{{ Lang::get('forms.not-applicable') }}]</option>
                              @foreach($addresses as $id => $address)
                              {{ '<option value="' . $id . '"' . ($id == Input::old('store_address_id') ? ' selected="selected"' : '') . '>' .$address . '</option>' }}
                              @endforeach
                            </select>
                            @else
                            <div class="well">
                              <i class="fa fa-exclamation-triangle fa-fw"></i>
                              {{ Lang::get('user.no-addresses', array('add-one' => link_to('profile/addresses', Lang::get('user.add-address')))) }}
                            </div>
                            @endif
                            @if($errors->has('store_address_id'))
                              <div for="address-id" class="label label-danger">{{ $errors->first('address_id') }}</div>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ $errors->has('store_telephone') ? ' has-errors' : '' }}">
                            <label for="store-telephone">{{ Lang::get('user.telephone') }}</label>
                            <input type="text" name="store_telephone" id="store-telephone" class="form-control" value="{{ Input::old('store_telephone') }}">
                            @if($errors->has('store_telephone'))
                              <div for="store-telephone" class="label label-danger">{{ $errors->first('store_telephone') }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ $errors->has('store_fax') ? ' has-errors' : '' }}">
                            <label for="store-fax">{{ Lang::get('user.fax') }}</label>
                            <input type="text" name="store_fax" id="store-fax" class="form-control" value="{{ Input::old('store_fax') }}">
                            @if($errors->has('store_fax'))
                              <div for="store-fax" class="label label-danger">{{ $errors->first('store_fax') }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          <div class="form-group has-feedback{{ $errors->has('store_email') ? ' has-errors' : '' }}">
                            <label for="store-email">{{ Lang::get('user.email') }}</label>
                            <input type="email" name="store_email" id="store-email" class="form-control" value="{{ Input::old('store_email') }}">
                            @if($errors->has('store_email'))
                              <div for="store-email" class="label label-danger">{{ $errors->first('store_email') }}</div>
                              <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-primary">
                    <div class="panel-heading">{{ Lang::get('store.personal-id') }}</div>
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-12 col-sm-8 col-sm-offset-2 col-md-offset-0">
                          <div class="form-group has-feedback{{ $errors->has('proof') ? ' has-errors' : '' }}">
                            <label for="proof">{{ Lang::get('store.proof-label') }}</label>
                            <input type="file" name="proof[]" id="proof" class="form-control" multiple required>
                            @if($errors->has('proof'))
                              <div for="proof" class="label label-danger">{{ $errors->first('proof') }}</div>
                            @endif
                          </div>
                          <div class="well">
                            <i class="fa fa-exclamation-triangle fa-fw"></i>
                            {{ Lang::get('store.proof-warning') }}
                            <ul>
                              <li>{{ Lang::get('store.proof-of-identity') }} <small>({{ Lang::get('store.proof-identity-note') }})</small></li>
                              <li>{{ Lang::get('store.proof-of-address') }} <small>({{ Lang::get('store.proof-address-note') }})</small></li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="row form-note">
                        <div class="col-md-12 col-sm-8 col-md-offset-0 col-sm-offset-2">
                          {{ Lang::get('store.create-accept-terms', array('terms' => link_to('terms/store', Lang::get('store.store-terms'), array('target' => '_blank')))) }}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        {{ Form::close() }}
      </div>
@stop