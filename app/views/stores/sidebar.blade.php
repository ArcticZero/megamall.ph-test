            @if(Route::currentRouteAction() == 'StoreController@view')
            <div class="list-group">
              <a href="{{ $store->store_url }}" class="list-group-item list-group-item-danger" title="{{ Lang::get('store.visit-store') }}">
                <i class="fa fa-tags fa-fw" title="{{ Lang::get('store.visit-store') }}"></i>{{ Lang::get('store.visit-store') }}
              </a>
            </div>
            @else
            @if(Auth::user() && Auth::user()->id == $store->user_id)
            <div class="list-group">
              <a href="{{ $store->store_url }}admin" class="list-group-item list-group-item-danger" title="{{ Lang::get('store.store-admin') }}">
                <i class="fa fa-key fa-fw" title="{{ Lang::get('store.store-admin') }}"></i>{{ Lang::get('store.store-admin') }}
              </a>
            </div>
            @endif
            @endif
            <div class="list-group">
              @if(Route::currentRouteAction() != 'StoreController@view')
              <a href="{{ url('stores/' . $store->permalink) }}" class="list-group-item red" title="{{ Lang::get('store.view-portal') }}">
                <i class="fa fa-tags fa-fw" title="{{ Lang::get('store.store-name') }}"></i><strong>{{ $store->store_name }}</strong>
              </a>
              @endif
              @if(Route::currentRouteAction() == 'StoreController@view' && $store->user->avg_sell_rating)
              <a class="list-group-item list-group-item-primary no-link">
                <small>{{ Lang::get('store.average-rating') }}:</small>
                <div class="star-rating pull-right" data-score="{{ $store->user->avg_sell_rating }}"></div>
              </a>
              @endif
              @if($store->address)
              <a class="list-group-item no-link">
                <i class="fa fa-home fa-fw blue" title="{{ Lang::get('user.address') }}"></i>
                {{ implode(", ", array_filter(array($store->address->city, $store->address->zone->name, $store->address->country->name))) }}
              </a>
              @endif
              <a class="list-group-item no-link">
                <i class="fa fa-phone fa-fw blue" title="{{ Lang::get('user.phone') }}"></i>
                {{ $store->telephone ? $store->telephone : '<em>' . Lang::get('user.no-telephone') . '</em>' }}
              </a>
              @if($store->fax)
              <a class="list-group-item no-link">
                <i class="fa fa-fax fa-fw blue" title="{{ Lang::get('user.fax') }}"></i>
                {{ $store->fax }}
              </a>
              @endif
              @if($store->email)
              <a href="mailto:{{ HTML::email($store->email) }}" class="list-group-item">
                <i class="fa fa-envelope fa-fw red" title="{{ Lang::get('user.email') }}"></i>
                {{ HTML::email($store->email) }}
              </a>
              @endif
            </div>
            @if(Route::currentRouteAction() == 'StoreController@view' && $store->description)
            <div class="list-group">
              <a class="list-group-item no-link">
                {{ $store->description }}
              </a>
            </div>
            @endif