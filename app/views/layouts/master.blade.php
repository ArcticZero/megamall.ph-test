<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>{{ $title or Lang::get('seo.title') }} - {{ Lang::get('seo.megamall') }}</title>
    <meta name="description" content="{{ Lang::get('seo.description') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    {? $css[] = '/css/bootstrap.css' ?}
    {? $css[] = '/css/bootstrap_ms.css' ?}
    {? $css[] = '/css/bootstrap-social.css' ?}
    {? $css[] = '/css/bootstrap-select.min.css' ?}
    {? $css[] = '/css/main.css' ?}

    @yield('styles')

    {? if(isset($add_css)) $css = array_merge($css, $add_css) ?}
    {{ Minify::stylesheet($css, array('type' => 'text/css', 'media' => 'all'))->withFullUrl() }}

    {{ HTML::style('//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css') }}

    {{ HTML::script('//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js') }}
    <script>window.jQuery || document.write('<script src="{{ URL::to('js/vendor/jquery-1.11.0.min.js') }}"><\/script>')</script>
    
    <script>
  		(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
  		function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
  		e=o.createElement(i);r=o.getElementsByTagName(i)[0];
  		e.src='//www.google-analytics.com/analytics.js';
  		r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
  		ga('create','UA-1806911-54');ga('send','pageview');
  	</script>
  </head>
  <body>
  <header>
    <nav id="navbar-top" class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-top-content">
                <span class="sr-only">{{ Lang::get('navigation.toggle') }}</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              @if(Auth::check())
              <div class="pull-right navbar-brand hidden-sm hidden-md hidden-lg">
                @if($unread['pm'] > 0)
                <a href="{{ url('messages') }}" title="{{ Lang::get('user.unread-messages') }}"><span class="badge badge-danger"><i class="fa fa-comment fa-fw"></i><span class="unread-pm">{{ $unread['pm'] }}</span></span></a>
                @endif
                @if($unread['fb'] > 0)
                <a href="{{ url('profile') }}" title="{{ Lang::get('user.unread-feedback') }}"><span class="badge badge-danger"><i class="fa fa-envelope fa-fw"></i><span class="unread-fb">{{ $unread['fb'] }}</span></span></a>
                @endif
              </div>
              @endif
              <a class="navbar-brand hidden-sm hidden-md hidden-lg" href="{{ url() }}">{{ HTML::image('img/megamall-logo-md.png', $alt='Megamall.ph') }}</a>
            </div>
            <div id="navbar-top-content" class="navbar-collapse collapse">
              <ul class="nav navbar-nav pull-right">
                <li{{ Request::path() == '/' ? $active : '' }}><a href="{{ url('/') }}"><i class="fa fa-home fa-fw"></i>{{ Lang::get('navigation.home') }}</a></li>
                @if(Auth::check())
                <li class="dropdown{{ Request::path() == '' ? ' active' : '' }}">
                  <a href="{{ url('profile') }}" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-user fa-fw"></i>{{ Lang::get('navigation.account') }}
                    @if($unread['pm'] + $unread['fb'] > 0)
                    <span class="badge badge-danger unread-all">{{ $unread['pm'] + $unread['fb'] }}</span>
                    @endif
                    &nbsp;<span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                    <li><a href="{{ url('profile/edit') }}"><i class="fa fa-user fa-fw"></i>{{ Lang::get('user.edit-profile') }}</a></li>
                    <li>
                      <a href="{{ url('messages') }}">
                        <i class="fa fa-envelope fa-fw"></i>{{ Lang::get('user.messages') }}
                        @if($unread['pm'] > 0)
                        <span class="badge badge-danger unread-pm">{{ $unread['pm'] }}</span>
                        @endif
                      </a>
                    </li>
                    <li>
                      <a href="{{ url('profile') }}">
                        <i class="fa fa-comment fa-fw"></i>{{ Lang::get('user.my-feedback') }}
                        @if($unread['fb'] > 0)
                        <span class="badge badge-danger unread-fb">{{ $unread['fb'] }}</span>
                        @endif
                      </a>
                    </li>
                    <li><a href="{{ url('profile/addresses') }}"><i class="fa fa-book fa-fw"></i>{{ Lang::get('user.manage-addresses') }}</a></li>
                    <li><a href="{{ url('profile/bookmarks') }}"><i class="fa fa-bookmark fa-fw"></i>{{ Lang::get('user.manage-bookmarks') }}</a></li>
                    <!--
                    <li class="divider"></li>
                    <li><a href="{{ url('profile/settings') }}"><i class="fa fa-cog fa-spin fa-fw"></i>{{ Lang::get('user.settings') }}</a></li>
                    -->
                    <li class="divider"></li>
                    <li><a href="{{ url('logout') }}"><i class="fa fa-sign-out fa-fw"></i>{{ Lang::get('navigation.logout') }}</a></li>
                  </ul>
                </li>
                @endif
                <li{{ Request::path() == 'stores' ? $active : '' }}><a href="{{ url('stores') }}"><i class="fa fa-shopping-cart fa-fw"></i>{{ Lang::get('navigation.stores') }}</a></li>
                <li{{ Request::path() == 'help' ? $active : '' }}><a href="{{ url('help') }}"><i class="fa fa-question-circle fa-fw"></i>{{ Lang::get('navigation.help') }}</a></li>
                @if(Auth::check() && !Auth::user()->store)
                <li id="link-create-store"><a href="{{ url('stores/create') }}"><i class="fa fa-tags fa-fw"></i>{{ Lang::get('store.create-your-store') }}</a></li>
                @endif
              </ul>
              <div class="nav-welcome hidden-xs">
              @if(Auth::check())
                {{ Lang::get('navigation.welcome-user', array('firstname' => link_to('profile', Auth::user()->firstname))) }}
              @else
                {{ Lang::get('navigation.welcome-guest', array('login' => link_to('login', Lang::get('navigation.login'), array('id' => 'link-login', 'onclick' => 'return false;')), 'register' => link_to('signup', Lang::get('navigation.register')))) }}
              @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
    <div class="container">
      <div class="row visible-xs">
        @if(Auth::check())
        <div class="col-xs-12">
          <h4>{{ Lang::get('navigation.welcome-user', array('firstname' => link_to('profile', Auth::user()->firstname))) }}</h4>
        </div>
        @else
        <div class="col-xs-6">
          <a class="btn btn-danger btn-lg btn-block" href="{{ url('login') }}">{{ Lang::get('forms.login') }}</a>
        </div>
        <div class="col-xs-6">
          <a class="btn btn-primary btn-lg btn-block" href="{{ url('signup') }}">{{ Lang::get('forms.register') }}</a>
        </div>
        @endif
      </div>
      <div class="row">
        <div class="col-md-2 hidden-xs">
          <a class="logo" href="{{ url() }}">{{ HTML::image('img/megamall-logo-md.png', $alt='Megamall.ph') }}</a>
        </div>
        <div class="col-md-9 col-md-offset-1">
          {{ Form::open(array('route' => 'search', 'method' => 'get', 'id' => 'search-form')) }}
            <div class="input-group">
              <input type="text" name="s" class="form-control" value="{{ Input::get('s') }}" placeholder="{{ Lang::get('forms.terms-ph') }}" x-webkit-speech>
              <div class="input-group-btn hidden-xxs">
                <select name="c" class="selectpicker show-menu-arrow show-tick">
                  <option value="">{{ Lang::get('forms.all-categories') }}</option>
                  @foreach($search_cats as $cat)
                  <option value="{{ $cat['id'] }}"{{ $cat['id'] == Input::get('c') ? ' selected="selected"' : '' }}>{{ $cat['name'] }}</option>
                  @endforeach
                </select>
              </div>
              <div class="input-group-btn">
                <button class="btn btn-danger" type="submit"><i class="fa fa-search fa-fw"></i><span class="hidden-xs">{{ Lang::get('forms.search') }}</span></button>
              </div>
            </div>
          {{ Form::close() }}
        </div>
      </div>
      <div class="row category-nav">
        <div class="col-md-12">
          <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
              <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="sr-only">{{ Lang::get('navigation.toggle') }}</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand hidden-md hidden-lg hidden-sm" href="#">{{ Lang::get('navigation.categories') }}</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-main">
              <ul class="nav navbar-nav">
                @foreach($cat_bar as $cat)
                <li>{{ link_to($cat['permalink'], $cat['name']) }}</li>
                @endforeach
                <li>{{ link_to('categories', Lang::get('navigation.more-categories')) }}</li>
              </ul>
            </div>
          </nav>
        </div>
      </div>
    </div>
    @if(!Auth::check())
    <div id="login-box" class="hide">
      <div class="box-title">{{ Lang::get('forms.login_to') . HTML::image('img/megamall-logo-sm.png', $alt=Lang::get('seo.megamall')) }}</div>
      <div class="box-content">
        {{ Form::open(array('route' => 'login')) }}
          <div class="form-group">
            <input type="text" class="form-control" name="auth" placeholder="{{ Lang::get('forms.email-username-ph') }}">
          </div>
          <div class="form-group">
            <input type="password" class="form-control" name="password" placeholder="{{ Lang::get('forms.password-ph') }}">
            <div><small>{{ link_to(url('forgot-password'), Lang::get('user.forgot-pw-link')) }}</small></div>
          </div>
          <div class="form-group">
            <label>
              <input type="checkbox" name="remember"><small> {{ Lang::get('forms.remember-me') }}</small>
            </label>
          </div>
          <button class="btn btn-primary btn-block" type="submit">{{ Lang::get('forms.login') }}</button>
          <div class="filler-text">- {{ Lang::get('navigation.or') }} -</div>
          <a href="{{ url('login/facebook') }}" class="btn btn-block btn-facebook"><i class="fa fa-facebook"></i> | {{ Lang::get('forms.login-fb') }}</a>
          <a href="{{ url('login/google') }}" class="btn btn-block btn-google-plus"><i class="fa fa-google-plus"></i> | {{ Lang::get('forms.login-google') }}</a>
        {{ Form::close() }}
      </div>
    </div>
    @endif
  </header>
  @yield('breadcrumbs')
  @if(Session::has('notice'))
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="alert alert-{{ Session::get('notice.type') }} alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          {{ (Session::has('notice.title') ? '<strong>' . Session::get('notice.title') . '</strong> ' : '') . Session::get('notice.msg') }}
        </div>
      </div>
    </div>
  </div>
  @endif
  @yield('content')
  <div class="container">
    <div class="ga">
      <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
      <!-- megamall-responsive -->
      <ins class="adsbygoogle"
           style="display:block"
           data-ad-client="ca-pub-0317550451740718"
           data-ad-slot="9088660230"
           data-ad-format="auto"></ins>
      <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
      </script>
    </div>
  </div>
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <div>{{ Lang::get('copyright.info') }}</div>
          <div>{{ link_to('http://www.mongutierrez.com', Lang::get('copyright.dev-link'), array('target' => '_blank', 'title' => Lang::get('copyright.dev-link-title'))) }} {{ Lang::get('copyright.dev-name') }}</div>
        </div>
        <div id="nav-bottom" class="col-md-6 col-sm-12">
          <ul class="nav nav-pills">
            @foreach($navbottom as $key => $nav)
              <li>{{ link_to($nav, Lang::get('navigation.' . $key)) }}</li>
            @endforeach
          </ul>
        </div>
      </div>
    </div>
  </footer>
  {? $js[] = '/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js' ?}
  <script type="text/javascript">
    var baseurl = "{{ url() }}";
    @if(Auth::check())
    $(document).ready(function() {
        setInterval(function() {
            refreshMessages();
            refreshFeedback();
        }, {{ Config::get('settings.unread_poll_rate') }});
    });
    @endif
  </script>

  {? $js[] = '/js/vendor/bootstrap.min.js' ?}
  {? $js[] = '/js/vendor/bootstrap-select.min.js' ?}
  {? $js[] = '/js/main.js' ?}

  @yield('scripts')

  {? if(isset($add_js)) $js = array_merge($js, $add_js) ?}
  {{ Minify::javascript($js)->withFullUrl() }}

  @if(Agent::isMobile())
  <script type="text/javascript">
    $(document).ready(function() {
        $('.selectpicker').selectpicker('mobile');
    });
  </script>
  @endif
  </body>
</html>