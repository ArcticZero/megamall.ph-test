<!DOCTYPE html>
<html lang="en-US">
  <head>
		<meta charset="utf-8">
  </head>
  <body style="font-family: Helvetica, Arial, sans-serif;">
  	<a href="{{ url() }}" target="_blank"><img src="{{ $message->embed(url('img/megamall-logo-md.png')) }}" alt="{{ Lang::get('seo.megamall') }}"></a>
  	<div style="margin-top: 20px;">
	  	@yield('content')
		</div>
		<div style="margin-top: 20px; border-top: 1px solid #eee; padding-top: 10px; font-size: 0.9em;">
			<div>
				<a href="{{ url('terms') }}" target="_blank">{{ Lang::get('navigation.terms') }}</a> | <a href="{{ url('privacy') }}" target="_blank">{{ Lang::get('navigation.privacy') }}</a> | <a href="{{ url('contact') }}" target="_blank">{{ Lang::get('navigation.contact-us') }}</a> | <a href="{{ url('help') }}" target="_blank">{{ Lang::get('navigation.help') }}</a>
			</div>
			<div>{{ Lang::get('copyright.info') }}</div>
		</div>
  </body>
</html>