<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>{{ $title or Lang::get('seo.title') }} - {{ Lang::get('seo.megamall') }}</title>
    <meta name="description" content="{{ Lang::get('seo.description') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    {? $css[] = '/css/bootstrap.css' ?}
    {? $css[] = '/css/bootstrap_ms.css' ?}
    {? $css[] = '/css/bootstrap-social.css' ?}
    {? $css[] = '/css/bootstrap-select.min.css' ?}
    {? $css[] = '/css/toastr.min.css' ?}
    {? $css[] = '/css/main.css' ?}
    {? $css[] = '/css/admin.css' ?}

    @yield('styles')

    {? if(isset($add_css)) $css = array_merge($css, $add_css) ?}
    {{ Minify::stylesheet($css, array('type' => 'text/css', 'media' => 'all'))->withFullUrl() }}

    {{ HTML::style('//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css') }}

    {{ HTML::script('//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js') }}
    <script>window.jQuery || document.write('<script src="{{ URL::to('js/vendor/jquery-1.11.0.min.js') }}"><\/script>')</script>
  </head>
  <body>
  <header>
    <nav id="navbar-top" class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-top-content">
                <span class="sr-only">{{ Lang::get('navigation.toggle') }}</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand hidden-sm hidden-md hidden-lg" href="{{ url() }}">{{ HTML::image('img/megamall-logo-md.png', $alt='Megamall.ph') }}</a>
            </div>
            <div id="navbar-top-content" class="navbar-collapse collapse">
              <ul class="nav navbar-nav pull-right">
                @if(Auth::check() && Auth::user()->admin)
                <li{{ Request::path() == Config::get('app.admin') ? $active : '' }}><a href="{{ url(Config::get('app.admin')) }}"><i class="fa fa-dashboard fa-fw"></i>{{ Lang::get('admin.dashboard') }}</a></li>
                <li{{ Request::path() == Config::get('app.admin') . '/users' ? $active : '' }}><a href="{{ url(Config::get('app.admin') . '/users') }}"><i class="fa fa-users fa-fw"></i>{{ Lang::get('admin.users') }}</a></li>
                <li class="dropdown{{ Request::is(Config::get('app.admin') . '/storefronts/*') ? ' active' : '' }}">
                  <a href="{{ url(Config::get('app.admin') . '/storefronts') }}" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-shopping-cart fa-fw"></i>{{ Lang::get('admin.storefronts') }}
                    &nbsp;<span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                    <li><a href="{{ url(Config::get('app.admin') . '/storefronts') }}"><i class="fa fa-list-alt fa-fw"></i>{{ Lang::get('admin.store-list') }}</a></li>
                    <li><a href="{{ url(Config::get('app.admin') . '/storefronts/payment') }}"><i class="fa fa-money fa-fw"></i>{{ Lang::get('admin.payment-gateways') }}</a></li>
                    <li><a href="{{ url(Config::get('app.admin') . '/storefronts/shipping') }}"><i class="fa fa-truck fa-fw"></i>{{ Lang::get('admin.shipping-methods') }}</a></li>
                  </ul>
                </li>
                <li class="dropdown{{ Request::is(Config::get('app.admin') . '/catalog/*') ? ' active' : '' }}">
                  <a href="{{ url(Config::get('app.admin') . '/catalog') }}" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-newspaper-o fa-fw"></i>{{ Lang::get('admin.catalog') }}
                    &nbsp;<span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                    <li><a href="{{ url(Config::get('app.admin') . '/catalog/categories') }}"><i class="fa fa-sitemap fa-fw"></i>{{ Lang::get('admin.categories') }}</a></li>
                    <li><a href="{{ url(Config::get('app.admin') . '/catalog/products') }}"><i class="fa fa-tags fa-fw"></i>{{ Lang::get('admin.products') }}</a></li>
                  </ul>
                </li>
                <li class="dropdown{{ Request::is(Config::get('app.admin') . '/location/*') ? ' active' : '' }}">
                  <a href="{{ url(Config::get('app.admin') . '/location') }}" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-location-arrow fa-fw"></i>{{ Lang::get('admin.location') }}
                    &nbsp;<span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                    <li><a href="{{ url(Config::get('app.admin') . '/location/countries') }}"><i class="fa fa-globe fa-fw"></i>{{ Lang::get('admin.countries') }}</a></li>
                    <li><a href="{{ url(Config::get('app.admin') . '/location/provinces') }}"><i class="fa fa-fw fa-th-large"></i>{{ Lang::get('admin.provinces') }}</a></li>
                    <li><a href="{{ url(Config::get('app.admin') . '/location/cities') }}"><i class="fa fa-fw fa-th"></i>{{ Lang::get('admin.cities') }}</a></li>
                  </ul>
                </li>
                <li class="dropdown{{ Request::is(Config::get('app.admin') . '/management/*') ? ' active' : '' }}">
                  <a href="{{ url(Config::get('app.admin') . '/management') }}" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-wrench fa-fw"></i>{{ Lang::get('admin.site-management') }}
                    &nbsp;<span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                    <li><a href="{{ url(Config::get('app.admin') . '/management/reports') }}"><i class="fa fa-exclamation-triangle fa-fw"></i>{{ Lang::get('admin.reported-items') }}</a></li>
                    <li><a href="{{ url(Config::get('app.admin') . '/management/analytics') }}"><i class="fa fa-line-chart fa-fw"></i>{{ Lang::get('admin.analytics') }}</a></li>
                    <li><a href="{{ url(Config::get('app.admin') . '/management/ads') }}"><i class="fa fa-photo fa-fw"></i>{{ Lang::get('admin.ad-spaces') }}</a></li>
                    <li><a href="{{ url(Config::get('app.admin') . '/management/partners') }}"><i class="fa fa-bank fa-fw"></i>{{ Lang::get('admin.payment-partners') }}</a></li>
                    <li><a href="{{ url(Config::get('app.admin') . '/management/settings') }}"><i class="fa fa-gears fa-fw"></i>{{ Lang::get('admin.settings') }}</a></li>
                  </ul>
                </li>
                <li{{ Request::path() == Config::get('app.admin') . '/logout' ? $active : '' }}><a href="{{ url(Config::get('app.admin') . '/logout') }}"><i class="fa fa-lock fa-fw"></i>{{ Lang::get('navigation.logout') }}</a></li>
                @else
                <li{{ Request::path() == Config::get('app.admin') . '/login' ? $active : '' }}><a href="{{ url(Config::get('app.admin') . '/login') }}"><i class="fa fa-lock fa-fw"></i>{{ Lang::get('navigation.login') }}</a></li>
                @endif
              </ul>
              <div class="nav-welcome hidden-xs">
              @if(Auth::check() && Auth::user()->admin)
                {{ Lang::get('navigation.welcome-user', array('firstname' => '<strong>' . Auth::user()->firstname . '</strong>')) }}
              @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
    <div class="container">
      <div class="row">
        <div class="col-md-12 hidden-xs">
          <a class="logo" href="{{ url(Config::get('app.admin')) }}">{{ HTML::image('img/megamall-logo-md.png', $alt='Megamall.ph') }}</a>
        </div>
      </div>
    </div>
  </header>
  @yield('breadcrumbs')
  @if(Session::has('notice'))
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="alert alert-{{ Session::get('notice.type') }} alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          {{ (Session::has('notice.title') ? '<strong>' . Session::get('notice.title') . '</strong> ' : '') . Session::get('notice.msg') }}
        </div>
      </div>
    </div>
  </div>
  @endif
  @yield('content')
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div>{{ Lang::get('copyright.info') }}</div>
          <div>{{ link_to('http://www.mongutierrez.com', Lang::get('copyright.dev-link'), array('target' => '_blank', 'title' => Lang::get('copyright.dev-link-title'))) }} {{ Lang::get('copyright.dev-name') }}</div>
        </div>
      </div>
    </div>
  </footer>

  @if(Auth::check())
  <!-- Confirmation Dialog -->
  <div class="modal" id="modal-dialog" tabindex="-1" role="dialog" aria-labelledBy="dialog-title" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{{ Lang::get('admin.modal-close') }}</span></button>
          <h4 id="dialog-title" class="modal-title"></h4>
        </div>
        <div class="modal-body" id="dialog-body"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-fw fa-close"></i>{{ Lang::get('admin.modal-no') }}</button>
          <button type="button" class="btn btn-success" data-url="" data-id="" id="dialog-confirm"><i class="fa fa-fw fa-check"></i>{{ Lang::get('admin.modal-yes') }}</button>
        </div>
      </div>
    </div>
  </div>
  @endif

  {? $js[] = '/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js' ?}
  <script type="text/javascript">
    var baseurl = "{{ url() }}";
    @if(Auth::check() && Auth::user()->admin)
    // display status message
    function status(title, body, type, box) {
        if(box) {
            var html = '<' + 'div class="alert alert-dismissible ' + type + '" role="alert">' +
                       '<' + 'button type="button" class="close" data-dismiss="alert"><' + 'span aria-hidden="true">&times;</' + 'span><' + 'span class="sr-only">{{ Lang::get('admin.modal-close') }}</' + 'span></' + 'button>' +
                       (title ? '<strong>' + title + '</strong> ' : '') + body +
                       '</div>';

            $(box).removeClass('hide').html(html);
        } else {
            switch(type) {
                case 'alert-success':
                    toastr.success(body);
                    break;
                case 'alert-danger':
                    toastr.error(body);
                    break;
            }
        }
    }

    $(document).ready(function() {
        // do action after confirmation
        $("#dialog-confirm").click(function() {
            var url = $(this).data('url');
            var id = $(this).data('id');

            $.post(url, { id: id }, function(response) {
                $("#modal-dialog").modal('hide');

                if(response.error) {
                    status("Error", response.error, 'alert-danger');
                } else {
                    status(response.title ? response.title : false, response.body, 'alert-success', response.box ? response.box : false);
                    refresh();
                }
            }, 'json');
        });
    });
    @endif
  </script>

  {? $js[] = '/js/vendor/bootstrap.min.js' ?}
  {? $js[] = '/js/vendor/bootstrap-select.min.js' ?}
  {? $js[] = '/js/vendor/toastr.min.js' ?}
  {? $js[] = '/js/main.js' ?}
  {? $js[] = '/js/admin/admin.js' ?}

  @yield('scripts')

  {? if(isset($add_js)) $js = array_merge($js, $add_js) ?}
  {{ Minify::javascript($js)->withFullUrl() }}

  @if(Agent::isMobile())
  <script type="text/javascript">
    $(document).ready(function() {
        $('.selectpicker').selectpicker('mobile');
    });
  </script>
  @endif
  </body>
</html>