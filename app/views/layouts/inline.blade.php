<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>{{ Lang::get('seo.megamall') }}</title>
    <meta name="description" content="{{ Lang::get('seo.description') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{ HTML::style('css/bootstrap.css') }}
    {{ HTML::style('css/bootstrap_ms.css') }}
    {{ HTML::style('//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css') }}
    {{ HTML::style('css/bootstrap-social.css') }}
    {{ HTML::style('css/main.css') }}

    @yield('styles')

    {{ HTML::script('js/vendor/modernizr-2.6.2-respond-1.1.0.min.js') }}
    
    <script>
  		(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
  		function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
  		e=o.createElement(i);r=o.getElementsByTagName(i)[0];
  		e.src='//www.google-analytics.com/analytics.js';
  		r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
  		ga('create','UA-1806911-54');ga('send','pageview');
  	</script>
  </head>
  <body>
  	@yield('content')
		<script>window.jQuery || document.write('<script src="{{ URL::to('js/vendor/jquery-1.11.0.min.js') }}"><\/script>')</script>
	  <script type="text/javascript">
	    var baseurl = "{{ url() }}";
	  </script>
	  {{ HTML::script('js/vendor/bootstrap.min.js') }}
	  {{ HTML::script('js/vendor/bootstrap-select.min.js') }}
	  {{ HTML::script('js/main.js') }}
	  @yield('scripts')
  </body>
</html>