@extends('layouts.master')

@section('styles')
  {? $add_css[] = '/css/summernote.css' ?}
@stop

@section('scripts')
  @include('includes.validator')
  {? $add_js[] = '/js/vendor/jquery.validate.min.js' ?}
  {? $add_js[] = '/js/vendor/summernote.min.js' ?}
  {? $add_js[] = '/js/post-report.js' ?}
@stop

@section('breadcrumbs')
  @include('includes.breadcrumbs')
@stop

@section('content')
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-danger">
              <div class="panel-heading">{{ Lang::get('help.send-a-report') }}</div>
              <div class="panel-body">
                {{ Form::open(array('url' => 'help/report', 'id' => 'form-report')) }}
                  <div class="row">
                    <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                      <div class="form-group">
                        <label for="item-type">{{ Lang::get('help.item-type') }}</label>
                        <input type="text" name="type_text" id="item-type" class="form-control" value="{{ Lang::get('help.' . $type) }}" readonly>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 col-sm-8 col-md-offset-0 col-sm-offset-2">
                      <div class="form-group">
                        <label for="item-id">{{ Lang::get('help.item-description') }}</label>
                        <input type="text" name="id_text" id="item-id" class="form-control" value="{{ $name }}" readonly>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                      <div class="form-group has-feedback{{ $errors->has('poster_name') ? ' has-errors' : '' }}">
                        <label for="poster-name">{{ Lang::get('user.fullname') }}</label>
                        <input type="text" name="poster_name" id="poster-name" class="form-control" value="{{ Input::old('poster_name', $poster_name) }}" required>
                        @if($errors->has('poster_name'))
                          <div for="poster-name" class="label label-danger">{{ $errors->first('poster_name') }}</div>
                          <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                        @endif
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-8 col-md-offset-0 col-sm-offset-2">
                      <div class="form-group has-feedback{{ $errors->has('poster_email') ? ' has-errors' : '' }}">
                        <label for="poster-email">{{ Lang::get('user.email') }}</label>
                        <input type="email" name="poster_email" id="poster-email" class="form-control" value="{{ Input::old('poster_email', $poster_email) }}" required>
                        @if($errors->has('poster_name'))
                          <div for="poster-email" class="label label-danger">{{ $errors->first('poster_email') }}</div>
                          <span class="glyphicon glyphicon-remove form-control-feedback red"></span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 col-sm-8 col-md-offset-0 col-sm-offset-2">
                      <div class="form-group has-feedback{{ $errors->has('message') ? ' has-errors' : '' }}">
                        <label for="report-message">{{ Lang::get('help.details') }}</label>
                        <textarea name="message" class="form-control" id="report-message" rows="8">{{ Input::old('message') }}</textarea>
                        @if($errors->has('message'))
                          <div for="report-message" class="label label-danger">{{ $errors->first('message') }}</div>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-2 col-sm-8 col-md-offset-0 col-sm-offset-2">
                      <input class="hide" type="text" name="honeypot" value="">
                      <input type="hidden" name="item_type" value="{{ $type }}" required>
                      <input type="hidden" name="item_id" value="{{ $id }}" required>
                      <button type="submit" name="submit" class="btn btn-danger btn-block">{{ Lang::get('forms.submit') }}</button>
                    </div>
                  </div>
                {{ Form::close() }}
              </div>
            </div>
          </div>
        </div>
      </div>
@stop