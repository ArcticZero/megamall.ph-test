@extends('layouts.master')

@section('styles')
  {? $add_css[] = '/css/summernote.css' ?}
@stop

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {
        msg_delete_confirm = "{{ Lang::get('user.delete-confirm') }}";
    });
  </script>
  {? $add_js[] = '/js/vendor/jquery.raty.js' ?}
  {? $add_js[] = '/js/vendor/summernote.min.js' ?}
  {? $add_js[] = '/js/message-list.js' ?}
@stop

@section('breadcrumbs')
  @include('includes.breadcrumbs')
@stop

@section('content')
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h3>{{ Lang::get('user.messages') }}</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            @include('users.sidebar')
          </div>
          <div class="col-md-8">
            <div class="panel panel-default">
              <div class="panel-heading clearfix">
                <div class="option-left">
                  <button class="btn btn-primary btn-xs" id="btn-mark-read" type="button"><i class="fa fa-fw fa-envelope-o"></i>{{ Lang::get('user.mark-as-read') }}</button>
                  <button class="btn btn-danger btn-xs" id="btn-delete" type="button"><i class="fa fa-fw fa-trash-o"></i>{{ Lang::get('forms.delete-selected') }}</button>
                </div>
                <div class="option-right">
                  {{ Form::open(array('url' => Request::path(), 'method' => 'get', 'id' => 'form-message-list')) }}
                  <small>
                    {{ Lang::get('product.show') }}
                    <select name="show">
                      @foreach(explode(',', Config::get('settings.conversations_per_page_options')) as $num)
                      <option value="{{ $num }}"{{ $num == $messages_per_page ? ' selected="selected"' : '' }}>{{ $num }}</option>
                      @endforeach
                    </select>
                    {{ Lang::get('product.items-per-page') }}
                  </small>
                  {{ Form::close() }}
                </div>
              </div>
              <div class="table-responsive message-list">
                <table class="table table-hover table">
                  <thead>
                    <th class="col-md-1"><input type="checkbox" id="toggle-check"></th>
                    <th class="col-md-2">{{ Lang::get('user.with') }}</th>
                    <th class="col-md-7">{{ Lang::get('forms.subject') }}</th>
                    <th class="col-md-2">{{ Lang::get('forms.updated') }}</th>
                  </thead>
                  <tbody>
                  @if($total_conversations > 0)
                    @foreach($conversations as $con)
                    <tr data-con-id="{{ $con->id }}" class="{{ in_array($con->id, $unread_conversations) ? 'unread' : 'active' }}">
                      <td><input type="checkbox" name="messages" value="{{ $con->id }}"></td>
                      <td><a href="{{ url('profile/' . $with[$con->id]->id) }}">{{ $with[$con->id]->username ? $with[$con->id]->username : implode(' ', array($with[$con->id]->firstname, $with[$con->id]->lastname)) }}</a></td>
                      <td>{{ $con->subject ? $con->subject : '<em>( ' . Lang::get('user.no-subject') . ' )</em>' }}</td>
                      <td><small>{{ date(date('Ymd') == date('Ymd', strtotime($con->updated_at)) ? 'g:i A' : 'M j, Y', strtotime($con->updated_at)) }}</small></td>
                    </tr>
                    @endforeach
                  @else
                    <tr>
                      <td colspan="4">
                        <div class="alert alert-danger"><strong>{{ Lang::get('user.no-conversations') }}</strong></div>
                      </td>
                    </tr>
                  @endif
                  </tbody>
                </table>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 text-center">
                {{ $conversations->links() }}
              </div>
            </div>
          </div>
        </div>
      </div>
@stop