                <div data-user="{{ $msg->user_id }}" data-msg-id="{{ $msg->id }}" data-sent-time="{{ strtotime($msg->sent_at) }}" data-read-time="{{ $msg->read_at ? strtotime($msg->read_at) : '' }}" class="message-box{{ $msg->user_id == Auth::user()->id ? ' me' : '' }}">
                  <div class="popover {{ $msg->user_id == Auth::user()->id ? 'left message-me' : 'right message-you' }}">
                    <div class="arrow"></div>
                    <div class="popover-content">
                      {{ $msg->message }}
                    </div>
                  </div>
                </div>