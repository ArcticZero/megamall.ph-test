@extends('layouts.master')

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {
        conversation = {{ isset($info->id) ? $info->id : 0 }};
        message_pages = {{ isset($message_pages) ? $message_pages : 0 }};
        message_ids = [{{ isset($message_ids) ? $message_ids : '' }}];
        msg_delete_confirm = "{{ Lang::get('user.delete-confirm-one') }}";
        chat_poll_rate = {{ Config::get('settings.chat_poll_rate') }};
        @if(isset($info))
        $('input[name=message]').focus();
        @else
        user = '{{ $with->id }}';
        @if(isset($subject))
        $('input[name=message]').focus();
        @else
        $('input[name=subject]').focus();
        @endif
        @endif
    });
  </script>
  {? $add_js[] = '/js/message-view.js' ?}
@stop

@section('breadcrumbs')
  @include('includes.breadcrumbs')
@stop

@section('content')
      <div class="container">
        <div class="row">
          <div id="conversation-title" class="col-md-12">
            <h3>{{ Lang::get('user.conversation-with', array('user' => $with->username ? $with->username : implode(' ', array($with->firstname, $with->lastname)))) }}</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            @include('users.sidebar')
          </div>
          <div class="col-md-8">
            <div class="panel panel-default">
              <div class="panel-heading clearfix">
                <div class="message-subject"{{ !isset($info) ? ' style="width: 100%;"' : '' }}>
                  @if(isset($info))
                  {{ $info->subject ? $info->subject : '( ' . Lang::get('user.no-subject') . ' )' }}
                  @elseif(isset($subject))
                  {{ $subject }}
                  <input type="hidden" name="subject" value="{{ $subject }}">
                  @else
                  <input type="text" name="subject" tabindex="1" class="form-control" placeholder="{{ Lang::get('user.enter-subject') }}">
                  @endif
                </div>
                <div class="pull-right">
                  <button class="btn btn-danger btn-xs{{ !isset($info) ? ' hidden' : '' }}" id="btn-delete" type="button"><i class="fa fa-fw fa-trash-o"></i>{{ Lang::get('forms.delete') }}</button>
                </div>
              </div>
              <div class="panel-body chat-box">
                @if(isset($info))
                @if($total_messages > 0 && $total_messages > count($messages))
                <div class="text-center">
                  <a href="#" class="btn btn-default btn-xs text-center" id="btn-load-more">{{ Lang::get('user.load-old-messages') }}</a>
                  {{ HTML::image('img/loading-med.gif', '', array('id' => 'message-loader', 'class' => 'hidden')) }}
                </div>
                @endif
                @endif
                <div id="message-list" class="clearfix">
                  @if(isset($info))
                  @foreach($messages as $key => $msg)
                  @if(!isset($messages[$key - 1]) || (isset($messages[$key - 1]) && $messages[$key - 1]->user_id != $msg->user_id))
                  <div class="message-user{{ $msg->user_id == Auth::user()->id ? ' me' : '' }}">
                    <strong>{{ link_to('profile/' . $msg->user_id, $msg->user->username ? $msg->user->username : implode(' ', array($msg->user->firstname, $msg->user->lastname))) }}</strong>
                  </div>
                  @endif
                  @include('messages.single-message')
                  @if(!isset($messages[$key + 1]) || (isset($messages[$key + 1]) && $messages[$key + 1]->user_id != $msg->user_id))
                  <div class="message-time{{ $msg->user_id == Auth::user()->id ? ' me' : '' }}">
                    <div><i class="fa fa-clock-o fa-fw" title="{{ Lang::get('user.sent-on') }}"></i>{{ date(date('Ymd') == date('Ymd', strtotime($msg->sent_at)) ? 'g:i A' : 'M j, Y', strtotime($msg->sent_at)) }}</div>
                    @if($msg->read == 1)
                    <div><i class="fa fa-check fa-fw" title="{{ Lang::get('user.read-on') }}"></i>{{ date(date('Ymd') == date('Ymd', strtotime($msg->read_at)) ? 'g:i A' : 'M j, Y', strtotime($msg->read_at)) }}</div>
                    @endif
                  </div>
                  @endif
                  @endforeach
                  @endif
                </div>
              </div>
              <div class="panel-body">
                {{ Form::open(array('url' => 'messages/send', 'id' => 'form-send-message', 'class' => isset($info) ? '' : 'new')) }}
                <div class="message-input">
                  <div class="input-group">
                    <input type="text" tabindex="{{ isset($info) || isset($subject) ? 1 : 2 }}" name="message" class="form-control" placeholder="{{ Lang::get('user.type-message-here') }}">
                    <span class="input-group-btn">
                      <input class="hide" type="text" name="honeypot" value="">
                      <button name="send" tabindex="{{ isset($info) || isset($subject) ? 2 : 3 }}" class="btn btn-danger" type="submit">{{ Lang::get('forms.send') }}</button>
                    </span>
                  </div>
                </div>
                {{ Form::close() }}
              </div>
            </div>
          </div>
        </div>
      </div>
@stop