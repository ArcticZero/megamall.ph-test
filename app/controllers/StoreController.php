<?php
/*
|--------------------------------------------------------------------------
| Store Controller
|--------------------------------------------------------------------------
|
| This controller manages the display of merchants on the mall.
|
*/

class StoreController extends BaseController {
    /**
     * Show all merchants
     *
     */
    public function show_all() {
        // under construction
        return View::make('under-construction', $this->data);

        $this->data['name'] = Lang::get('store.store-directory');
        $this->data['stores'] = Store::where('status', 1)->all();
        $this->data['total_stores'] = count($this->data['stores']);
        
        $this->data['breadcrumbs'][] = array(Lang::get('navigation.stores'), 'stores');

        return View::make('stores.list', $this->data);
    }

    /**
     * Apply to become a merchant
     *
     */
    public function create() {
        // require user, and must not have store yet
        if(Auth::guest()) {
            return Redirect::to('login')
                           ->with('redirect', 'stores/create');
        } else if(Auth::user()->store) {
            return Redirect::to('/');
        }

        // check if user exists
        $user = Auth::user();

        if($user) {
            $this->data['info'] = $user;

            // format addresses
            $this->data['addresses'] = array();
            
            foreach($this->data['info']->addresses as $row) {
                $address_name = implode(' ', array_filter(array($row->firstname, $row->lastname))) . ($row->company ? " ($row->company)" : '');
                $address_fields = isset($row->zone) ? array($row->address_1, $row->address_2, $row->city, $row->zone->name, $row->country->name, $row->postcode) : array($row->address_1, $row->address_2, $row->city, $row->country->name, $row->postcode);
                $this->data['addresses'][$row->id] = ($address_name ? $address_name . ' - ' : '') . implode(', ', array_filter($address_fields));
            }

            $this->data['breadcrumbs'][] = array(Lang::get('navigation.stores'), 'stores');
            $this->data['breadcrumbs'][] = array(Lang::get('store.create-store-head'), '');

            $this->data['title'] = Lang::get('store.create-store-head');

            return View::make('stores.apply', $this->data);
        } else {
            App::abort(404);
        }
    }

    /**
     * Submit store front application
     *
     */
    public function do_create() {
        // require user, and must not have store yet
        if(Auth::guest() || (Auth::check() && Auth::user()->store)) {
            App::abort(404);
        }

        // nullify textarea if blank
        if(Input::get('description') == '<p><br></p>') {
            Input::merge(array('description' => null));
        }

        // validation rules
        $rules = array(
            'store_name' => 'required|min:4|alpha_spaces|unique:stores,store_name',
            'store_url' => 'required|min:4|unique:stores,store_url',
            'store_description' => 'max_clean:' . Config::get('settings.store_description_max_characters'),
            'store_address_id' => 'exists:user_addresses,id',
            'proof' => 'has_files|file_sizes:' . Config::get('settings.max_upload_file_size') . '|max:' . Config::get('settings.max_upload_count'),
            'honeypot' => 'blank'
        );

        $categories = Input::get('categories');

        if($categories) {
            foreach($categories as $index => $cat) {
                $rules['categories.' . $index] = 'integer|exists:categories,id';
            }
        }

        // format URL
        $urlstring = Input::get('store_url');
        $url = 'http://' . $urlstring . '.megamall.ph/';
        Input::merge(array('store_url' => $url));

        // custom messages
        $messages = array(
            'required' => Lang::get('forms.error-required'),
            'unique' => Lang::get('forms.error-attr-taken'),
            'exists' => Lang::get('forms.error-add-not-found'),
            'max' => Lang::get('forms.error-too-many-files', array('num' => Config::get('settings.max_upload_count'))),
            'file_sizes' => Lang::get('validation.file_sizes', array('num' => Config::get('settings.max_upload_file_size'))),
            'max_clean' => Lang::get('validation.max.string', array('max' => Config::get('settings.store_description_max_characters')))
        );

        // do validation
        $input = Input::all();
        $validator = Validator::make($input, $rules, $messages);

        if($validator->passes()) {
            // no errors, create store
            $store = new Store;
            $store->user_id = Auth::user()->id;
            $store->store_name = Input::get('store_name');
            $store->store_url = strtolower(Input::get('store_url'));
            $store->description = Input::get('description');
            $store->permalink = self::storeSlug($store->store_name);
            $store->email = Input::get('store_email');
            $store->telephone = Input::get('store_telephone');
            $store->fax = Input::get('store_fax');
            $store->address_id = Input::get('store_address_id');
            $store->status = 0;
            $store->deployed = 0;
            $store->save();

            // sync categories
            $store->categories()->sync($categories);

            // create user uploads directory if it doesn't exist
            $path = public_path() . '\\uploads\\' . md5(Auth::user()->id);

            if(!File::exists($path)) {
                File::makeDirectory($path);
            }

            // upload all files
            foreach(Input::file('proof') as $file) {
                if($file->isValid()) {
                    $filename = $file->getClientOriginalName();
                    $file->move($path, $filename);
                }
            }

            return Redirect::action('UserController@profile')
                           ->with('notice', array(
                                'title' => Lang::get('forms.success-head'),
                                'msg' => Lang::get('forms.success-create-store'),
                                'type' => 'success'
                            ));
        } else {
            // revert URL
            Input::merge(array('store_url' => $urlstring));

            // stuff happens
            return Redirect::action('StoreController@create')
                           ->withInput(Input::except('proof'))
                           ->withErrors($validator);
        }
    }

    /**
     * View store portal
     *
     */
    public function view($slug) {
        // filter inputs
        $sortby = preg_replace( "/[^a-z]/", '', Input::get('sort'));
        $show = preg_replace("/[^0-9]/", '', Input::get('show'));

        // check if store exists
        $store = Store::where('permalink', $slug)
                      ->first();

        if($store) {
            // determine sorting
            switch($sortby) {
                case 'rating':
                    $sort = 'products.avg_rating';
                    $order = 'desc';
                    break;
                case 'lowprice':
                    $sort = 'products.price';
                    $order = 'asc';
                    break;
                case 'highprice':
                    $sort = 'products.price';
                    $order = 'desc';
                    break;
                case 'date':
                default:
                    $sort = 'products.created_at';
                    $order = 'desc';
            }

            // filter per page limit
            $show = in_array($show, explode(",", Config::get('settings.products_per_page_options'))) ? $show : Config::get('settings.products_per_page');

            $this->data['info'] = $store;
            $this->data['products'] = Product::getByStore($store->id, $sort, $order, $show);
            $this->data['total_products'] = Product::where('store_id', $store->id)->count();

            $this->data['name'] = $store->store_name;
            $this->data['sidebar'] = 'stores.sidebar';
            $this->data['sidebar_data'] = array('store' => $store);

            $this->data['breadcrumbs'][] = array(Lang::get('navigation.stores'), 'stores');
            $this->data['breadcrumbs'][] = array($store->store_name, '');

            $this->data['title'] = $store->store_name;
            
            return View::make('products.list', $this->data);
        } else {
            // not found
             App::abort(404);
        }
    }
}
