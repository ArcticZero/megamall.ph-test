<?php
/*
|--------------------------------------------------------------------------
| Form Validation Controller
|--------------------------------------------------------------------------
|
| This controller handles AJAX requests for form validation.
|
*/

class ValidationController extends BaseController {

	public function __construct() {
		// only allow AJAX requests
		if(!Request::ajax()) {
			App::abort(401);
		}
	}

    /**
     * Check if email is already in use
     *
     */
    public function postEmailTaken() {
        $email = Input::get('email');
        $edit = Input::get('edit');

        // check db
        $query = User::where('email', $email);

        if($edit && Auth::check()) {
        	$query->where('id', '!=', Auth::user()->id);
        }

        $exists = $query->first();

        // return
        echo $exists ? Lang::get('forms.error-email-taken') : "true";
    }

    /**
     * Check if email exists
     *
     */
    public function postEmailExists() {
        $email = Input::get('email');

        // check db
        $exists = User::where('email', $email)->first();

        // return
        echo !$exists ? Lang::get('forms.error-email-not-found') : "true";
    }

    /**
     * Check if username is already in use
     *
     */
    public function postUsernameTaken() {
        $username = Input::get('username');
        $edit = Input::get('edit');

        // check db
        $query = User::where('username', $username);

        if($edit && Auth::check()) {
            $query->where('id', '!=', Auth::user()->id);
        }

        $exists = $query->first();

        // return
        echo $exists ? Lang::get('forms.error-username-taken') : "true";
    }

    /**
     * Check if address exists
     *
     */
    public function postAddressExists() {
        $address_id = Input::get('store_address') ? Input::get('store_address') : Input::get('address_id');

        // check db
        $exists = Address::where('id', $address_id)->first();

        // return
        echo !$exists ? Lang::get('forms.error-add-not-found') : "true";
    }

    /**
     * Check if password is correct
     *
     */
    public function postCurrentPassword() {
        $password = Input::get('current_password');

        // check db
        $oldpass = User::where('id', Auth::user()->id)
                       ->pluck('password');

        // return
        echo Hash::check($password, $oldpass) ? "true" : Lang::get('forms.error-oldpw-wrong');
    }

    /**
     * Check if store name is taken
     *
     */
    public function postStoreNameTaken() {
        $name = Input::get('store_name');
        $edit = Input::get('edit');

        // check db
        $query = Store::where('store_name', $name);

        if($edit && Auth::check() && Auth::user()->store) {
            $query->where('id', '!=', Auth::user()->store->id);
        }

        $exists = $query->first();

        // return
        echo $exists ? Lang::get('forms.error-store-name-taken') : "true";
    }

    /**
     * Check if store URL is taken
     *
     */
    public function postStoreUrlTaken() {
        $url = 'http://' . Input::get('store_url') . '.megamall.ph/';
        $edit = Input::get('edit');

        // check db
        $query = Store::where('store_url', $url);

        if($edit && Auth::check() && Auth::user()->store) {
            $query->where('id', '!=', Auth::user()->store->id);
        }

        $exists = $query->first();

        // return
        echo $exists ? Lang::get('forms.error-store-url-taken') : "true";
    }
}
