<?php
/*
|--------------------------------------------------------------------------
| Home Controller
|--------------------------------------------------------------------------
|
| This controller generates and displays the front page.
|
*/

class HomeController extends BaseController {
    /**
     * Home page
     *
     */
    public function home() {
    	// banners
        $this->data['home_banner'] = Banner::find(1);
        $this->data['home_box_1'] = Banner::find(2);
        $this->data['home_box_2'] = Banner::find(3);

    	// featured products
        $now = date("Y-m-d H:i:s");
        $this->data['featured'] = Product::getFeatured(Config::get('settings.home_featured_products_count'));

    	// recent products
        $this->data['recent'] = Product::grab(Config::get('settings.home_recent_products_count'));

        // recently viewed
        if(Auth::check()) {
            $this->data['viewed'] = Product::getLastViewed(Auth::user()->id, 'user', Config::get('settings.home_viewed_products_count'));
        } else {
            $this->data['viewed'] = Product::getLastViewed(Request::getClientIp(), 'ip', Config::get('settings.home_viewed_products_count'));
        }

        return View::make('home', $this->data);
    }
}
