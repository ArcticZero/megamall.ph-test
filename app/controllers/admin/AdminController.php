<?php
/*
|--------------------------------------------------------------------------
| Administration Panel Controller
|--------------------------------------------------------------------------
|
| This controller generates and displays the admin dashboard, and
| handles common AJAX data requests.
|
*/

class AdminController extends BaseController {
    /**
     * Dashboard
     *
     */
    public function dashboard() {
    	$this->data['title'] = Lang::get('admin.dashboard');
    	
    	return View::make('admin.dashboard', $this->data);
    }

    /**
     * Get Zones
     *
     */
    public function get_zones() {
    	$country = Input::get('country');

    	if($country) {
    		$zones = Zone::where('country_id', $country)->orderBy('name', 'asc')->get();

    		$result['zones'] = $zones->toArray();
            return Response::json($result);
    	} else {
    		return Response::json(['error' => Lang::get('admin.invalid-row')]);
    	}
    }
}
