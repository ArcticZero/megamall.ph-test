<?php
/*
|--------------------------------------------------------------------------
| Store Controller (Admin)
|--------------------------------------------------------------------------
|
| Handles storefront management.
|
*/

use Symfony\Component\Console\Output\BufferedOutput;

class AdminStoreController extends BaseController {
    /**
     * Display the user list
     *
     */
    public function index() {
        // get row set
        $result = $this->do_list();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];

        // get countries
        $this->data['countries'] = Country::orderBy('name')->get();

        // render
        $this->data['breadcrumbs'][] = array(Lang::get('admin.storefronts'), '');
        $this->data['title'] = Lang::get('admin.storefronts');
        return View::make('admin.storefronts.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function do_list() {
        // sort and order
        $result['sort'] = Input::get('s') ?: 'store_name';
        $result['order'] = Input::get('o') ?: 'asc';

        // build query
        $rows = Store::select('stores.*', 'users.username', DB::raw('COUNT(product_id) AS total_products'))
                     ->orderBy('deployed', 'asc')
                     ->orderBy($result['sort'], $result['order'])
                     ->join('users', 'users.id', '=', 'stores.user_id')
                     ->leftJoin('products', 'products.store_id', '=', 'stores.id')
                     ->groupBy('stores.id')
                     ->paginate(Config::get('settings.admin_rows_per_page'));

        // generate page links
        $result['pages'] = str_replace(url(Config::get('app.admin') . '/storefronts/refresh'), url(Config::get('app.admin') . '/storefronts'), $rows->appends(array('s' => $result['sort'], 'o' => $result['order']))->links());
        
        // return response (format accordingly)
        if(Request::ajax()) {
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['rows'] = $rows;
            return $result;
        }
    }

    /**
     * Retrieve user information
     *
     */
    public function info() {
        $id = Input::get('id');

        $row = Store::with('user')
                    ->with('user.addresses')
                    ->find($id);

        if($row) {
            // format addresses
            $addresses = array();
            
            foreach($row->user->addresses as $address) {
                $address_name = implode(' ', array_filter(array($address->firstname, $address->lastname))) . ($address->company ? " ($address->company)" : '');
                $address_fields = isset($address->zone) ? array($address->address_1, $address->address_2, $address->city, $address->zone->name, $address->country->name, $address->postcode) : array($address->address_1, $address->address_2, $address->city, $address->country->name, $address->postcode);
                $addresses[] = array('id' => $address->id, 'address' => ($address_name ? $address_name . ' - ' : '') . implode(', ', array_filter($address_fields)));
            }

            return Response::json(['row' => $row, 'addresses' => $addresses]);
        } else {
            return Response::json(['error' => Lang::get('admin.invalid-row')]);
        }
    }

    /**
     * Save model
     *
     */
    public function save() {   
        // check if an ID is passed
        if(Input::get('id')) {
            $row = Store::find(Input::get('id'));

            if(!$row) {
                return Response::json(['error' => Lang::get('admin.not-found')]);
            }
        }

        // validation rules
        $rules = array(
            'store_name' => 'required|min:4|alpha_spaces|unique:stores,store_name,' . $row->id,
            'permalink' => 'required|slug|unique:stores,permalink,' . $row->id,
            'description' => 'max_clean:' . Config::get('settings.store_description_max_characters'),
            'address_id' => 'exists:user_addresses,id',
            'email' => 'email'
        );

        $names = array(
            'store_url' => Lang::get('store.store-url'),
            'custom_url' => Lang::get('store.store-url'),
            'address_id' => Lang::get('store.physical-address')
        );

        if(Input::get('use_custom_url')) {
            $rules['custom_url'] = 'required|url|unique:stores,store_url,' . $row->id;
        } else {
            $rules['store_url'] = 'required|min:4|alpha_dash|unique:stores,store_url,' . $row->id;
        }

        // do validation
        $validator = Validator::make(Input::all(), $rules);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        // make sure everything saves
        DB::beginTransaction();

        // set field values
        $row->store_name = Input::get('store_name');
        $row->store_url = Input::get('use_custom_url') ? Input::get('custom_url') : Input::get('store_url');
        $row->custom_url = Input::get('use_custom_url') ? 1 : 0;
        $row->description = Input::get('description');
        $row->permalink = Input::get('permalink');
        $row->email = Input::get('email');
        $row->telephone = Input::get('telephone');
        $row->fax = Input::get('fax');
        $row->address_id = Input::get('address_id');

        // save model
        $row->save();

        // finish
        DB::commit();

        // return
        return Response::json(['body' => Lang::get('admin.changes-saved')]);
    }

    /**
     * Activates the store
     *
     */
    public function activate() {
        $row = Store::where('status', '!=', 1)->where('id', Input::get('id'))->first();

        // check if store exists
        if(!is_null($row)) {
            $row->status = 1;
            $row->save();

            // return
            return Response::json(['body' => Lang::get('admin.store-activated')]);
        } else {
            // not found
            return Response::json(['error' => Lang::get('admin.not-found')]);
        }
    }

    /**
     * Deactivates the store
     *
     */
    public function deactivate() {
        $row = Store::where('status', 1)->where('id', Input::get('id'))->first();

        // check if store exists
        if(!is_null($row)) {
            $row->status = 0;
            $row->save();

            // return
            return Response::json(['body' => Lang::get('admin.store-deactivated')]);
        } else {
            // not found
            return Response::json(['error' => Lang::get('admin.not-found')]);
        }
    }

    /**
     * Synchronize products
     *
     */
    public function sync() {
        $store = Input::get('id');

        if($store) {
            // get store row if given
            $row = Store::where('status', 1)->where('deployed', 1)->where('id', Input::get('id'))->first();
        }

        if(($store && !is_null($row)) || !$store) {
            // prepare to capture output
            $output = new BufferedOutput;

            if(!$store) {
                // sync all
                try {
                    Artisan::call('sync:products', [], $output);
                } catch(Exception $e) {
                    return Response::json(['error' => $e->getMessage()]);
                }
            } else {
                // sync specific store
                try {
                    Artisan::call('sync:products', ['--store' => [$store]], $output);
                } catch(Exception $e) {
                    return Response::json(['error' => $e->getMessage()]);
                }
            }

            // get latest sync time
            $last_sync = Store::where('id', Input::get('id'))->pluck('last_sync');
            
            // return
            return Response::json(['body' => Lang::get('admin.store-sync-done', ['store' => '<strong>' . $row->store_name . '</strong>']), 'output' => nl2br($output->fetch()), 'last_sync' => $last_sync]);
        } else {
            // not found
            return Response::json(['error' => Lang::get('admin.not-found')]);
        }
    }

    /**
     * Deploy store
     *
     */
    public function deploy() {
        $row = Store::where('status', 1)->where('id', Input::get('id'))->first();

        // check if store exists
        if(!is_null($row)) {
            // DO DEPLOYMENT FUNCTION


            // return
            return Response::json(['body' => Lang::get('admin.store-deployed')]);
        } else {
            // not found
            return Response::json(['error' => Lang::get('admin.not-found')]);
        }
    }

    /**
     * Display the payment gateway list
     *
     */
    public function payment() {
        // get row set
        $result = $this->do_payment_list();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];

        // render
        $this->data['breadcrumbs'][] = array(Lang::get('admin.storefronts'), 'storefronts');
        $this->data['breadcrumbs'][] = array(Lang::get('admin.payment-gateways'), '');
        $this->data['title'] = Lang::get('admin.payment-gateways');
        return View::make('admin.storefronts.payments.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function do_payment_list() {
        // sort and order
        $result['sort'] = Input::get('s') ?: 'store_name';
        $result['order'] = Input::get('o') ?: 'asc';

        // build query
        $rows = Extension::where('type', 'payment')
                         ->paginate(Config::get('settings.admin_rows_per_page'));

        // generate page links
        $result['pages'] = str_replace(url(Config::get('app.admin') . '/storefronts/payment/refresh'), url(Config::get('app.admin') . '/storefronts'), $rows->appends(array('s' => $result['sort'], 'o' => $result['order']))->links());
        
        // return response (format accordingly)
        if(Request::ajax()) {
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['rows'] = $rows;
            return $result;
        }
    }
}
