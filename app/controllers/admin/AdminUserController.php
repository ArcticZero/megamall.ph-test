<?php
/*
|--------------------------------------------------------------------------
| User Controller (Admin)
|--------------------------------------------------------------------------
|
| Handles user management.
|
*/

class AdminUserController extends BaseController {
    /**
     * Display the user list
     *
     */
    public function index() {
        // get row set
        $result = $this->do_list();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];

        // get countries
        $this->data['countries'] = Country::orderBy('name')->get();

        // render
        $this->data['breadcrumbs'][] = array(Lang::get('admin.users'), '');
        $this->data['title'] = Lang::get('admin.users');
        return View::make('admin.users.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function do_list() {
        // sort and order
        $result['sort'] = Input::get('s') ?: 'lastname';
        $result['order'] = Input::get('o') ?: 'asc';

        // build query
        $rows = User::orderBy('admin', 'desc')
                    ->orderBy($result['sort'], $result['order'])
                    ->paginate(Config::get('settings.admin_rows_per_page'));

        // generate page links
        $result['pages'] = str_replace(url(Config::get('app.admin') . '/users/refresh'), url(Config::get('app.admin') . '/users'), $rows->appends(array('s' => $result['sort'], 'o' => $result['order']))->links());
        
        // return response (format accordingly)
        if(Request::ajax()) {
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['rows'] = $rows;
            return $result;
        }
    }

    /**
     * Retrieve user information
     *
     */
    public function info() {
        $id = Input::get('id');

        $row = User::with('addresses')->find($id);

        if($row) {
            return Response::json($row);
        } else {
            return Response::json(['error' => Lang::get('admin.invalid-row')]);
        }
    }

    /**
     * Save model
     *
     */
    public function save() {   
        // check if an ID is passed
        if(Input::get('id')) {
            $row = User::find(Input::get('id'));

            if(!$row) {
                return Response::json(['error' => Lang::get('admin.not-found')]);
            }
        }

        // validation rules
        $rules = array(
            'firstname' => 'required|alpha_spaces',
            'lastname' => 'required|alpha_spaces',
            'username' => 'alpha_num|min:4|unique:users,username,' . $row->id,
            'email' => 'required|email|unique:users,email,' . $row->id,
            'fax' => 'integer',
            'ym_id' => 'alpha_dash',
            'skype_id' => 'alpha_dash',
            'password' => 'min:8|confirmed'
        );

        $names = array();
        $address_ids = Input::get('address_id');
        $addresses = array();
        
        // validate child rows
        if($address_ids) {
            foreach($address_ids as $key => $id) {
                if($id) {
                    // check if user owns this address
                    $address = $row->addresses()->where('user_addresses.id', $id)->first();

                    $subrules = array(
                        "address_id.$id" => 'required|exists:user_addresses,id',
                        "address_firstname.$id" => 'required|alpha_spaces',
                        "address_lastname.$id" => 'required|alpha_spaces',
                        "address_country_id.$id" => 'required|exists:countries,country_id',
                        "address_zone_id.$id" => 'exists:zones,id'
                    );

                    $fn = array(
                        "address_firstname.$id" => Lang::get('user.firstname'),
                        "address_lastname.$id" =>  Lang::get('user.lastname'),
                        "address_country_id.$id" =>  Lang::get('user.country'),
                        "address_zone_id.$id" =>  Lang::get('user.zone'),
                    );

                    $rules = array_merge($rules, $subrules);
                    $names = array_merge($names, $fn);
                    $addresses[] = $address;
                }    
            }
        }

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        // make sure everything saves
        DB::beginTransaction();

        // set field values
        $row->firstname = Input::get('firstname');
        $row->lastname = Input::get('lastname');
        $row->username = Input::get('username');
        $row->telephone = Input::get('telephone');
        $row->fax = Input::get('fax');
        $row->ym_id = Input::get('ym_id');
        $row->skype_id = Input::get('skype_id');
        $row->email = Input::get('email');

        // do not change password if old user and field is empty
        if(Input::get('password')) {
            $row->password = Hash::make(Input::get('password'));
        }

        // clear default address if deleted
        if(!in_array($row->address_id, $address_ids)) {
            $row->address_id = null;
        }

        // delete addresses not included
        if(!empty($address_ids)) {
            $row->addresses()->whereNotIn('id', $address_ids)->delete();
        }

        // save addresses
        if(!empty($addresses)) {
            foreach($addresses as $address) {
                $address->firstname = Input::get("address_firstname.$address->id");
                $address->lastname = Input::get("address_lastname.$address->id");
                $address->company = Input::get("address_company.$address->id");
                $address->address_1 = Input::get("address_address_1.$address->id");
                $address->address_2 = Input::get("address_address_2.$address->id");
                $address->city = Input::get("address_city.$address->id");
                $address->country_id = Input::get("address_country_id.$address->id");
                $address->zone_id = Input::get("address_zone_id.$address->id");
                $address->postcode = Input::get("address_postcode.$address->id");
                $address->save();
            }
        }

        // save model
        $row->save();

        // finish
        DB::commit();

        // return
        return Response::json(['body' => Lang::get('admin.changes-saved')]);
    }

    /**
     * Displays the login form
     *
     */
    public function login() {
        if(Auth::guest() || (Auth::check() && Auth::user()->admin == 0)) {
            $this->data['breadcrumbs'][] = array(Lang::get('forms.login'), '');
            $this->data['title'] = Lang::get('admin.admin-panel');

            return View::make('admin.users.login', $this->data);
        } else {
            return Redirect::to('admin');
        }
    }

    /**
     * Attempt to do login
     *
     */
    public function do_login() {
        $input = array(
            'auth' => Input::get('auth'),
            'password' => Input::get('password'),
            'honeypot' => Input::get('honeypot')
        );

        // determine if username or email was passed
        $field = filter_var(Input::get('auth'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        if($input['honeypot']) {
            // robot detected
            $error = Lang::get('validation.blank');
        } else if(Auth::attempt(array($field => $input['auth'], 'password' => $input['password'], 'status' => 1, 'confirmed' => 1, 'admin' => 1), false)) {
            // get user
            $user = Auth::user();

            // get address info
            $address = Address::find($user->address_id);
            
            // store session
            $_SESSION['customer_id'] = $user->id;

            if($address) {
                $_SESSION['shipping_country_id'] = $address->country_id;
                $_SESSION['shipping_zone_id'] = $address->zone_id;
                $_SESSION['shipping_postcode'] = $address->postcode;
                $_SESSION['payment_country_id'] = $address->country_id;
                $_SESSION['payment_zone_id'] = $address->zone_id;
            }

            // save user IP
            $user->ip = Request::getClientIp();
            $user->save();

            return Redirect::intended();
        } else if(Auth::validate(array($field => $input['auth'], 'password' => $input['password']))) {
            $user = User::where($field, $input['auth'])->first();

            if($user->status == 0) {
                // user is not active
                $error = Lang::get('forms.error-login-status');
            } else if($user->status == 2) {
                // user is banned
                $error = Lang::get('forms.error-login-banned');
            } else if($user->confirmed == 0) {
                // user has not yet been confirmed
                $error = Lang::get('forms.error-login-confirm');
            }
        } else {
            // invalid login
            $error = Lang::get('forms.error-login-user');
        }

        // return error
        return Redirect::action('AdminUserController@login')
                       ->withInput(Input::except('password'))
                       ->with('notice', array(
                            'title' => Lang::get('forms.error-head'),
                            'msg' => $error,
                            'type' => 'danger'
                       ));
    }

    /**
     * Activates the user
     *
     */
    public function activate() {
        $row = User::where('status', '!=', 1)->where('id', Input::get('id'))->first();

        // check if user exists
        if(!is_null($row)) {
            $row->status = 1;
            $row->save();

            // return
            return Response::json(['body' => Lang::get('admin.user-activated')]);
        } else {
            // not found
            return Response::json(['error' => Lang::get('admin.not-found')]);
        }
    }

    /**
     * Deactivates the user
     *
     */
    public function deactivate() {
        $row = User::where('status', 1)->where('id', Input::get('id'))->first();

        // check if user exists
        if(!is_null($row)) {
            $row->status = 0;
            $row->save();

            // return
            return Response::json(['body' => Lang::get('admin.user-deactivated')]);
        } else {
            // not found
            return Response::json(['error' => Lang::get('admin.not-found')]);
        }
    }

    /**
     * Ban the user
     *
     */
    public function ban() {
        $row = User::where('status', '!=', 2)->where('id', Input::get('id'))->first();

        // check if user exists
        if(!is_null($row)) {
            $row->status = 2;
            $row->save();

            // return
            return Response::json(['body' => Lang::get('admin.user-banned')]);
        } else {
            // not found
            return Response::json(['error' => Lang::get('admin.not-found')]);
        }
    }

    /**
     * Get zones for country
     *
     */
    public function get_zones() {
        // require user and ajax
        if(!Request::ajax()) {
            App::abort(404);
        }

        // check if country exists
        $country = Country::find(Input::get('country_id'));
        
        if($country && $country->zones) {
            $zones = $country->zones()
                             ->orderBy('code', 'asc')
                             ->get()
                             ->toArray();

            return Response::json(['success' => true, 'zones' => $zones]);
        } else {
            // not found
            return Response::json(['success' => false]);
        }
    }

    /**
     * Log the user out of the application.
     *
     */
    public function logout() {
        if(Auth::check()) {
            $user = Auth::user();
            $user->ip = NULL;
            $user->token = NULL;
            $user->save();

            // unset store sessions
            session_destroy();

            // reflash message if exists
            Session::keep(array('notice'));
            
            // logout
            Auth::logout();
        }

        return Redirect::to(Config::get('app.admin'));
    }
}
