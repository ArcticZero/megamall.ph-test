<?php
/*
|--------------------------------------------------------------------------
| Messaging Controller
|--------------------------------------------------------------------------
|
| Handles private messages among users.
|
*/

class MessagingController extends BaseController {
    public function __construct() {
        parent::__construct();

        // require user
        $this->beforeFilter(function() {
            if(Auth::guest()) {
                return Redirect::to('login')
                               ->with('redirect', 'messages');
            }
        });
    }

    public function conversations() {
        // filter input
        $show = preg_replace("/[^0-9]/", '', Input::get('show'));

        // get conversations
        $this->data['conversations'] = Auth::user()->conversations()
                                           ->where('conversation_users.deleted', 0)
                                           ->orderBy('conversations.updated_at', 'desc')
                                           ->paginate(Config::get('settings.conversations_per_page'));

        // mark unread conversations
        $this->data['unread_conversations'] = array();
        foreach($this->data['conversations'] as $con) {
            $with_user = DB::table('conversation_users')
                           ->where('user_id', '!=', Auth::user()->id)
                           ->where('conversation_id', $con->id)
                           ->pluck('user_id');

            $this->data['with'][$con->id] = User::find($with_user);

            foreach($con->messages as $msg) {
                if($msg->user_id != Auth::user()->id) {
                    if($msg->read == 0) {
                        $this->data['unread_conversations'][] = $con->id;
                        break;
                    }
                }
            }
        }

        // total conversations
        $this->data['total_conversations'] = Auth::user()->conversations()
                                                         ->where('conversation_users.deleted', 0)
                                                         ->count();

        $this->data['breadcrumbs'][] = array(Lang::get('user.messages'), '');

        $this->data['title'] = Lang::get('user.messages');

        return View::make('messages.list', $this->data);
    }

    /**
     * View message
     *
     */
    public function view($id) {
        // check if you are in this conversation
        $conversation = Auth::user()->conversations()
                                    ->where('conversations.id', $id)
                                    ->where('conversation_users.deleted', 0)
                                    ->first();

        if($conversation) {
            $this->data['info'] = $conversation;

            // read all messages
            self::read_messages($conversation->id);

            // get other user
            $with_user = DB::table('conversation_users')
                           ->where('user_id', '!=', Auth::user()->id)
                           ->where('conversation_id', $conversation->id)
                           ->pluck('user_id');

            $this->data['with'] = User::find($with_user);

            $this->data['messages'] = $conversation->messages()
                                                   ->orderBy('sent_at', 'desc')
                                                   ->take(Config::get('settings.messages_per_load'))
                                                   ->get()
                                                   ->reverse();

            $message_ids = array();

            foreach($this->data['messages'] as $msg) {
                $message_ids[] = $msg->id;
            }
            
            $this->data['message_ids'] = implode(',', $message_ids);

            $this->data['total_messages'] = $conversation->messages()->count();
            $this->data['message_pages'] = ceil($this->data['total_messages'] / Config::get('settings.messages_per_load'));
            $this->data['current_user'] = '';

            $this->data['breadcrumbs'][] = array(Lang::get('user.messages'), 'messages');
            $this->data['breadcrumbs'][] = array(Lang::get('user.view-conversation'), '');

            $this->data['title'] = $conversation->subject;

            return View::make('messages.view', $this->data);
        } else {
            // not found
            App::abort(404);
        }
    }

    /**
     * Start new conversation
     *
     */
    public function new_conversation($user_id) {
        // check if user exists
        $user = User::where('status', 1)
                    ->find($user_id);

        if($user) {
            $this->data['with'] = $user;
            $this->data['current_user'] = Auth::user()->id;
            $this->data['breadcrumbs'][] = array(Lang::get('user.messages'), 'messages');
            $this->data['breadcrumbs'][] = array(Lang::get('user.compose-message'), '');
            $this->data['title'] = Lang::get('user.compose-message');
            return View::make('messages.view', $this->data);
        } else {
            // not found
            App::abort(404);
        }
    }

    /**
     * Inquire about product
     *
     */
    public function product_inquiry($slug) {
        // check if user exists
        $product = Product::getBySlug($slug);

        $user = $product->store->user;

        if($user) {
            $this->data['with'] = $user;
            $this->data['current_user'] = Auth::user()->id;
            $this->data['subject'] = Lang::get('product.inquiry-on-product', array('product' => $product->name));
            $this->data['breadcrumbs'][] = array(Lang::get('user.messages'), 'messages');
            $this->data['breadcrumbs'][] = array(Lang::get('product.inquiry-on-product', array('product' => $product->name)), '');
            $this->data['title'] = Lang::get('product.inquiry-on-product', array('product' => $product->name));
            return View::make('messages.view', $this->data);
        } else {
            // not found
            App::abort(404);
        }
    }

    /**
     * Save new conversation
     *
     */
    public function do_new_conversation() {
        // validation rules
        $rules = array(
            'user' => 'required|exists:users,id,status,1,confirmed,1|not_in:' . Auth::user()->id,
            'subject' => 'alpha_spaces',
            'message' => 'required',
            'honeypot' => 'blank'
        );

        // do validation
        $input = Input::all();
        $validator = Validator::make($input, $rules);

        if($validator->passes()) {
            // create new conversation
            $conversation = new Conversation;
            $conversation->subject = Input::get('subject');
            $conversation->save();

            // add pivot rows
            $conversation->users()->attach(Auth::user()->id);
            $conversation->users()->attach(Input::get('user'));

            if($conversation) {
                // post message
                $message = new Message;
                $message->user_id = Auth::user()->id;
                $message->conversation_id = $conversation->id;
                $message->sent_at = date("Y-m-d H:i:s");
                $message->message = Input::get('message');
                $message->save();

                $html = '';

                // generate HTML
                $html .= '<div class="message-user me">' .
                         '<strong>' . link_to('profile/' . $message->user_id, $message->user->username ? $message->user->username : implode(' ', array($message->user->firstname, $message->user->lastname))) . '</strong>' .
                         '</div>';

                // the message
                $this->data['msg'] = $message;
                $view = View::make('messages.single-message', $this->data);
                $html .= (string) $view;

                // display time
                $html .= '<div class="message-time me">' .
                         '<div><i class="fa fa-clock-o fa-fw" title="' . Lang::get('user.sent-on') . '"></i>' . date(date('Ymd') == date('Ymd', strtotime($message->sent_at)) ? 'g:i A' : 'M j, Y', strtotime($message->sent_at)) . '</div>' .
                         '</div>';

                // undelete this conversation
                DB::table('conversation_users')
                  ->where('conversation_id', $conversation->id)
                  ->update(array('deleted' => 0));

                // return ok
                return Response::json(['success' => true, 'html' => $html, 'conversation' => $conversation->id, 'sent_at' => strtotime($message->sent_at), 'subject' => $conversation->subject ? $conversation->subject : '( ' . Lang::get('user.no-subject') . ' )', 'message_id' => $message->id]);
            }
        }

        // something went wrong
        return Response::json(['success' => false]);
    }

    public function read_messages($ids) {
        if($ids) {
            if(!is_array($ids)) $ids = explode(',', $ids);

            $total_unread = 0;

            foreach($ids as $id) {
                // check if you are in this conversation
                $conversation = Auth::user()->conversations()->where('conversations.id', $id)->first();

                if($conversation) {
                    // mark messages as read
                    $messages = $conversation->messages()
                                             ->where('read', 0)
                                             ->where('user_id', '!=', Auth::user()->id)
                                             ->get();

                    foreach($messages as $message) {
                        $message->read = 1;
                        $message->read_at = date("Y-m-d H:i:s");
                        $message->save();

                        $total_unread++;
                    }
                }
            }

            // remove from unread count
            $this->data['unread']['pm'] = $this->data['unread']['pm'] - $total_unread;

            // return
            if(Request::ajax()) {
                return Response::json(['success' => true, 'total' => $total_unread]);
            } else {
                return $total_unread;
            }
        }

        // something went wrong
        return Response::json(['success' => false]);
    }

    /**
     * Post a message
     *
     */
    public function send_message() {
        // validation rules
        $rules = array(
            'conversation' => 'required|exists:conversations,id',
            'message' => 'required',
            'latest_user' => 'required|integer',
            'honeypot' => 'blank'
        );

        // do validation
        $input = Input::all();
        $validator = Validator::make($input, $rules);

        if($validator->passes()) {
            // check if you own this message
            $conversation = Auth::user()->conversations()->where('conversations.id', Input::get('conversation'))->first();

            if($conversation) {
                // update timestamp
                $conversation->touch();

                // post message
                $message = new Message;
                $message->user_id = Auth::user()->id;
                $message->conversation_id = $conversation->id;
                $message->sent_at = date("Y-m-d H:i:s");
                $message->message = Input::get('message');
                $message->save();

                $html = '';

                // generate HTML
                if(Input::get('latest_user') != $message->user_id) {
                    $html .= '<div class="message-user' . ($message->user_id == Auth::user()->id ? ' me' : '') . '">' .
                             '<strong>' . link_to('profile/' . $message->user_id, $message->user->username ? $message->user->username : implode(' ', array($message->user->firstname, $message->user->lastname))) . '</strong>' .
                             '</div>';
                }

                // the message
                $this->data['msg'] = $message;
                $view = View::make('messages.single-message', $this->data);
                $html .= (string) $view;

                // display time
                $html .= '<div class="message-time' . ($message->user_id == Auth::user()->id ? ' me' : '') . '">' .
                         '<div><i class="fa fa-clock-o fa-fw" title="' . Lang::get('user.sent-on') . '"></i>' . date(date('Ymd') == date('Ymd', strtotime($message->sent_at)) ? 'g:i A' : 'M j, Y', strtotime($message->sent_at)) . '</div>' .
                         '</div>';

                // undelete this conversation
                DB::table('conversation_users')
                  ->where('conversation_id', $conversation->id)
                  ->update(array('deleted' => 0));

                // return ok
                return Response::json(['success' => true, 'html' => $html, 'sent_at' => strtotime($message->sent_at), 'message_id' => $message->id]);
            }
        }

        // something went wrong
        return Response::json(['success' => false]);
    }

    /**
     * Load previous messages
     *
     */
    public function load_previous_messages() {
        // validation rules
        $rules = array(
            'conversation' => 'required|exists:conversations,id',
            'page' => 'required|integer'
        );

        // do validation
        $input = Input::all();
        $validator = Validator::make($input, $rules);

        if($validator->passes()) {
            // check if you are in this conversation
            $info = Auth::user()->conversations()
                        ->where('conversations.id', Input::get('conversation'))
                        ->first();

            if($info) {
                // set offset
                $skip = Input::get('page') * Config::get('settings.messages_per_load');

                // generate HTML
                $html = '';

                $messages = $info->messages()
                                 ->orderBy('sent_at', 'desc')
                                 ->take(Config::get('settings.messages_per_load'))
                                 ->skip($skip)
                                 ->get()
                                 ->reverse();

                $ids = array();

                foreach($messages as $key => $msg) {
                    // mark as read
                    $msg->read = 1;
                    $msg->read_at = date("Y-m-d H:i:s");
                    $msg->save();

                    // display user per batch
                    if(!isset($messages[$key - 1]) || (isset($messages[$key - 1]) && $messages[$key - 1]->user_id != $msg->user_id)) {
                        $html .= '<div class="message-user' . ($msg->user_id == Auth::user()->id ? ' me' : '') . '">' .
                                 '<strong>' . link_to('profile/' . $msg->user_id, $msg->user->username ? $msg->user->username : implode(' ', array($msg->user->firstname, $msg->user->lastname))) . '</strong>' .
                                 '</div>';
                    }

                    // the message
                    $this->data['msg'] = $msg;
                    $view = View::make('messages.single-message', $this->data);
                    $html .= (string) $view;

                    // display time after batch
                    if(!isset($messages[$key + 1]) || (isset($messages[$key + 1]) && $messages[$key + 1]->user_id != $msg->user_id)) {
                        $html .= '<div class="message-time' . ($msg->user_id == Auth::user()->id ? ' me' : '') . '">' .
                                 '<div><i class="fa fa-clock-o fa-fw" title="' . Lang::get('user.sent-on') . '"></i>' . date(date('Ymd') == date('Ymd', strtotime($msg->sent_at)) ? 'g:i A' : 'M j, Y', strtotime($msg->sent_at)) . '</div>' .
                                 ($msg->read == 1 ? '<div><i class="fa fa-check fa-fw" title="' . Lang::get('user.read-on') . '"></i>' . date(date('Ymd') == date('Ymd', strtotime($msg->read_at)) ? 'g:i A' : 'M j, Y', strtotime($msg->read_at)) . '</div>' : '') .
                                 '</div>';
                    }

                    // add to ids
                    $ids[] = $msg->id;
                }

                // return ok
                return Response::json(['success' => true, 'html' => $html, 'message_ids' => implode(',', $ids)]);
            }
        }

        // something went wrong
        return Response::json(['success' => false]);
    }

    /**
     * Load previous messages
     *
     */
    public function load_next_messages() {
        // validation rules
        $rules = array(
            'conversation' => 'required|exists:conversations,id',
            'latest_time' => 'required|integer',
            'latest_user' => 'required|integer'
        );

        // do validation
        $input = Input::all();
        $validator = Validator::make($input, $rules);

        if($validator->passes()) {
            // check if you are in this conversation
            $info = Auth::user()->conversations()
                        ->where('conversations.id', Input::get('conversation'))
                        ->first();

            if($info) {
                // generate HTML
                $html = '';

                $sent_at = date("Y-m-d H:i:s", Input::get('latest_time'));

                $messages = $info->messages()
                                 ->orderBy('sent_at', 'desc')
                                 ->where('sent_at', '>=', $sent_at)
                                 ->whereNotIn('id', Input::get('filter'))
                                 ->get()
                                 ->reverse();

                $current_user = Input::get('latest_user');
                $ids = array();

                foreach($messages as $key => $msg) {
                    // mark as read
                    $msg->read = 1;
                    $msg->read_at = date("Y-m-d H:i:s");
                    $msg->save();

                    // display user per batch
                    if($current_user != $msg->user_id) {
                        $html .= '<div class="message-user' . ($msg->user_id == Auth::user()->id ? ' me' : '') . '">' .
                                 '<strong>' . link_to('profile/' . $msg->user_id, $msg->user->username ? $msg->user->username : implode(' ', array($msg->user->firstname, $msg->user->lastname))) . '</strong>' .
                                 '</div>';
                    }

                    // the message
                    $this->data['msg'] = $msg;
                    $view = View::make('messages.single-message', $this->data);
                    $html .= (string) $view;

                    // display time after batch
                    if(!isset($messages[$key + 1]) || (isset($messages[$key + 1]) && $messages[$key + 1]->user_id != $msg->user_id)) {
                        $html .= '<div class="message-time' . ($msg->user_id == Auth::user()->id ? ' me' : '') . '">' .
                                 '<div><i class="fa fa-clock-o fa-fw" title="' . Lang::get('user.sent-on') . '"></i>' . date(date('Ymd') == date('Ymd', strtotime($msg->sent_at)) ? 'g:i A' : 'M j, Y', strtotime($msg->sent_at)) . '</div>' .
                                 ($msg->read == 1 ? '<div><i class="fa fa-check fa-fw" title="' . Lang::get('user.read-on') . '"></i>' . date(date('Ymd') == date('Ymd', strtotime($msg->read_at)) ? 'g:i A' : 'M j, Y', strtotime($msg->read_at)) . '</div>' : '') .
                                 '</div>';
                    }

                    // change current user
                    $current_user = $msg->user_id;

                    // add to ids
                    $ids[] = $msg->id;
                }

                // return ok
                return Response::json(['success' => true, 'html' => $html, 'last_user' => $current_user, 'message_ids' => implode(',', $ids)]);
            }
        }

        // something went wrong
        return Response::json(['success' => false]);
    }

    /**
     * Check for unread messages
     *
     */
    public function count_unread_messages() {
        $unread = Auth::user()->conversations()->whereExists(function($q) {
                            $q->select('id')
                              ->from('messages')
                              ->whereRaw(DB::getTablePrefix() . 'messages.conversation_id = ' . DB::getTablePrefix() . 'conversations.id')
                              ->where('user_id', '!=', Auth::user()->id)
                              ->where('read', '=', 0);
                          })
                          ->where('conversation_users.deleted', 0)
                          ->count();

        // return ok
        return Response::json(['success' => true, 'count' => $unread]);
    }

    public function delete_messages($ids) {
        if($ids) {
            if(!is_array($ids)) $ids = explode(',', $ids);

            $total = 0;

            foreach($ids as $id) {
                // check if you own this message
                $conversation = Auth::user()->conversations()->where('conversations.id', $id)->first();

                if($conversation) {
                    $unread = $conversation->messages()
                                           ->where('read', 0)
                                           ->where('user_id', '!=', Auth::user()->id)
                                           ->count();

                    // delete
                    if($conversation->pivot->deleted == 0) {
                        $conversation->users()->updateExistingPivot(Auth::user()->id, array('deleted' => 1), false);

                        // count deleted unread messages
                        if($unread > 0) {
                            $total++;
                        }
                    }
                }
            }

            // return
            if(Request::ajax()) {
                return Response::json(['success' => true, 'total' => $total]);
            } else {
                return $total;
            }
        }

        // something went wrong
        return Response::json(['success' => false]);
    }
}
