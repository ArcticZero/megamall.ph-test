<?php
/*
|--------------------------------------------------------------------------
| Product Controller
|--------------------------------------------------------------------------
|
| This controller manages the display of products on the mall.
|
*/

class ProductController extends BaseController {
    /**
     * Show all products
     *
     */
    public function show_all() {
        $this->data['name'] = Lang::get('product.all-products');
        $this->data['products'] = Product::all();
        $this->data['total_products'] = count($this->data['products']);
        
        $this->data['breadcrumbs'][] = array(Lang::get('navigation.products'), 'products');

        $this->data['title'] = Lang::get('navigation.products');

        return View::make('products.list', $this->data);
    }

    /**
     * Search
     *
     */
    public function search() {
        // filter inputs
        $str = preg_replace("/[^0-9a-zA-Z-]/", '', Input::get('s'));
        $cat = preg_replace("/[^0-9]/", '', Input::get('c'));
        $sortby = preg_replace( "/[^a-z]/", '', Input::get('sort'));
        $show = preg_replace("/[^0-9]/", '', Input::get('show'));

        // redirect if nothing sent
        if(!$str && !$cat) {
            return Redirect::to('/');
        }

        // get category info
        if($cat) {
            $catinfo = Category::info($cat);
        }

        if($str) {
            $ip = Request::getClientIp();
            $now = time();

            // check if this IP has searched this term recently
            $hsql = SearchHistory::where('ip_address', $ip);

            if(Auth::check()) {
                $hsql->orWhere('user_id', Auth::user()->id);
            }

            $hist = $hsql->orderBy('search_time', 'desc')
                         ->first();

            if(!$hist || ($hist && $now - strtotime($hist->search_time) > Config::get('settings.search_history_spam_threshold') * 60)) {
                // add to search history
                $shrow = new SearchHistory;
                $shrow->ip_address = $ip;

                if(Auth::check()) {
                    $shrow->user_id = Auth::user()->id;
                }

                $shrow->search_term = $str;
                $shrow->search_time = date("Y-m-d H:i:s", $now);
                $shrow->save();
            }
        } else {
            // go to category product list if no string is sent
            return Redirect::to(isset($catinfo) ? $catinfo->permalink : '/');
        }

        // determine sorting
        switch($sortby) {
            case 'rating':
                $sort = 'products.avg_rating';
                $order = 'desc';
                break;
            case 'lowprice':
                $sort = 'products.price';
                $order = 'asc';
                break;
            case 'highprice':
                $sort = 'products.price';
                $order = 'desc';
                break;
            case 'date':
            default:
                $sort = 'products.created_at';
                $order = 'desc';
        }

        // add relevance to sorting
        array_unshift($this->data['sort_by'], 'relevance');

        // filter per page limit
        $show = in_array($show, explode(",", Config::get('settings.products_per_page_options'))) ? $show : Config::get('settings.products_per_page');

        $this->data['name'] = isset($catinfo) ? Lang::get('product.search-for-in', array('term' => $str, 'cat' => $catinfo->name)) : Lang::get('product.search-for', array('term' => $str));
        $this->data['products'] = Product::search($str, $cat, $sort, $order, $show)->appends(array('s' => $str, 'cat' => $cat, 'show' => $show, 'sort' => $sortby));
        $this->data['total_products'] = count($this->data['products']);

        $this->data['breadcrumbs'][] = array(Lang::get('navigation.products'), 'products');
        $this->data['breadcrumbs'][] = array(Lang::get('product.search-results'), '');

        $this->data['title'] = $this->data['name'];
        
        return View::make('products.list', $this->data);
    }

    /**
     * View product details
     *
     */
    public function view($permalink) {
        // check if slug exists
        $product = Product::getBySlug($permalink);

        if($product) {
            $this->data['info'] = $product;
            
            // get images
            $images = $product->images()->remember(Config::get('settings.review_cache_time'))->get();
            $this->data['images'] = array();

            if($product->image_url) {
                $this->data['images'][] = $product->store_url . $product->image_url;
            }

            foreach($images as $img) {
                $this->data['images'][] = $img->image_url;
            }
            
            // get reviews
            $this->data['reviews'] = ProductReview::getReviews($product->product_id, $product->store_id);
            $this->data['total_reviews'] = ProductReview::where('product_id', $product->product_id)
                                                        ->where('store_id', $product->store_id)
                                                        ->where('status', 1)
                                                        ->count();

            $this->data['bookmarked'] = false;
            $this->data['review_pages'] = ceil($this->data['total_reviews'] / Config::get('settings.reviews_per_load'));

            // get all category slugs
            $cat = Category::info($product['category_id']);

            if($cat) {
                $cat_crumbs = array(array($cat->name, $cat->permalink));
                $parent = $cat->parent_id;

                while($parent != false) {
                    $pinfo = Category::info($parent);
                    $cat_crumbs[] = array($pinfo->name, $pinfo->permalink);
                    $parent = $pinfo->parent_id;
                }

                foreach(array_reverse($cat_crumbs) as $crumb) {
                    $this->data['breadcrumbs'][] = $crumb;
                }
            }

            if(Auth::check()) {
                // check if bookmarked
                $this->data['bookmarked'] = Auth::user()->bookmarks()
                                                ->where('product_id', $product->product_id)
                                                ->where('store_id', $product->store_id)
                                                ->count();

                // clear cache of viewed items
                Cache::forget('user-product-views-' . Auth::user()->id . '-' . Config::get('settings.home-viewed-products-count'));
            }

            // check if viewed
            $ip = Request::getClientIp();
            $start_time = new DateTime('today');
            $end_time = new DateTime('today');
            $end_time = $end_time->modify('+1 day');

            $view = ProductView::where(function($q) use ($ip) {
                                   if(Auth::check()) {
                                       $q->where('user_id', '=', Auth::user()->id);
                                   } else {
                                       $q->where('ip_address', '=', $ip);
                                   }
                               })
                               ->where('store_id', $product->store_id)
                               ->where('product_id', $product->product_id)
                               ->whereBetween('viewed_at', array($start_time, $end_time))
                               ->first();

            if(!$view) {
                // add product view
                $view = new ProductView;
                $view->user_id = Auth::check() ? Auth::user()->id : null;
                $view->ip_address = $ip;
                $view->store_id = $product->store_id;
                $view->product_id = $product->product_id;
                $view->save();
            }

            $this->data['breadcrumbs'][] = array(Lang::get('navigation.products'), 'products');
            $this->data['breadcrumbs'][] = array($product->name, '');

            $this->data['title'] = $product->name;

            $this->data['img_width'] = Config::get("image.sizes.medium.0");
            $this->data['img_height'] = Config::get("image.sizes.medium.1");
            $this->data['thumb_width'] = Config::get("image.sizes.tiny.0");
            $this->data['thumb_height'] = Config::get("image.sizes.tiny.1");

            return View::make('products.view', $this->data);
        } else {
            // not found
            App::abort(404);
        }
    }

    /**
     * Toggle bookmarking an item
     *
     */
    public function toggle_bookmark($slug) {
        $product = Product::getBySlug($slug);

        // require a product and user
        if(!$product || Auth::guest()) {
            App::abort(404);
        }

        // check if a bookmark exists
        $exists = Bookmark::where('user_id', Auth::user()->id)
                          ->where('store_id', $product->store_id)
                          ->where('product_id', $product->product_id)
                          ->first();

        if($exists) {
            // delete existing bookmark
            $exists->delete();
            $action = 'del';
        } else {
            // add new bookmark
            $bookmark = new Bookmark;
            $bookmark->user_id = Auth::user()->id;
            $bookmark->store_id = $product->store_id;
            $bookmark->product_id = $product->product_id;
            $bookmark->save();
            $action = 'add';
        }

        // return ok
        if(Request::ajax()) {
            return Response::json(['success' => true, 'action' => $action]);
        } else {
            return Redirect::to('products/' . $slug);
        }
    }

    /**
     * Save product review
     *
     */
    public function save_review() {
        // require user
        if(Auth::guest()) {
            App::abort(404);
        }

        // nullify textarea if blank
        if(Input::get('comments') == '<p><br></p>') {
            Input::merge(array('comments' => null));
        }

        // validation rules
        $rules = array(
            'rating' => 'required|digits_between:1,5',
            'title' => 'required',
            'comments' => 'required|max_clean:' . Config::get('settings.feedback_review_max_characters'),
            'product' => 'exists:products,permalink',
            'honeypot' => 'blank'
        );

        // custom messages
        $messages = array(
            'max_clean' => Lang::get('validation.max.string', array('max' => Config::get('settings.feedback_review_max_characters')))
        );

        // do validation
        $input = Input::all();
        $validator = Validator::make($input, $rules, $messages);

        if($validator->passes()) {
            // get product info
            $info = Product::getBySlug(Input::get('product'));

            // get store info
            $store = Store::where('store_url', $info->store_url)->first();

            // check auth, and users cannot review their own products
            if(Auth::check() && Auth::user()->id != $store->user_id && $info->status == 1 && $store->status == 1) {
                // need approval?
                $approval = Config::get('settings.approve_reviews');

                // no errors, create
                $review = new ProductReview;
                $review->rating = Input::get('rating');
                $review->title = Input::get('title');
                $review->comments = Input::get('comments');
                $review->product_id = $info->product_id;
                $review->store_id = $info->store_id;
                $review->posted_by = Auth::user()->id;
                $review->posted_on = date("Y-m-d H:i:s");
                $review->status = $approval == 1 ? 0 : 1;
                $review->save();

                // if no approval is necessary, generate HTML
                if(!$approval) {
                    $this->data['firstname'] = Auth::user()->firstname;
                    $this->data['lastname'] = Auth::user()->lastname;
                    $this->data['username'] = Auth::user()->username;
                    $this->data['review'] = $review;
                    $view = View::make('products.single-review', $this->data);
                    $html = (string) $view;
                } else {
                    $html = false;
                }

                // re-calculate product average rating
                Product::computeRating($info->store_id, $info->product_id);

                // delete cache for this product
                Cache::forget('product-' . $info->permalink);

                // return ok
                return Response::json(['success' => true, 'notice' => $approval == 1 ? Lang::get('product.review-approval') : Lang::get('product.review-success'), 'html' => $html]);
            } else {
                // not authorized
                return Response::json(['success' => false ]);
            }
        } else {
            // something went wrong
            return Response::json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()]);
        }
    }

    /**
     * Load more product reviews
     *
     */
    public function load_reviews() {
        // validation rules
        $rules = array(
            'product' => 'exists:products,permalink',
            'page' => 'integer'
        );

        // do validation
        $input = Input::all();
        $validator = Validator::make($input, $rules);

        if($validator->passes()) {
            // get product info
            $info = Product::getBySlug(Input::get('product'));

            // get reviews
            $skip = Input::get('page') * Config::get('settings.reviews_per_load');
            $reviews = ProductReview::getReviews($info->product_id, $info->store_id, $skip);

            // generate HTML
            $html = '';

            foreach($reviews as $review) {
                $user = User::find($review->posted_by);

                $this->data['firstname'] = $user->firstname;
                $this->data['lastname'] = $user->lastname;
                $this->data['username'] = $user->username;
                $this->data['review'] = $review;
                $view = View::make('products.single-review', $this->data);
                $html .= (string) $view;
            }

            // return ok
            return Response::json(['success' => true, 'html' => $html]);
        } else {
            // something went wrong
            return Response::json(['success' => false]);
        }
    }
}
