<?php
/*
|--------------------------------------------------------------------------
| Help Controller
|--------------------------------------------------------------------------
|
| Handles help center and reporting of items to the administrator.
|
*/

class HelpController extends BaseController {
    /**
     * Home page
     *
     */
    public function home() {
        return View::make('under-construction', $this->data);
    }

    /**
     * Contact page
     *
     */
    public function contact_us() {
        return View::make('under-construction', $this->data);
    }

    /**
     * Terms and Conditions
     *
     */
    public function terms() {
        return View::make('under-construction', $this->data);
    }

    /**
     * Privacy Policy
     *
     */
    public function privacy() {
        return View::make('under-construction', $this->data);
    }

    /**
     * Report an item
     *
     */
    public function report($type, $id) {
        $this->data['types'] = array('user', 'product', 'store', 'feedback', 'review');

        if(in_array($type, $this->data['types'])) {
            // get info
            switch($type) {
                case 'product':
                    $info = Product::getBySlug($id);
                    if($info) {
                        $name = $info->name;
                    }
                    break;
                case 'user':
                    $info = User::find($id);
                    if($info) {
                        $name = $info->username ? $info->username : implode(' ', array($info->firstname, $info->lastname));
                    }
                    break;
                case 'review':
                    $info = ProductReview::find($id);
                    if($info) {
                        $product = Product::getByIds($info->store_id, $info->product_id);
                        $poster = User::find($info->posted_by);
                        $poster_name = $poster->username ? $poster->username : implode(' ', array($poster->firstname, $poster->lastname));
                        $name = Lang::get('help.review-name', array('product' => $product->name, 'user' => $poster_name));
                    }
                    break;
                case 'feedback':
                    $info = Feedback::find($id);
                    if($info) {
                        $poster = User::find($info->posted_by);
                        $poster_name = $poster->username ? $poster->username : implode(' ', array($poster->firstname, $poster->lastname));
                        $user = User::find($info->user_id);
                        $user_name = $user->username ? $user->username : implode(' ', array($user->firstname, $user->lastname));
                        $name = Lang::get('help.feedback-name', array('poster' => $poster_name, 'user' => $user_name));
                    }
                    break;
            }

            if($info) {
                $this->data['type'] = $type;
                $this->data['id'] = $id;
                $this->data['name'] = $name;
                $this->data['info'] = $info;
                $this->data['poster_name'] = Auth::check() ? implode(' ', array(Auth::user()->firstname, Auth::user()->lastname)) : '';
                $this->data['poster_email'] = Auth::check() ? Auth::user()->email : '';

                $this->data['breadcrumbs'][] = array(Lang::get('help.help-center'), 'help');
                $this->data['breadcrumbs'][] = array(Lang::get('help.send-a-report'), '');

                $this->data['title'] = Lang::get('help.send-a-report');

                return View::make('help.report', $this->data);
            }
        }

        // not found
        App::abort(404);
    }

    /**
     * Save report
     *
     */
    public function do_report() {
        $id = Input::get('item_id');
        $type = Input::get('item_type');

        // nullify textarea if blank
        if(Input::get('message') == '<p><br></p>') {
            Input::merge(array('message' => null));
        }

        // get info based on report type
        if($type) {
            $field = 'id';

            switch($type) {
                case 'product':
                    $table = 'products';
                    $field = 'permalink';
                    $route = 'products/' . $id;
                    break;
                case 'user':
                    $table = 'users';
                    $route = 'profile/' . $id;
                    break;
                case 'review':
                    $table = 'product_reviews';
                    $info = ProductReview::find($id);
                    if($info) {
                        $product = Product::getByIds($info->store_id, $info->product_id);
                        $route = 'products/' . $product->permalink;
                    }
                    break;
                case 'feedback':
                    $table = 'feedback';
                    $info = Feedback::find($id);
                    if($info) {
                        $user = User::find($info->user_id);
                        $route = 'profile/' . $user->id;
                    }
                    break;
            }
        }

        // validation rules
        $rules = array(
            'poster_name' => 'required',
            'poster_email' => 'required|email',
            'item_type' => 'in:product,user,review,feedback',
            'item_id' => 'exists:' . $table . ',' . $field,
            'message' => 'required|max_clean:' . Config::get('settings.report_max_characters'),
            'honeypot' => 'blank'
        );

        // custom messages
        $messages = array(
            'required' => Lang::get('forms.error-required'),
            'email' => Lang::get('forms.error-email'),
            'max_clean' => Lang::get('validation.max.string', array('max' => Config::get('settings.report_max_characters')))
        );

        // do validation
        $input = Input::all();
        $validator = Validator::make($input, $rules, $messages);

        if($validator->passes()) {
            // no errors, create report
            $report = new Report;
            $report->item_type = $type;

            if($type == 'product') {
                $product = Product::getBySlug($id);
                $report->item_id = $product->product_id;
                $report->store_id = $product->store_id;
            } else {
                $report->item_id = $id;
            }

            $report->user_id = Auth::check() ? Auth::user()->id : null;
            $report->name = Input::get('poster_name');
            $report->email = Input::get('poster_email');
            $report->message = Input::get('message');
            $report->save();

            if(Request::ajax()) {
                return Response::json(['success' => true]);
            } else {
                return Redirect::to($route)
                               ->with('notice', array(
                                    'title' => Lang::get('forms.success-head'),
                                    'msg' => Lang::get('forms.success-report'),
                                    'type' => 'success'
                                ));
            }
        } else {
            if(Request::ajax()) {
                return Response::json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()]);
            } else {
                // something went wrong
                return Redirect::to('help/report/' . $type . '/' . $id)
                               ->withInput()
                               ->withErrors($validator);
            }
        }
    }
}
