<?php
/*
|--------------------------------------------------------------------------
| User Controller
|--------------------------------------------------------------------------
|
| You'll find all manner of user account related methods in this controller.
|
*/

class UserController extends BaseController {
    /**
     * Displays the form for account creation
     *
     */
    public function signup() {
        if(Auth::guest()) {
            $this->data['breadcrumbs'][] = array(Lang::get('forms.register'), '');

            $this->data['title'] = Lang::get('forms.register');

            return View::make('users.signup', $this->data);
        } else {
            return Redirect::to('/');
        }
    }

    /**
     * Stores new account
     *
     */
    public function do_signup() {
        // validation rules
        $rules = array(
            'firstname' => 'required||alpha_spaces',
            'lastname' => 'required||alpha_spaces',
            'username' => 'alpha_num|min:4|unique:users,username',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8|confirmed',
            'honeypot' => 'blank'
        );

        // custom messages
        $messages = array(
            'required' => Lang::get('forms.error-required'),
            'email' => Lang::get('forms.error-email'),
            'unique' => Lang::get('forms.error-email-taken'),
            'confirmed' => Lang::get('forms.error-equalTo')
        );

        // do validation
        $input = Input::all();
        $validator = Validator::make($input, $rules, $messages);

        if($validator->passes()) {
            // no errors, create user
            $user = new User;
            $code = str_random(60);
            $user->firstname = Input::get('firstname');
            $user->lastname = Input::get('lastname');
            $user->username = Input::get('username');
            $user->email = Input::get('email');
            $user->password = Hash::make(Input::get('password'));
            $user->confirmation_code = $code;
            $user->save();

            // set data for email
            $data = array(
                'firstname' => $input['firstname'],
                'email' => $input['email'],
                'code' => $code
            );

            // send confirmation email
            Mail::queue('emails.confirmation', $data, function($message) use ($input) {
                $message->to($input['email'], $input['firstname'] . " " . $input['lastname'])
                        ->subject(Lang::get('email.user-confirm-subject'));
            });

            return Redirect::action('UserController@login')
                           ->with('notice', array(
                                'title' => Lang::get('forms.success-reg-head'),
                                'msg' => Lang::get('forms.success-reg'),
                                'type' => 'success'
                            ));
        } else {
            // Houston, we have a problem!
            return Redirect::action('UserController@signup')
                           ->withInput(Input::except('password'))
                           ->withErrors($validator);
        }
    }

    /**
     * Displays the login form
     *
     */
    public function login() {
        if(Auth::guest()) {
            $this->data['breadcrumbs'][] = array(Lang::get('forms.login'), '');

            // keep redirect
            Session::keep(array('redirect'));

            $this->data['title'] = Lang::get('forms.login');

            return View::make('users.login', $this->data);
        } else {
            return Redirect::to('/');
        }
    }
	
	/**
     * OAuth Login
     *
     */
	public function oauth_login($provider) {
		// get data from input
		$code = Input::get('code');
        $store = Input::get('s');
	
		// get service
		$provider = ucfirst($provider);
		$service = OAuth::consumer($provider);
	
		// if code is provided get user data and sign in
		if (!empty($code)) {
			// this was a callback request from the provider, get the token
			$token = $service->requestAccessToken($code);

			// send request with token
			$result = json_decode($service->request(Config::get("oauth-4-laravel::consumers.$provider.request_str")), true);
			
			// handle vars
			$name = array(
						  'firstname' => $result[Config::get("oauth-4-laravel::consumers.$provider.firstname_var")],
						  'lastname' => $result[Config::get("oauth-4-laravel::consumers.$provider.lastname_var")],
						 );
						 
			$pid = $result['id'];
	
			// check if provider ID exists
			$uid = Provider::where('provider', $provider)
						   ->where('provider_id', $pid)
						   ->pluck('user_id');

            // check if confirmed user with this email exists
            $urow = User::where('email', $result['email'])
                        ->first();
			
			// provider doesn't exist
			if(!$uid) {
				// user also not found
				if(!$urow) {
					// create new user account
					$user = new User(array(
						'firstname' => $name['firstname'],
						'lastname' => $name['lastname'],
						'email' => $result['email'],
						'password' => Hash::make($pid . time()),
                        'confirmed' => 1
					));
					
					$user->save();
					
					// add provider row
                    $prov = new Provider(array(
                        'user_id' => $user->id,
                        'provider' => $provider,
                        'provider_id' => $pid
                    ));

                    $prov->save();
					
					// login user
                    $login = self::do_login_with_id($user->id);

                    if($login) {
                        // redirect
                        if(Session::has('redirect')) {
                            return Redirect::to(Session::get('redirect'));
                        } else {
                            return Redirect::back();
                        }
                    }
				} else if($urow['status'] == 1 && $urow['confirmed'] == 1) {
                    // user exists, add provider row
					$prov = new Provider(array(
                        'user_id' => $urow['id'],
                        'provider' => $provider,
                        'provider_id' => $pid
                    ));

                    $prov->save();
					
					// login user
                    $login = self::do_login_with_id($urow['id']);

                    if($login) {
                        // redirect
                        if(Session::has('redirect')) {
                            return Redirect::to(Session::get('redirect'));
                        } else {
                            return Redirect::back();
                        }
                    }
				}		
			} else if($urow['status'] == 1 && $urow['confirmed'] == 1) {
				// user-provider pair exists. login immediately
                $login = self::do_login_with_id($uid);

                if($login) {
                    // redirect
                    if(Session::has('redirect')) {
                        return Redirect::to(Session::get('redirect'));
                    } else {
                        return Redirect::back();
                    }
                }
			}
		} else {
			// get authorization first
			$url = $service->getAuthorizationUri();
	
			// return to login url
			return Redirect::to((string)$url);
		}

        // produce error
        if($urow) {
            if($urow['status'] == 0) {
                $error = Lang::get('forms.error-login-status');
            } else if($urow['confirmed'] == 0) {
                $error = Lang::get('forms.error-login-confirm');
            }
        }

        // keep redirect
        Session::keep(array('redirect'));

        // an error occurred
        return Redirect::action('UserController@login')
                       ->with('notice', array(
                            'title' => Lang::get('forms.error-head'),
                            'msg' => isset($error) ? $error : Lang::get('forms.error-login-db'),
                            'type' => 'danger'
                        ));
	}

    /**
     * Login user using ID
     *
     */
    private static function do_login_with_id($id, $remember = true) {
        $user = Auth::loginUsingId($id, $remember);

        if($user) {
            // get address info
            $address = Address::find($user->address_id);
            
            // store session
            $_SESSION['customer_id'] = $user->id;

            if($address) {
                $_SESSION['shipping_country_id'] = $address->country_id;
                $_SESSION['shipping_zone_id'] = $address->zone_id;
                $_SESSION['shipping_postcode'] = $address->postcode;
                $_SESSION['payment_country_id'] = $address->country_id;
                $_SESSION['payment_zone_id'] = $address->zone_id;
            }

            // save user IP
            $user->ip = Request::getClientIp();
            $user->save();
        }

        return $user ? true : false;
    }

    /**
     * Attempt to do login
     *
     */
    public function do_login() {
        $input = array(
            'auth' => Input::get('auth'),
            'password' => Input::get('password'),
            'remember' => (bool) Input::get('remember', false),
            'honeypot' => Input::get('honeypot')
        );

        // determine if username or email was passed
        $field = filter_var(Input::get('auth'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        if($input['honeypot']) {
            // robot detected
            $error = Lang::get('validation.blank');
        } else if(Auth::attempt(array($field => $input['auth'], 'password' => $input['password'], 'status' => 1, 'confirmed' => 1), $input['remember'])) {
            // get user
            $user = Auth::user();

            // get address info
            $address = Address::find($user->address_id);
            
            // store session
            $_SESSION['customer_id'] = $user->id;

            if($address) {
                $_SESSION['shipping_country_id'] = $address->country_id;
                $_SESSION['shipping_zone_id'] = $address->zone_id;
                $_SESSION['shipping_postcode'] = $address->postcode;
                $_SESSION['payment_country_id'] = $address->country_id;
                $_SESSION['payment_zone_id'] = $address->zone_id;
            }

            // save user IP
            $user->ip = Request::getClientIp();
            $user->save();

            if(Session::has('redirect')) {
                return Redirect::to(Session::get('redirect'));
            } else {
                return Redirect::back();
            }
        } else if(Auth::validate(array($field => $input['auth'], 'password' => $input['password']))) {
            $user = User::where($field, $input['auth'])->first();

            if($user->status == 0) {
                // user is not active
                $error = Lang::get('forms.error-login-status');
            } else if($user->status == 2) {
                // user is banned
                $error = Lang::get('forms.error-login-banned');
            } else if($user->confirmed == 0) {
                // user has not yet been confirmed
                $error = Lang::get('forms.error-login-confirm');
            }
        } else {
            // invalid login
            $error = Lang::get('forms.error-login-user');
        }

        // keep redirect
        Session::keep(array('redirect'));

        // return error
        return Redirect::action('UserController@login')
                       ->withInput(Input::except('password'))
                       ->with('notice', array(
                            'title' => Lang::get('forms.error-head'),
                            'msg' => $error,
                            'type' => 'danger'
                       ));
    }

    /**
     * View user profile
     *
     */
    public function profile($user_id = null) {
        // check if user exists
        $user = Auth::check() && (is_null($user_id) || $user_id == Auth::user()->id) ? Auth::user() : User::find($user_id);

        if($user) {
            $this->data['info'] = $user;

            // get address if specified
            foreach($this->data['info']->addresses as $address) {
                if($address->id == $user->address_id) {
                    $this->data['address'] = $address;
                    break;
                }
            }
            
            // get feedback
            $this->data['feedback'] = Feedback::getFeedback($user->id);
            $ids = array();
            $classes = array();

            // mark all shown feedback as read
            if(Auth::check() && Auth::user()->id == $user->id) {
                foreach($this->data['feedback'] as $feedback) {
                    if($feedback->read == 0) {
                        $ids[] = $feedback->id;
                        $classes[] = '[data-fb-id="' . $feedback->id . '"]';
                    }
                }

                $total_unread = count($ids);

                if($total_unread > 0) self::read_feedback($ids);

                // remove from unread count
                $this->data['unread']['fb'] = $this->data['unread']['fb'] - $total_unread;
            }

            $this->data['sell_feedback'] = Feedback::getFeedback($user->id, 'sell');
            $this->data['buy_feedback'] = Feedback::getFeedback($user->id, 'buy');
            $this->data['given_feedback'] = Feedback::getGiven($user->id);

            $this->data['total_feedback'] = Feedback::where('user_id', $user->id)
                                                    ->where('status', 1)
                                                    ->count();

            $this->data['total_sell_feedback'] = Feedback::where('user_id', $user->id)
                                                         ->where('type', 'sell')
                                                         ->where('status', 1)
                                                         ->count();

            $this->data['total_buy_feedback'] = Feedback::where('user_id', $user->id)
                                                        ->where('type', 'buy')
                                                        ->where('status', 1)
                                                        ->count();

            $this->data['total_given_feedback'] = Feedback::where('posted_by', $user->id)
                                                          ->where('status', 1)
                                                          ->count();

            $this->data['feedback_pages'] = ceil($this->data['total_feedback'] / Config::get('settings.feedback_per_load'));
            $this->data['sell_feedback_pages'] = ceil($this->data['total_sell_feedback'] / Config::get('settings.feedback_per_load'));
            $this->data['buy_feedback_pages'] = ceil($this->data['total_buy_feedback'] / Config::get('settings.feedback_per_load'));
            $this->data['given_feedback_pages'] = ceil($this->data['total_given_feedback'] / Config::get('settings.feedback_per_load'));

            $this->data['idstring'] = implode(',', $classes);

            $title = Auth::check() && Auth::user()->id == $user->id ? Lang::get('user.my-profile') : Lang::get('user.profile-of', array('name' => $user->username ? $user->username : implode(' ', array($user->firstname, $user->lastname))));

            $this->data['breadcrumbs'][] = array($title, '');

            $this->data['title'] = $title;

            return View::make('users.profile', $this->data);
        } else {
            // not found
            if(is_null($user_id)) {
                return Redirect::to('login')
                               ->with('redirect', 'profile');
            } else {
                App::abort(404);
            }
        }
    }

    /**
     * Edit user profile
     *
     */
    public function edit_profile() {
        // require user
        if(Auth::guest()) {
            return Redirect::to('login')
                           ->with('redirect', 'profile/edit');
        }

        // check if user exists
        $user = Auth::user();

        if($user) {
            $this->data['info'] = $user;

            if($this->data['info']->store && $this->data['info']->store->status == 1) {
                $url = $this->data['info']->store->store_url;
                $url = parse_url($url, PHP_URL_HOST);
                $this->data['info']->store->store_url = strstr(str_replace("www.", "", $url), ".", true);
                $this->data['store_categories'] = $this->data['info']->store->categories()->lists('category_id');
            }

            // format addresses
            $this->data['addresses'] = array();
            
            foreach($this->data['info']->addresses as $row) {
                $address_name = implode(' ', array_filter(array($row->firstname, $row->lastname))) . ($row->company ? " ($row->company)" : '');
                $address_fields = isset($row->zone) ? array($row->address_1, $row->address_2, $row->city, $row->zone->name, $row->country->name, $row->postcode) : array($row->address_1, $row->address_2, $row->city, $row->country->name, $row->postcode);
                $this->data['addresses'][$row->id] = ($address_name ? $address_name . ' - ' : '') . implode(', ', array_filter($address_fields));
            }

            $this->data['breadcrumbs'][] = array(Lang::get('user.my-profile'), 'profile');
            $this->data['breadcrumbs'][] = array(Lang::get('user.edit-profile'), '');

            $this->data['title'] = Lang::get('user.edit-profile');

            return View::make('users.edit-profile', $this->data);
        } else {
            App::abort(404);
        }
    }

    /**
     * Save profile
     *
     */
    public function save_profile() {
        // require user
        if(Auth::guest()) {
            App::abort(404);
        }

        // check if user exists
        $user = User::find(Auth::user()->id);

        if($user) {
            // nullify textarea if blank
            if(Input::get('store_description') == '<p><br></p>') {
                Input::merge(array('store_description' => null));
            }

            // validation rules
            $rules = array(
                'firstname' => 'required|alpha_spaces',
                'lastname' => 'required|alpha_spaces',
                'address_id' => 'exists:user_addresses,id',
                'username' => 'alpha_num|min:4|unique:users,username,' . Auth::user()->id,
                'email' => 'required|email|unique:users,email,' . Auth::user()->id,
                'fax' => 'integer',
                'ym_id' => 'alpha_dash',
                'skype_id' => 'alpha_dash',
                'password' => 'min:8|confirmed',
                'honeypot' => 'blank'
            );

            $categories = Input::get('categories');

            if($categories) {
                foreach($categories as $index => $cat) {
                    $rules['categories.' . $index] = 'integer|exists:categories,id';
                }
            }

            // format URL for megamall
            if(Auth::user()->store && Auth::user()->store->status == 1 && strlen(Input::get('store_url')) >= 4 && ctype_alnum(Input::get('store_url'))) {
                $url = 'http://' . Input::get('store_url') . '.megamall.ph/';
                Input::merge(array('store_url' => $url));
            }

            // custom messages
            $messages = array(
                'required' => Lang::get('forms.error-required'),
                'email' => Lang::get('forms.error-email'),
                'unique' => Lang::get('forms.error-attr-taken'),
                'confirmed' => Lang::get('forms.error-equalTo'),
                'exists' => Lang::get('forms.error-add-not-found'),
                'max_clean' => Lang::get('validation.max.string', array('max' => Config::get('settings.store_description_max_characters')))
            );

            // additional rules
            if(Auth::user()->store && Auth::user()->store->status == 1) {
                $srules = array(
                    'store_name' => 'required|min:4|alpha_spaces|unique:stores,store_name,' . Auth::user()->store->id,
                    'store_url' => 'required|min:4|alpha_dash|unique:stores,store_url,' . Auth::user()->store->id,
                    'store_description' => 'max_clean:' . Config::get('settings.store_description_max_characters'),
                    'store_address_id' => 'exists:user_addresses,id',
                    'store_email' => 'email'
                );

                $rules = array_merge($rules, $srules);
            }

            // do validation
            $input = Input::all();
            $validator = Validator::make($input, $rules, $messages);

            // follow up
            $validator->sometimes('current_password', 'required|authenticate', function($input) {
                return Input::get('email') != Auth::user()->email || Input::get('username') != Auth::user()->username || Input::get('password') || Input::get('confirm_password') ? true : false;
            });

            if($validator->passes()) {
                // confirm new email if given
                if(Auth::user()->email != Input::get('email')) {
                    $code = str_random(60);
                    $user->confirmation_code = $code;
                    $user->confirmed = 0;

                    $data = array(
                                  'firstname' => Input::get('firstname'),
                                  'email' => Input::get('email'),
                                  'code' => $code
                                 );

                    // send confirmation email
                    Mail::queue('emails.email-change', $data, function($message) use ($input) {
                        $message->to(Input::get('email'), Input::get('firstname') . " " . Input::get('lastname'))
                                ->subject(Lang::get('email.user-confirm-subject'));
                    });

                    // log user out after done
                    $redirect = 'UserController@logout';
                } else {
                    $redirect = 'UserController@profile';
                }

                // no errors, save profile
                $user->firstname = Input::get('firstname');
                $user->lastname = Input::get('lastname');
                $user->username = Input::get('username');
                $user->address_id = Input::get('address_id') ? Input::get('address_id') : null;
                $user->telephone = Input::get('telephone');
                $user->fax = Input::get('fax');
                $user->ym_id = Input::get('ym_id');
                $user->skype_id = Input::get('skype_id');
                $user->email = Input::get('email');

                if(Input::get('password')) {
                    $user->password = Hash::make(Input::get('password'));
                }
                
                if(Auth::user()->store && Auth::user()->store->status == 1) {
                    $store = Store::where('id', $user->store->id)->first();

                    $store->description = Input::get('store_description');
                    $store->address_id = Input::get('store_address_id') ? Input::get('store_address_id') : null;
                    $store->telephone = Input::get('store_telephone');
                    $store->fax = Input::get('store_fax');
                    $store->email = Input::get('store_email');
                    $store->save();

                    // add categories
                    $store->categories()->sync($categories != null ? $categories : array());

                    // connect to local store db
                    Config::set('database.connections.store', Config::get('stores/' . $store->id . '.connection'));     
                    $db = DB::Connection('store');

                    if($store->address) {
                        $address = isset($store->address->zone) ? array($store->address->address_1, $store->address->address_2, $store->address->city, $store->address->zone->name, $store->address->country->name, $store->address->postcode) : array($store->address->address_1, $store->address->address_2, $store->address->city, $store->address->country->name, $store->address->postcode);
                        $address = implode(', ', array_filter($address));
                    } else {
                        $address = '';
                    }

                    // update local store info
                    $db->table('setting')->where('key', 'config_owner')->update(array('value' => $user->firstname . ' ' . $user->lastname));
                    $db->table('setting')->where('key', 'config_address')->update(array('value' => $address));
                    $db->table('setting')->where('key', 'config_email')->update(array('value' => $store->email));
                    $db->table('setting')->where('key', 'config_telephone')->update(array('value' => $store->telephone));
                    $db->table('setting')->where('key', 'config_fax')->update(array('value' => $store->fax));
                    
                    // create request if store name or URL are changed
                    if((Auth::user()->store->store_name != Input::get('store_name') || Auth::user()->store->store_url != Input::get('store_url')) && Auth::user()->store->status == 1) {
                        // delete all previous requests
                        StoreEditRequest::where('store_id', Auth::user()->store->id)
                                        ->delete();

                        // new request
                        $request = new StoreEditRequest;
                        $request->store_id = Auth::user()->store->id;
                        $request->store_name = Auth::user()->store->store_name != Input::get('store_name') ? Input::get('store_name') : null;
                        $request->store_url = Auth::user()->store->store_url != Input::get('store_url') ? Input::get('store_url') : null;
                        $request->save();
                    }
                }
                
                // save user data
                $user->save();

                return Redirect::action($redirect)
                               ->with('notice', array(
                                    'title' => Lang::get('forms.success-head'),
                                    'msg' => Lang::get('forms.success-edit-profile'),
                                    'type' => 'success'
                                ));
            } else {
                // stuff happens
                return Redirect::action('UserController@edit_profile')
                               ->withInput(Input::except('password', 'current_password'))
                               ->withErrors($validator);
            }
        } else {
            App::abort(404);
        }
    }

    /**
     * Manage address book
     *
     */
    public function address_book() {
        // require user
        if(Auth::guest()) {
            return Redirect::to('login')
                           ->with('redirect', 'profile/address-book');
        }

        // check if user exists
        $user = Auth::user();
        
        if($user) {
            $this->data['user'] = $user;
            $this->data['countries'] = Country::orderBy('name', 'asc')->get();
            $this->data['breadcrumbs'][] = array(Lang::get('user.my-profile'), 'profile');
            $this->data['breadcrumbs'][] = array(Lang::get('user.manage-addresses'), '');

            $this->data['title'] = Lang::get('user.manage-addresses');

            return View::make('users.address-book', $this->data);
        } else {
            // not found
             App::abort(404);
        }
    }

    /**
     * Save user addresses
     *
     */
    public function save_addresses() {
        // require user
        if(Auth::guest()) {
            App::abort(404);
        }

        // check if user exists
        $user = User::find(Auth::user()->id);

        if($user) {
            // validation rules
            $rules = array('honeypot' => 'blank');
            $names = array();
            $old = array();

            if(Input::get('old')) {
                foreach(Input::get('old') as $id) {
                    // check if you own this address
                    $address = Auth::user()->addresses()->where('user_addresses.id', $id)->first();

                    if($address) {
                        $rs = array(
                            "firstname.$id" => 'required|alpha_spaces',
                            "lastname.$id" => 'required|alpha_spaces',
                            "country_id.$id" => 'required|exists:countries,country_id',
                            "zone_id.$id" => 'exists:zones,id'
                        );

                        $fn = array(
                            "firstname.$id" => 'first name',
                            "lastname.$id" => 'last name',
                            "country_id.$id" => 'country',
                            "zone_id.$id" => 'zone',
                        );

                        $rules = array_merge($rules, $rs);
                        $names = array_merge($names, $fn);
                        $old[] = $address;
                    }
                }
            }

            if(Input::get('new')) {
                for($i = 0; $i < count(Input::get('new.firstname')); $i++) {
                    $rs = array(
                        "new.firstname.$i" => 'required|alpha_spaces',
                        "new.lastname.$i" => 'required|alpha_spaces',
                        "new.country_id.$i" => 'required|exists:countries,country_id',
                        "new.zone_id.$i" => 'exists:zones,id'
                    );

                    $fn = array(
                        "new.firstname.$i" => 'first name',
                        "new.lastname.$i" => 'last name',
                        "new.country_id.$i" => 'country',
                        "new.zone_id.$i" => 'zone',
                    );

                    $rules = array_merge($rules, $rs);
                    $names = array_merge($names, $fn);
                }
            }

            // custom messages
            $messages = array(
                'required' => Lang::get('forms.error-required'),
                'exists' => Lang::get('forms.error-add-not-found')
            );

            // do validation
            $input = Input::all();
            $validator = Validator::make($input, $rules, $messages);
            $validator->setAttributeNames($names); 

            if($validator->passes()) {
                // no errors, save all addresses
                DB::beginTransaction();

                $exclude = array();

                // save old addresses
                if(!empty($old)) {
                    foreach($old as $address) {
                        $address->firstname = Input::get("firstname.$address->id");
                        $address->lastname = Input::get("lastname.$address->id");
                        $address->company = Input::get("company.$address->id");
                        $address->address_1 = Input::get("address_1.$address->id");
                        $address->address_2 = Input::get("address_2.$address->id");
                        $address->city = Input::get("city.$address->id");
                        $address->country_id = Input::get("country_id.$address->id");
                        $address->zone_id = Input::get("zone_id.$address->id");
                        $address->postcode = Input::get("postcode.$address->id");
                        $address->save();

                        $exclude[] = $address->id;
                    }
                }

                // delete addresses not included
                if(!empty($exclude)) {
                    Auth::user()->addresses()->whereNotIn('id', $exclude)->delete();
                }

                // save new addresses
                if(Input::get('new')) {
                    for($i = 0; $i < count(Input::get('new.firstname')); $i++) {
                        $address = new Address;
                        $address->user_id = Auth::user()->id;
                        $address->firstname = Input::get("new.firstname.$i");
                        $address->lastname = Input::get("new.lastname.$i");
                        $address->company = Input::get("new.company.$i");
                        $address->address_1 = Input::get("new.address_1.$i");
                        $address->address_2 = Input::get("new.address_2.$i");
                        $address->city = Input::get("new.city.$i");
                        $address->country_id = Input::get("new.country_id.$i");
                        $address->zone_id = Input::get("new.zone_id.$i");
                        $address->postcode = Input::get("new.postcode.$i");
                        $address->save();
                    }
                }

                // finish
                DB::commit();

                return Redirect::action('UserController@profile')
                               ->with('notice', array(
                                    'title' => Lang::get('forms.success-head'),
                                    'msg' => Lang::get('forms.success-edit-addresses'),
                                    'type' => 'success'
                                ));
            } else {
                // stuff happens
                return Redirect::action('UserController@address_book')
                               ->withInput()
                               ->withErrors($validator);
            }
        } else {
            App::abort(404);
        }
    }

    /**
     * Get zones for country
     *
     */
    public function get_zones() {
        // require user and ajax
        if(Auth::guest() || !Request::ajax()) {
            App::abort(404);
        }

        // check if country exists
        $country = Country::find(Input::get('country_id'));
        
        if($country && $country->zones) {
            $zones = $country->zones()
                             ->orderBy('name', 'asc')
                             ->get()
                             ->toArray();

            return Response::json(['success' => true, 'zones' => $zones]);
        } else {
            // not found
            return Response::json(['success' => false]);
        }
    }

    /**
     * Manage bookmarks
     *
     */
    public function bookmarks() {
        // filter inputs
        $sortby = preg_replace( "/[^a-z]/", '', Input::get('sort'));
        $show = preg_replace("/[^0-9]/", '', Input::get('show'));

        // require user
        if(Auth::guest()) {
            return Redirect::to('login')
                           ->with('redirect', 'profile/bookmarks');
        }

        // check if user exists
        $user = Auth::user();
        
        if($user) {
            // determine sorting
            switch($sortby) {
                case 'rating':
                    $sort = 'products.avg_rating';
                    $order = 'desc';
                    break;
                case 'lowprice':
                    $sort = 'products.price';
                    $order = 'asc';
                    break;
                case 'highprice':
                    $sort = 'products.price';
                    $order = 'desc';
                    break;
                case 'date':
                default:
                    $sort = 'products.created_at';
                    $order = 'desc';
            }

            // filter per page limit
            $show = in_array($show, explode(",", Config::get('settings.products_per_page_options'))) ? $show : Config::get('settings.products_per_page');

            $this->data['user'] = $user;
            $this->data['products'] = Product::getBookmarks($sort, $order, $show);
            $this->data['total_products'] = Auth::user()->bookmarks()->count();

            $this->data['name'] = Lang::get('user.manage-bookmarks');
            $this->data['sidebar'] = 'users.sidebar';
            $this->data['sidebar_data'] = array();

            $this->data['breadcrumbs'][] = array(Lang::get('user.my-profile'), 'profile');
            $this->data['breadcrumbs'][] = array(Lang::get('user.manage-bookmarks'), '');

            $this->data['title'] = Lang::get('user.manage-bookmarks');

            return View::make('products.list', $this->data);
        } else {
            // not found
             App::abort(404);
        }
    }

    /**
     * Mark feedback as read
     *
     */
    public function read_feedback($ids) {
        if($ids && Auth::check()) {
            if(!is_array($ids)) $ids = explode(',', $ids);

            foreach($ids as $id) {
                // get info
                $feedback = Feedback::find($id);

                // check if you own this feedback
                if(Auth::user()->id == $feedback->user_id) {
                    // mark as read
                    if($feedback->read == 0) {
                        Feedback::where('id', $feedback->id)
                                ->update(array('read' => 1));
                    }
                }
            }
        }
    }


    /**
     * Save feedback
     *
     */
    public function save_feedback() {
        // require user
        if(Auth::guest()) {
            App::abort(404);
        }
        
        // nullify textarea if blank
        if(Input::get('comments') == '<p><br></p>') {
            Input::merge(array('comments' => null));
        }

        // validation rules
        $rules = array(
            'rating' => 'required|digits_between:1,5',
            'comments' => 'required|max_clean:' . Config::get('settings.feedback_review_max_characters'),
            'product' => 'exists:products,permalink',
            'type' => 'required|in:buy,sell',
            'honeypot' => 'blank'
        );

        // custom messages
        $messages = array(
            'max_clean' => Lang::get('validation.max.string', array('max' => Config::get('settings.feedback_review_max_characters')))
        );

        // do validation
        $input = Input::all();
        $validator = Validator::make($input, $rules, $messages);

        if($validator->passes()) {
            // get user info
            $info = User::find(Input::get('user'));

            // check auth, and users cannot rate themselves
            if(Auth::check() && Auth::user()->id != $info->user_id && $info->status == 1) {
                // need approval?
                $approval = Config::get('settings.approve_feedback');

                // no errors, create
                $feedback = new Feedback;
                $feedback->rating = Input::get('rating');
                $feedback->comments = Input::get('comments');
                $feedback->user_id = $info->id;
                $feedback->posted_by = Auth::user()->id;
                $feedback->posted_on = date("Y-m-d H:i:s");
                $feedback->type = Input::get('type');
                $feedback->status = $approval == 1 ? 0 : 1;
                $feedback->save();

                // if no approval is necessary, generate HTML
                if(!$approval) {
                    $this->data['firstname'] = Auth::user()->firstname;
                    $this->data['lastname'] = Auth::user()->lastname;
                    $this->data['feedback'] = $feedback;
                    $view = View::make('users.single-feedback', $this->data);
                    $html = (string) $view;
                } else {
                    $html = false;
                }

                // re-calculate product average rating
                User::computeRating($info->id, Input::get('type'));

                // delete cache for this user
                //Cache::forget('user-' . $info->id);

                // return ok
                return Response::json(['success' => true, 'notice' => $approval == 1 ? Lang::get('user.feedback-approval') : Lang::get('user.feedback-success'), 'html' => $html]);
            } else {
                // not authorized
                return Response::json(['success' => false ]);
            }
        } else {
            // something went wrong
            return Response::json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()]);
        }
    }

    /**
     * Load more feedback
     *
     */
    public function load_feedback() {
        // validation rules
        $rules = array(
            'user' => 'exists:users,id',
            'page' => 'integer',
            'type' => 'in:all,buy,sell,given'
        );

        // do validation
        $input = Input::all();
        $validator = Validator::make($input, $rules);

        if($validator->passes()) {
            // vars
            $id = Input::get('user');
            $type = Input::get('type') != 'all' ? Input::get('type') : false;

            // get feedback
            $skip = Input::get('page') * Config::get('settings.feedback_per_load');
            $feedback = $type == 'given' ? Feedback::getGiven($id, false, $skip) : Feedback::getFeedback($id, $type, $skip);

            $html = '';
            $ids = array();
            $classes = array();
            $total_unread = 0;

            foreach($feedback as $row) {
                // generate HTML
                $user = User::find($type == 'given' ? $row->user_id : $row->posted_by);

                if($type == 'given') $this->data['given'] = true;
                $row->firstname = $user->firstname;
                $row->lastname = $user->lastname;
                $this->data['feedback'] = $row;
                $view = View::make('users.single-feedback', $this->data);
                $html .= (string) $view;

                // mark as read
                if(Auth::check() && Auth::user()->id == $row->user_id) {
                    if($row->read == 0) {
                        $ids[] = $row->id;
                        $classes[] = '[data-fb-id="' . $row->id . '"]';
                    }

                    $total_unread = count($ids);
                    if($total_unread > 0) self::read_feedback($ids);
                }
            }

            // return ok
            return Response::json(['success' => true, 'html' => $html, 'unread' => $total_unread, 'idstring' => implode(',', $classes)]);
        } else {
            // something went wrong
            return Response::json(['success' => false]);
        }
    }

    /**
     * Check for unread feedback
     *
     */
    public function count_unread_feedback() {
        if(Auth::check()) {
            $unread = Feedback::where('user_id', Auth::user()->id)
                              ->where('read', 0)
                              ->count();

            // return ok
            return Response::json(['success' => true, 'count' => $unread]);
        }

        // something went wrong
        return Response::json(['success' => false]);
    }

    /**
     * Attempt to confirm account with code
     *
     */
    public function confirm($code) {
        // check if code exists
        $user_id = User::where('confirmation_code', $code)->pluck('id');

        if($user_id) {
            // confirm and activate user
            $user = User::find($user_id);
            $user->confirmation_code = NULL;
            $user->confirmed = 1;
            $user->status = 1;
            $user->save();

            return Redirect::action('UserController@login')
                           ->with('notice', array(
                                'title' => Lang::get('forms.success-confirm-head'),
                                'msg' => Lang::get('forms.success-confirm'),
                                'type' => 'success'
                           ));
        } else {
            // no such code
            return Redirect::action('UserController@login')
                           ->with('notice', array(
                                'title' => Lang::get('forms.error-head'),
                                'msg' => Lang::get('forms.error-confirm'),
                                'type' => 'danger'
                           ));
        }
    }

    /**
     * Displays the forgot password form
     *
     */
    public function forgot_password() {
        if(Auth::guest()) {
            $this->data['breadcrumbs'][] = array(Lang::get('user.password-recovery'), '');

            $this->data['title'] = Lang::get('user.password-recovery');

            return View::make('users.forgotpass', $this->data);
        } else {
            return Redirect::to('/');
        }
    }

    /**
     * Attempt to send change password link to the given email
     *
     */
    public function do_forgot_password() {
        $user = User::where('email', Input::get('email'))
                    ->first();

        View::composer('emails.reminder', function($view) use ($user) {
            $view->with([
                'firstname' => $user->firstname
            ]);
        });

        $response = Password::remind(Input::only('email'), function($message) {
            $message->subject(Lang::get('email.pw-reminder-subject'));
        });

        switch ($response) {
            case Password::INVALID_USER:
                return Redirect::back()->with('notice', array(
                    'title' => Lang::get('forms.error-head'),
                    'msg' => Lang::get($response),
                    'type' => 'danger'
                ));

            case Password::REMINDER_SENT:
                return Redirect::to('/')
                               ->withInput()
                               ->with('notice', array(
                                    'msg' => Lang::get($response),
                                    'type' => 'success'
                ));
        }
    }

    /**
     * Shows the change password form with the given token
     *
     */
    public function reset_password($token = null) {
        if(is_null($token)) App::abort(404);

        if(Auth::guest()) {
            $this->data['token'] = $token;
            $this->data['breadcrumbs'][] = array(Lang::get('user.reset-password'), '');

            $this->data['title'] = Lang::get('user.reset-password');

            return View::make('users.resetpass', $this->data);
        } else {
            return Redirect::to('/');
        }
    }

    /**
     * Attempt change password of the user
     *
     */
    public function do_reset_password() {
        $credentials = Input::only(
            'email', 'password', 'password_confirmation', 'token'
        );

        Password::validator(function($credentials) {
            return strlen($credentials['password']) >= 8 && $credentials['password'] != $credentials['email'];
        });

        $response = Password::reset($credentials, function($user, $password) {
            $user->password = Hash::make($password);
            $user->save();
        });

        switch ($response) {
            case Password::INVALID_PASSWORD:
            case Password::INVALID_TOKEN:
            case Password::INVALID_USER:
                return Redirect::back()
                               ->withInput(Input::except('password'))
                               ->with('notice', array(
                                    'title' => Lang::get('forms.error-head'),
                                    'msg' => Lang::get($response),
                                    'type' => 'danger'
                ));
            case Password::PASSWORD_RESET:
                return Redirect::to('login')
                               ->with('notice', array(
                                    'title' => Lang::get('forms.success-head'),
                                    'msg' => Lang::get('reminders.complete'),
                                    'type' => 'success'
                ));
        }
    }

    /**
     * Log the user out of the application.
     *
     */
    public function logout() {
        if(Auth::check()) {
            $user = Auth::user();
            $user->ip = NULL;
            $user->token = NULL;
            $user->save();

            // unset store sessions
            session_destroy();

            // reflash message if exists
            Session::keep(array('notice'));
            
            // logout
            Auth::logout();
        }

        return Redirect::to('/');
    }
}
