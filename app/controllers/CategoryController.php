<?php
/*
|--------------------------------------------------------------------------
| Category Controller
|--------------------------------------------------------------------------
|
| This controller manages the display of product categories on the mall.
|
*/

class CategoryController extends BaseController {
    /**
     * Show all categories
     *
     */
    public function show_all() {
        $this->data['breadcrumbs'][] = array(Lang::get('product.all-categories'), '');
        $this->data['title'] = Lang::get('product.all-categories');
        return View::make('categories.list', $this->data);
    }

    /**
     * List products in category
     *
     */
    public function products($permalink) {
        // filter inputs
        $sortby = preg_replace( "/[^a-z]/", '', Input::get('sort'));
        $show = preg_replace("/[^0-9]/", '', Input::get('show'));

        // check if slug exists
        $info = Category::getBySlug($permalink);

        if($info) {
            // determine sorting
            switch($sortby) {
                case 'rating':
                    $sort = 'products.avg_rating';
                    $order = 'desc';
                    break;
                case 'lowprice':
                    $sort = 'products.price';
                    $order = 'asc';
                    break;
                case 'highprice':
                    $sort = 'products.price';
                    $order = 'desc';
                    break;
                case 'date':
                default:
                    $sort = 'products.created_at';
                    $order = 'desc';
            }

            // filter per page limit
            $show = in_array($show, explode(",", Config::get('settings.products_per_page_options'))) ? $show : Config::get('settings.products_per_page');

            // get children
            $children = Category::get($info->id);
            $this->data['children'] = array();

            foreach($children as $row) {
                $this->data['children'][] = link_to(url($row['permalink']), $row['name']);
            }

            $this->data['total_children'] = count($this->data['children']);

            $this->data['info'] = $info;
            $this->data['name'] = $info->name;
            $this->data['products'] = Product::search(false, $info->id, $sort, $order, $show)->appends(array('show' => $show, 'sort' => $sortby));
            $this->data['total_products'] = count($this->data['products']);
            $this->data['breadcrumbs'][] = array(Lang::get('navigation.products'), 'products');

            // get all category slugs
            $cat_crumbs = array(array($info->name, $info->permalink));
            $parent = $info->parent_id;

            while($parent != false) {
                $pinfo = Category::info($parent);
                $cat_crumbs[] = array($pinfo->name, $pinfo->permalink);
                $parent = $pinfo->parent_id;
            }

            foreach(array_reverse($cat_crumbs) as $crumb) {
                $this->data['breadcrumbs'][] = $crumb;
            }

            $this->data['title'] = $info->name;

            return View::make('products.list')
                       ->with($this->data);
        } else {
            // not found
            App::abort(404);
        }
    }
}
