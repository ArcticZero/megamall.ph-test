<?php
/*
|--------------------------------------------------------------------------
| Image Controller
|--------------------------------------------------------------------------
|
| This controller is responsible for all image manipulation.
|
*/

class ImageController extends BaseController {

    /**
     * Generate Thumbnail
     *
     */
    public static function thumb($url, $size, $save, $fill = true) {

        $url = str_replace(" ", "%20", $url);
        $path = pathinfo($url);
        $width = Config::get("image.sizes.$size.0");
        $height = Config::get("image.sizes.$size.1");

        // debug
        //echo 'Generating thumbnail of: ' . $url . ', ' . $size  . "\r\n";

        $img = Image::make($url)
                    ->resize($width, $height, function($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });

        // fill out canvas if specified
        if($fill) {
            $img->resizeCanvas($width, $height, 'center', false, '#fff');
        }
        
        // save
        $img->save($save . '/' . $path['filename'] . '-' . $width . 'x' . $height . '.jpg');

        // free memory
        $img->destroy();

        // done!
        return true;
    }
}
