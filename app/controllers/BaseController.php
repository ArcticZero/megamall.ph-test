<?php

class BaseController extends Controller {
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	public $data = array();

	public function __construct() {
		$this->data['products_per_page'] = Input::get('show') ? Input::get('show') : Config::get('settings.products_per_page');
		$this->data['messages_per_page'] = Input::get('show') ? Input::get('show') : Config::get('settings.messages_per_page');
		$this->data['sort_by'] = array('date', 'rating', 'lowprice', 'highprice');

		if(Request::is(Config::get('app.admin') . '/*')) {
			$this->data['breadcrumbs'][] = array(Lang::get('admin.admin-panel'), Config::get('app.admin'));
		} else {
			$this->data['breadcrumbs'][] = array(Lang::get('navigation.home'), '/');
		}

        if(Auth::check()) {
        	// asset arrays
			$this->data['css'] = array();
			$this->data['add_css'] = array();
			$this->data['js'] = array();
			$this->data['add_js'] = array();
			
			//if(Auth::user()->admin == 0) {
		        // reload session variables if remembered
		        if(Auth::viaRemember()) {
		        	// get address info
		            $address = Address::find(Auth::user()->address_id);
		            
		            // store session
		            $_SESSION['customer_id'] = Auth::user()->id;

		            if($address) {
			            $_SESSION['shipping_country_id'] = $address->country_id;
			            $_SESSION['shipping_zone_id'] = $address->zone_id;
			            $_SESSION['shipping_postcode'] = $address->postcode;
			            $_SESSION['payment_country_id'] = $address->country_id;
			            $_SESSION['payment_zone_id'] = $address->zone_id;
			        }
				}

				// check for unread feedback
				$this->data['unread']['fb'] = Feedback::where('user_id', Auth::user()->id)
										  			  ->where('read', 0)
										   			  ->count();

				// check for unread messages
				$this->data['unread']['pm'] = Auth::user()->conversations()->whereExists(function($q) {
															$q->select('id')
															  ->from('messages')
															  ->whereRaw(DB::getTablePrefix() . 'messages.conversation_id = ' . DB::getTablePrefix() . 'conversations.id')
															  ->where('user_id', '!=', Auth::user()->id)
															  ->where('read', '=', 0);
														  })
														  ->where('conversation_users.deleted', 0)
														  ->count();
			//}	
		}
	}

	protected function setupLayout() {
		if (!is_null($this->layout)) {
			$this->layout = View::make($this->layout);
		}
	}

	static protected function slug($title, $type) {
		$slug = Str::slug(str_replace(' & ', ' and ', html_entity_decode($title, ENT_COMPAT)));
		$slugCount = count($type::whereRaw("permalink REGEXP '^{$slug}(-[0-9]*)?$'")->get());

		return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
	}

	static public function productSlug($title) {
		return self::slug($title, 'Product');
	}

	static public function categorySlug($title) {
		return self::slug($title, 'Category');
	}

	static public function storeSlug($title) {
		return self::slug($title, 'Store');
	}

}