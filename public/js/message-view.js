$(document).ready(function() {
	var message_page = 1;

	$("#form-send-message").submit(function(e) {
		e.preventDefault();

		var message = $('input[name=message]').val();
		var honeypot = $('input[name=honeypot]').val();

		$('button[name=send], input[name=message]').attr('disabled', true);

		if($(this).hasClass('new')) {
			var subject = $('input[name=subject]').val();

			$('input[name=subject]').attr('disabled', true);

			$.post($(this).attr('action') + '/new', { user: user, subject: subject, message: message, honeypot: honeypot }, function(response) {
				if(response.success) {
					if(response.html) {
						$('.message-subject').css('width', 'auto').html(response.subject);
						$('#form-send-message').removeClass('new');
						$('#message-list').append(response.html);
						$('.chat-box').scrollTop($('.chat-box').prop('scrollHeight'));
						conversation = response.conversation;
						message_ids.push(response.message_id);
					}
				} else {
					alert(response.error);
				}

				$('button[name=send], input[name=message]').attr('disabled', false);
				$('input[name=message]').val("").focus();
			});
		} else {
			var last_msg = $('.message-box').last();
			var latest_user = last_msg.data('user');
			
			$.post($(this).attr('action'), { conversation: conversation, message: message, latest_user: latest_user, honeypot: honeypot }, function(response) {
				if(response.success) {
					if(response.html) {
						var last_time = $('#message-list').find('.message-time').last();

						if(last_time.hasClass('me')) {
							last_time.remove();
						}

						$('#message-list').append(response.html);
						$('.chat-box').scrollTop($('.chat-box').prop('scrollHeight'));
						message_ids.push(response.message_id);
					}
				} else {
					alert(response.error);
				}

				$('button[name=send], input[name=message]').attr('disabled', false);
				$('input[name=message]').val("").focus();
			});
		}
	});

	$('#btn-load-more').click(function() {
		$(this).addClass('hidden');
		$('#message-loader').removeClass('hidden');

		$.post(baseurl + '/messages/load-previous', { conversation: conversation, page: message_page, 'filter[]': message_ids }, function(response) {
			if(response.success) {
				$('#message-list').prepend(response.html);

				if(message_page < message_pages - 1) {
					$('#btn-load-more').removeClass('hidden');
				}

				$('.chat-box').scrollTop(0);

				$.each(response.message_ids.split(','), function(index, value) {
					message_ids.push(value);
				});
			}

			$('#message-loader').addClass('hidden');
			message_page++;
		});
	});

	$('#btn-delete').click(function() {
		if(confirm(msg_delete_confirm)) {
	    	$.post(baseurl + '/messages/delete/' + conversation, function(response) {
	    		window.location = baseurl + '/messages';
	    	});
	    }
	});

	resizeChat();

	setInterval(function() {
		refreshChat();
	}, chat_poll_rate);
});

$(window).resize(function() {
	resizeChat();
});

function resizeChat() {
	var chatbox = $('.chat-box');
	var viewport = $(window).height();
	var footer = $('footer').outerHeight();
	var header = $('header').outerHeight() + $('#crumbs').outerHeight();
	var navbar = $('#navbar-top').outerHeight();
	var title = $('#conversation-title').outerHeight();
	var panelhead = chatbox.siblings('.panel-heading').outerHeight();
	var allowance = 250;

	var newheight = viewport - footer - header - title - panelhead - navbar - allowance;

	if(newheight >= parseInt(chatbox.css('min-height').replace('px', ''))) {
		chatbox.css('height', newheight + 'px');
	} else {
		chatbox.css('height', chatbox.css('min-height'));
	}

	chatbox.scrollTop(chatbox.prop('scrollHeight'));
}

function refreshChat() {
	console.log('convo='+conversation);
	if(parseInt(conversation) > 0) {
		var last_msg = $('.message-box').last();
		var latest_time = last_msg.data('sent-time');
		var latest_user = last_msg.data('user');

		$.post(baseurl + '/messages/load-next', { conversation: conversation, latest_time: latest_time, latest_user: latest_user, 'filter[]': message_ids }, function(response) {
			if(response.success) {
				if(response.html) {
					var last_time = $('#message-list').find('.message-time').last();

					if(last_msg.data('user') == response.last_user) {
						last_time.remove();
					}

					$('#message-list').append(response.html);

					$.each(response.message_ids.split(','), function(index, value) {
						message_ids.push(value);
					});

					$('.chat-box').scrollTop($('.chat-box').prop('scrollHeight'));
				}
			}
		});
	}

	
}