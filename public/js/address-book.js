$(document).ready(function() {
    $('#form-address-book').validate({
    	errorClass: 'label label-danger',
    	errorElement: 'div',
    	highlight: function(element, errorClass, validClass) {
            $(element).parent().addClass('has-error').removeClass('has-success');
            $(element).parent().children('span').remove();
            $(element).parent().append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parent().removeClass('has-error').addClass('has-success');
            $(element).parent().children('span').remove();
            $(element).parent().append('<span class="glyphicon glyphicon-ok form-control-feedback"></span>');
        },
        submitHandler: function(form) {
            $(form).find('[readonly]').remove();
            form.submit();
        },
        ignore: '[readonly=readonly]'
    });

    $(document).on('click', '.panel-heading', function(e) {
        if(e.target.nodeName == 'DIV') {
            $($(this).data('target')).collapse('toggle');
        }
    });

    $('.btn-delete').click(function() {
        if(confirm(msg_delete_confirm)) {
            $(this).closest('.panel').fadeOut(function() {
                $(this).remove();

                if($('#address-list').find('.panel:visible').length == 0) {
                    $('#address-notice').removeClass('hidden');
                }
            });
        }
    });

    $(document).on('click', '.btn-delete-new', function() {
        if(confirm(msg_delete_confirm)) {
            var panel = $(this).closest('.panel');

            if($('.new-row').length > 1) {
                $(panel).fadeOut(function() { $(this).remove(); });
            } else {
                $(panel).fadeOut(function() { 
                    $(this).addClass('hidden');

                    if($('#address-list').find('.panel:visible').length == 0) {
                        $('#address-notice').removeClass('hidden');
                    }
                });

                $(panel).find('input[type=text]').val('').attr('readonly', true);
                $(panel).find('.has-error, .has-success').removeClass('has-error').removeClass('has-success').children('span').remove();
                $(panel).find('input[name="new[firstname][]"]').val(fn);
                $(panel).find('input[name="new[lastname][]"]').val(ln);
                $(panel).find('.zone-select').attr('disabled', true).html('<' + 'option value="">' + select_ph + '<' + '/option>');
                $(panel).find('select').prop('selectedIndex', 0).attr('readonly', true);
                $(panel).find('.country-select').change();
            }
        }
    });

    $('#btn-add-new, .link-add-new').click(function() {
        new_adds++;

        var first_row = $('.new-row:first');
        $('#address-notice').addClass('hidden');

        if(first_row.hasClass('hidden')) {
            first_row.find('.collapse').addClass('in');
            first_row.find('.zone-select').attr('disabled', true).html('<' + 'option value="">' + select_ph + '<' + '/option>');
            first_row.find('input[type=text]').attr('readonly', false);
            first_row.find('select').prop('selectedIndex', 0).attr('readonly', false);
            first_row.find('.country-select').change();
            first_row.show().removeClass('hidden');
        } else {
            var row = first_row.clone();

            $(row).find('.panel-heading').attr('data-target', '#collapse-new' + new_adds);
            $(row).find('.collapse').attr('id', 'collapse-new' + new_adds).addClass('in');
            $(row).find('input[type=text]').val('').attr('readonly', false);
            $(row).find('.has-error, .has-success').removeClass('has-error').removeClass('has-success').children('span').remove();
            $(row).find('.form-control-feedback, .label-danger').remove();
            $(row).find('input[name="new[firstname][]"]').val(fn);
            $(row).find('input[name="new[lastname][]"]').val(ln);
            $(row).find('.new-row').val(new_adds);
            $(row).find('.zone-select').attr('disabled', true).html('<' + 'option value="">' + select_ph + '<' + '/option>');
            $(row).find('select').prop('selectedIndex', 0).attr('readonly', false);
            $(row).find('.country-select').change();

            // change input names
            $(row).find('input, select').filter(function() {
                if($(this).attr('name') !== undefined) {
                    return $(this).attr('name').match(/\[\d\]+$/);
                } else {
                    return false;
                }
            }).each(function() {
                $(this).attr('name', $(this).attr('name').replace(/\[\d\]+$/, '[]'));
            });

            $('#address-list').append(row);
            $('html, body').stop().animate({ scrollTop: $('#collapse-new' + new_adds).offset().top }, 500, 'easeInOutExpo', function() {
                scr = false;
            });
        }
    });

    $(document).on('change', '.country-select', function() {
        var zone = $(this).closest('.row').find('.zone-select');
        var val = zone.data('value');

        if($(this).val()) {
            $.post(baseurl + '/profile/addresses/zones', { country_id: $(this).val() }, function(response) {
                if(response.zones.length > 0) {
                    zone.html('');
                    var selected = ' selected="selected"';

                    $.each(response.zones, function(index, value) {
                        zone.append('<' + 'option value="' + value.id + '"' + (value.id == val ? selected : '') + '>' + value.name + '<' + '/option>');
                        zone.attr('disabled', false);
                    });
                } else {
                    zone.html('<' + 'option value="">' + no_zones + '<' + '/option>').attr('disabled', true);
                }
            });
        } else {
            zone.html('<' + 'option value="">' + select_ph + '<' + '/option>').attr('disabled', true);
        }
    });

    // set zones
    $('.country-select').change();
});