$(document).ready(function() {
    $.validator.messages = {
        required: "{{ Lang::get('forms.error-required') }}",
        remote: "{{ Lang::get('forms.error-remote') }}",
        email: "{{ Lang::get('forms.error-email') }}",
        url: "{{ Lang::get('forms.error-url') }}",
        date: "{{ Lang::get('forms.error-date') }}",
        dateISO: "{{ Lang::get('forms.error-dateISO') }}",
        number: "{{ Lang::get('forms.error-number') }}",
        digits: "{{ Lang::get('forms.error-digits') }}",
        creditcard: "{{ Lang::get('forms.error-creditcard') }}",
        equalTo: "{{ Lang::get('forms.error-equalTo') }}",
        maxlength: $.validator.format("{{ Lang::get('forms.error-maxlength-js') }}"),
        minlength: $.validator.format("{{ Lang::get('forms.error-minlength-js') }}"),
        rangelength: $.validator.format("{{ Lang::get('forms.error-rangelength-js') }}"),
        range: $.validator.format("{{ Lang::get('forms.error-range-js') }}"),
        max: $.validator.format("{{ Lang::get('forms.error-max-js') }}"),
        min: $.validator.format("{{ Lang::get('forms.error-min-js') }}")
    };
});