$(document).ready(function() {
	$('#form-display-options select').change(function() {
		var form = $('#form-display-options');
		var s = form.find('input[name=s]');
		var c = form.find('input[name=c]');

		if(s.val() === '') {
			s.attr('name', '');
		}

		if(c.val() === '') {
			c.attr('name', '');
		}

		form.submit();
	});

	$('.star-rating').raty({
		readOnly: true,
		score: function() {
			return $(this).attr('data-score');
		}
	});
});