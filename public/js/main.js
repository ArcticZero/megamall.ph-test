$(document).ready(function() {
	$("#link-login").popover({
		html: true,
		placement: 'bottom',
		title: function() {
			return $("#login-box .box-title").html();
		},
		content: function() {
			return $("#login-box .box-content").html();
		},
		container: 'body'
	});

	$('html').on('click', function(e) {
	  if (typeof $(e.target).data('original-title') == 'undefined' && !$(e.target).parents().is('.popover.in')) {
	    $('#link-login').popover('hide');
	  }
	});

	$('#search-form').submit(function(e) {
		var s = $(this).find('input[name=s]');
		var c = $(this).find('select[name=c]');

		if(s.val() === '' && c.val() === '') {
			e.preventDefault();
		} else {
			if(s.val() === '') {
				s.attr('name', '');
			}

			if(c.val() === '') {
				c.attr('name', '');
			}
		}
	});

	$(".selectpicker").selectpicker();
});

function updateUnreadCounts(type, num, replace) {
	var fields = $('.unread-' + type);
	var fields_all = $('.unread-all');
	var field_val = fields.first().html();
	var old = field_val ? parseInt(field_val) : 0;
	var all = fields_all.first().html() ? parseInt(fields_all.first().html()) : 0;
	var count = parseInt(replace ? num : parseInt(old) + num);
	var total = (all - old) + count;

	//console.log('counts:' + old + ', ' + all + ', ' + count + ', ' + total + ', type: ' + type);

	if($(fields).length > 0) {
		$(fields).html(count);

		if(count <= 0) {
			$(fields).addClass('hidden');
		} else {
			$(fields).removeClass('hidden');
		}
	}

	if($(fields_all).length > 0) {
		$(fields_all).html(total);

		if(total <= 0) {
			$(fields_all).addClass('hidden');
		} else {
			$(fields_all).removeClass('hidden');
		}
	}
}

function refreshMessages() {
	$.post(baseurl + '/messages/count-unread', function(response) {
		if(response.success) {
			updateUnreadCounts('pm', response.count, true);
		}
	});
}

function refreshFeedback() {
	$.post(baseurl + '/review/user/count-unread', function(response) {
		if(response.success) {
			updateUnreadCounts('fb', response.count, true);
		}
	});
}