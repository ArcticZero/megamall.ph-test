$(document).ready(function() {
	$('#home-banner').nivoSlider({
		pauseTime: 4000,
		randomStart: true,
		controlNav: false
	});

	$('#home-box-1, #home-box-2').nivoSlider({
		effect: 'fade',
		controlNav: false,
		directionNav: false,
		randomStart: true,
		animSpeed: 200,
		pauseTime: 4000
	});

	$('.star-rating').raty({
		readOnly: true,
		score: function() {
			return $(this).attr('data-score');
		}
	});
});