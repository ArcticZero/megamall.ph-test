// show dialog modal
function dialog(title, body, action, id) {
	$("#dialog-title").html(title);
	$("#dialog-body").html(body);
	$("#dialog-confirm").data('url', action)
						.data('id', id);

	// show confirmation
    $("#modal-dialog").modal('show');
}

// get url parameter
function urlData(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');

    for(var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if(sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

$(document).ready(function() {
	toastr.options = {
		"closeButton": true,
		"positionClass": "toast-bottom-right",
		"progressBar": true,
		"timeOut": 5000,
		"extendedTimeOut": 5000
	};

	// reset form notice
	$("#modal-form").on('show.bs.modal', function(e) {
		$("#form-notice").html("");
	});

	// submit form
    $("#modal-form").find("form").submit(function(e) {
  		var form = $("#modal-form").find("form");
  		var loading = $("#load-form");

  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');

  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#form-notice');
				} else {
					$("#modal-form").modal('hide');
					status(response.title, response.body, 'alert-success');
				}

				loading.addClass('hide');

				// refresh the list
				refresh();
			}
  		});
    });
});