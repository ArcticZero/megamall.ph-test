$(document).ready(function() {
	$('#form-message-list select').change(function() {
		$('#form-message-list').submit();
	});

	$('#btn-mark-read').click(function() {
		var msgs = $('input:checkbox[name=messages]:checked').map(function() {
  			return $(this).val();
	    }).get();

		if(msgs.length > 0) {
	    	$.post(baseurl + '/messages/read/' + msgs.join(','), function(response) {
	    		$.each(msgs, function(index, value) {
	    			$('tr[data-con-id=' + value + ']').removeClass('unread').addClass('active');
	    		});

	    		updateUnreadCounts('pm', (response.total * -1));
	    	});
	    }
	});

	$('#btn-delete').click(function() {
		var msgs = $('input:checkbox[name=messages]:checked').map(function() {
  			return $(this).val();
	    }).get();

		if(msgs.length > 0) {
			if(confirm(msg_delete_confirm)) {
		    	$.post(baseurl + '/messages/delete/' + msgs.join(','), function(response) {
		    		$.each(msgs, function(index, value) {
		    			$('tr[data-con-id=' + value + ']').fadeOut('fast', function() {
		    				$(this).remove();
		    			});
		    		});

		    		updateUnreadCounts('pm', (response.total * -1));
		    	});
		    }
	    }
	});

	$('.message-list table tr').click(function(e) {
		if(e.target.nodeName == 'TD') {
			var msg = $(this).data('con-id');
			window.location = baseurl + '/messages/' + msg;
		}
	});

	$('#toggle-check').click(function() {
		$('tbody').find(':checkbox').prop('checked', this.checked);
	});
});