$(document).ready(function() {
    $('#form-create-store').validate({
    	errorClass: 'label label-danger',
    	errorElement: 'div',
        errorPlacement: function(error, element) {
            var fg = $(element).closest('.form-group');
            error.appendTo(fg);
        },
    	highlight: function(element, errorClass, validClass) {
            var fg = $(element).closest('.form-group');
            var tab = $(element).closest('.tab-pane').attr('id');
		    fg.addClass('has-error').removeClass('has-success');
		    fg.children('span').remove();
		    fg.append('<span class="glyphicon glyphicon-remove form-control-feedback"' + (fg.children('.input-group').length ? 'style="right: ' + (fg.find('.input-group-addon').width() + 25) + 'px"' : '') + '></span>');
            $('a[href="#' + tab + '"]').tab('show');
        },
	  	unhighlight: function(element, errorClass, validClass) {
            var fg = $(element).closest('.form-group');
		    fg.removeClass('has-error').addClass('has-success');
		    fg.children('span').remove();
		    fg.append('<span class="glyphicon glyphicon-ok form-control-feedback"' + (fg.children('.input-group').length ? 'style="right: ' + (fg.find('.input-group-addon').width() + 25) + 'px"' : '') + '></span>');
	  	},
	  	rules: {
            store_address: {
                remote: {
                    url: baseurl + "/validate/address-exists",
                    type: "post"
                }
            },
            store_name: {
               remote: {
                    url: baseurl + "/validate/store-name-taken",
                    type: "post",
                    data: {
                        edit: function() {
                            return '1';
                        }
                    }
                }
            },
            store_url: {
                remote: {
                    url: baseurl + "/validate/store-url-taken",
                    type: "post",
                    data: {
                        edit: function() {
                            return '1';
                        }
                    }
                }
            },
            store_telephone: {
                digits: true
            },
            store_fax: {
                digits: true
            }
	  	},
        ignore: '[type=file]'
    });

    $('#store-description').summernote({
        height: 200,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['fontname', ['fontname']]
        ]
    });

    $('#proof').fileinput({
        'showPreview': false,
        'showUpload': false,
        'browseIcon': '<i class="fa fa-folder-open fa-fw"></i>'
    });
});