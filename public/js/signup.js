$(document).ready(function() {
	ps = 0;
	var options = {};

    options.ui = {
    	usernameField: "#signup-firstname",
    	container: "#pw-feedback",
    	showVerdictsInsideProgressBar: true,
    	verdicts: pwlabels,
        viewports: {
            progress: ".pw-strength"
        }
    };

    options.common = {
    	minChar: 8,
    	onKeyUp: function(event, score) {
    		ps = score;
    	}
    };

    options.rules = {
    	activated: {
    		wordTwoCharacterClasses: true,
            wordRepetitions: true
    	}
    };

    $('#signup-password').pwstrength(options);
    $('.glyphicon-info-sign').tooltip();

    $.validator.addMethod('pwStrength', function(value, element, param) {
    	return ps.score >= 25 ? true : false; 
    }, msgWeakPw);

    $('#form-signup').validate({
    	errorClass: 'label label-danger',
    	errorElement: 'div',
    	highlight: function(element, errorClass, validClass) {
		    $(element).parent().addClass('has-error').removeClass('has-success');
		    $(element).parent().children('span').remove();
		    $(element).parent().append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
	  	},
	  	unhighlight: function(element, errorClass, validClass) {
		    $(element).parent().removeClass('has-error').addClass('has-success');
		    $(element).parent().children('span').remove();
		    $(element).parent().append('<span class="glyphicon glyphicon-ok form-control-feedback"></span>');
	  	},
	  	rules: {
	  		email: {
	  			remote: {
	  				url: baseurl + "/validate/email-taken",
	  				type: "post"
	  			}
	  		},
            username: {
                remote: {
                    url: baseurl + "/validate/username-taken",
                    type: "post",
                    data: {
                        edit: function() {
                            return '1';
                        }
                    }
                }
            },
	  		password: {
	  			pwStrength: true
	  		},
	  		password_confirmation: {
	      		equalTo: "#signup-password"
		    }
	  	}
    });
});