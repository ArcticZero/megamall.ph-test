$(document).ready(function() {
    $('#form-forgotpass').validate({
    	errorClass: 'label label-danger',
    	errorElement: 'div',
    	highlight: function(element, errorClass, validClass) {
		    $(element).parent().addClass('has-error').removeClass('has-success');
		    $(element).parent().children('span').remove();
		    $(element).parent().append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
	  	},
	  	unhighlight: function(element, errorClass, validClass) {
		    $(element).parent().removeClass('has-error').addClass('has-success');
		    $(element).parent().children('span').remove();
		    $(element).parent().append('<span class="glyphicon glyphicon-ok form-control-feedback"></span>');
	  	},
	  	rules: {
	  		email: {
	  			remote: {
	  				url: baseurl + "/validate/email-exists",
	  				type: "post"
	  			}
	  		}
	  	}
    });
});