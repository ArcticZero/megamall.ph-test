$(document).ready(function() {
    $('#form-report').validate({
    	errorClass: 'label label-danger',
    	errorElement: 'div',
    	highlight: function(element, errorClass, validClass) {
		    $(element).parent().addClass('has-error').removeClass('has-success');
		    $(element).parent().children('span').remove();
		    $(element).parent().append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
	  	},
	  	unhighlight: function(element, errorClass, validClass) {
		    $(element).parent().removeClass('has-error').addClass('has-success');
		    $(element).parent().children('span').remove();
		    $(element).parent().append('<span class="glyphicon glyphicon-ok form-control-feedback"></span>');
	  	}
    });

    $('#report-message').summernote({
		height: 200,
		toolbar: [
			['style', ['bold', 'italic', 'underline', 'clear']],
		    ['fontsize', ['fontsize']],
		    ['color', ['color']],
		    ['para', ['ul', 'ol', 'paragraph']],
		    ['height', ['height']],
		    ['fontname', ['fontname']]
		]
	});
});