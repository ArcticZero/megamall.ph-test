$(document).ready(function() {
	$('.star-rating').raty({
		readOnly: true,
		score: function() {
			return $(this).attr('data-score');
		}
	});
});