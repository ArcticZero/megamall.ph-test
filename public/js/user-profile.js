$(document).ready(function() {
	var feedback_page = 1;
	var sell_feedback_page = 1;
	var buy_feedback_page = 1;
	var given_feedback_page = 1;

	$('#link-show-feedback').click(function(e) {
		e.preventDefault();
		$(document.body).scrollTop($('#user-feedback').offset().top);
		showFeedback();
	});

	$('.link-post-feedback').click(function(e) {
		e.preventDefault();
		showFeedback();
		showEditor();
		$('#form-post-feedback').fadeIn().removeClass('hidden');
		$('.btn-post-feedback.hidden-xxs').addClass('hidden');
	});

	$('.btn-post-feedback').click(function() {
		showFeedback();
		showEditor();
		$('#form-post-feedback').fadeIn().removeClass('hidden');
		$('.btn-post-feedback.visible-xxs').addClass('hidden');
	});

	$("#btn-feedback-cancel").click(function() {
		$('#form-post-feedback, #error-box').addClass('hidden');
		$('.rating').raty('cancel');
		$('#feedback-type-buy').attr('checked', true);
		$('.btn-post-feedback.visible-xxs').removeClass('hidden');
		$('#feedback-comments').destroy().val("");
	});

	$("#form-post-feedback form").submit(function(e) {
		e.preventDefault();
		var rating = $('input[name=rating]').val();
		var comments = $('#feedback-comments').code();
		var type = $('input[name=type]:checked').val();
		var honeypot = $('input[name=honeypot]').val();
		
		$.post($(this).attr('action'), { rating: rating, type: type, comments: comments, user: userid, honeypot: honeypot }, function(response) {
			if(response.success) {
				$('#feedback-notice').html(response.notice).addClass('alert-success').removeClass('hidden alert-danger');
				$("#btn-feedback-cancel").click();
				
				if(response.html) {
					$('#feedback-list').prepend(response.html).children('.review-panel:first-child').find('.star-rating').raty({
						readOnly: true,
						score: function() {
							return $(this).attr('data-score');
						}
					});
				}
			} else {
				var alert = $('#error-box');
				var errors = '';

				$.each(response.errors, function(index, value) {
					errors += '<' + 'li>' + value + '<' + '/li>';
				});

				alert.children('ul').html(errors);
				alert.removeClass('hidden');
			}
		});
	});

	$('.star-rating').raty({
		readOnly: true,
		score: function() {
			return $(this).attr('data-score');
		}
	});

	$('#btn-load-more').click(function() {
		$(this).addClass('hidden');
		$('#feedback-loader').removeClass('hidden');

		$.post(baseurl + '/review/user/load', { user: userid, page: feedback_page, type: 'all' }, function(response) {
			if(response.success) {
				$('#feedback-list').append(response.html).find('.star-rating').raty({
					readOnly: true,
					score: function() {
						return $(this).attr('data-score');
					}
				});

				if(feedback_page < feedback_pages - 1) {
					$('#btn-load-more').removeClass('hidden');
				}

				updateUnreadCounts('fb', (response.unread * -1));
				feedback_ids = response.idstring;
			}

			$('#feedback-loader').addClass('hidden');
			feedback_page++;
		});
	});

	$('#btn-load-more-sell').click(function() {
		$(this).addClass('hidden');
		$('#sell-feedback-loader').removeClass('hidden');

		$.post(baseurl + '/review/user/load', { user: userid, page: sell_feedback_page, type: 'sell' }, function(response) {
			if(response.success) {
				$('#sell-feedback-list').append(response.html).find('.star-rating').raty({
					readOnly: true,
					score: function() {
						return $(this).attr('data-score');
					}
				});

				if(sell_feedback_page < sell_feedback_pages - 1) {
					$('#btn-load-more-sell').removeClass('hidden');
				}

				updateUnreadCounts('fb', (response.unread * -1));
				feedback_ids = response.idstring;
			}

			$('#sell-feedback-loader').addClass('hidden');
			sell_feedback_page++;
		});
	});

	$('#btn-load-more-buy').click(function() {
		$(this).addClass('hidden');
		$('#buy-feedback-loader').removeClass('hidden');

		$.post(baseurl + '/review/user/load', { user: userid, page: buy_feedback_page, type: 'buy' }, function(response) {
			if(response.success) {
				$('#buy-feedback-list').append(response.html).find('.star-rating').raty({
					readOnly: true,
					score: function() {
						return $(this).attr('data-score');
					}
				});

				if(buy_feedback_page < buy_feedback_pages - 1) {
					$('#btn-load-more-buy').removeClass('hidden');
				}

				updateUnreadCounts('fb', (response.unread * -1));
				feedback_ids = response.idstring;
			}

			$('#buy-feedback-loader').addClass('hidden');
			buy_feedback_page++;
		});
	});

	$('#btn-load-more-given').click(function() {
		$(this).addClass('hidden');
		$('#given-feedback-loader').removeClass('hidden');

		$.post(baseurl + '/review/user/load', { user: userid, page: given_feedback_page, type: 'given' }, function(response) {
			if(response.success) {
				$('#given-feedback-list').append(response.html).find('.star-rating').raty({
					readOnly: true,
					score: function() {
						return $(this).attr('data-score');
					}
				});

				if(given_feedback_page < given_feedback_pages - 1) {
					$('#btn-load-more-given').removeClass('hidden');
				}
			}

			$('#given-feedback-loader').addClass('hidden');
			given_feedback_page++;
		});
	});

	$('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
		// mark everything as read
		readFeedback();

		// set new idstring
		var selectors = [];
		var ids = [];

		$(e.target.hash).find('.panel-primary').each(function() {
			selectors.push('[data-fb-id="' + $(this).data('fb-id') + '"]');
			ids.push($(this).data('fb-id'));
		});

		// mark as read
		if(ids.length > 0) {
			$.post(baseurl + '/review/user/read/' + ids.join(','));
			updateUnreadCounts('fb', (selectors.length * -1));
		}

		// update feedback ids
		feedback_ids = selectors.join(',');
	});

	$('.rating').raty({
		scoreName: 'rating'
	});
});

function readFeedback() {
	if(feedback_ids) {
		var ids = [];

		// mark all as read
		$(feedback_ids).each(function() {
			$(this).removeClass('panel-primary').addClass('panel-default');
			$(this).find('.label-danger').removeClass('label-danger').addClass('label-default');

			ids.push($(this).data('fb-id'));
		});
	}
}

function showFeedback() {
	var feedbacktab = $('a[href="#all-feedback"]');

	if(!feedbacktab.parent().hasClass('active')) {
		feedbacktab.tab('show');
	}
}

function showEditor() {
	$('#feedback-comments').summernote({
		height: 200,
		toolbar: [
			['style', ['bold', 'italic', 'underline', 'clear']],
		    ['fontsize', ['fontsize']],
		    ['color', ['color']],
		    ['para', ['ul', 'ol', 'paragraph']],
		    ['height', ['height']],
		    ['fontname', ['fontname']]
		]
	});
}