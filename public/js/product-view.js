$(document).ready(function() {
	var review_page = 1;

	$('.flexslider').flexslider({
		animation: "slide",
	    animationLoop: false,
	    itemWidth: 75,
    	maxItems: 6,
	    controlNav: false,
	    prevText: '',
	    nextText: ''
	});

	$('#img-preview').elevateZoom({
		zoomType: "lens",
  		lensSize: 200,
  		containLensZoom : true,
  		borderSize: 1,
  		onImageSwap: function(e) {
  			$(".product-showcase .loader").show();
  		},
  		onImageSwapComplete: function(e) {
  			$(".product-showcase .loader").hide();
  		}
	});

	$('.product-thumb').on('mouseover', function() {
		var bigimg = $("#img-preview");
		var ez = bigimg.data('elevateZoom');
		
		$('.flexslider .slides li img.active').removeClass('active');
		$(this).addClass('active');

		ez.swaptheimage($(this).data('big-image'), $(this).data('zoom-image'));
	});

	$('#link-show-reviews').click(function(e) {
		e.preventDefault();
		$(document.body).scrollTop($('#product-details').offset().top);
		showReviews();
	});

	$('#link-write-review').click(function(e) {
		e.preventDefault();
		showEditor();
		$("#form-write-review").fadeIn().removeClass('hidden');
		$('.btn-write-review.hidden-xxs').addClass('hidden');
	});

	$('.btn-write-review').click(function() {
		showReviews();
		showEditor();
		$("#form-write-review").fadeIn().removeClass('hidden');
		$('.btn-write-review.visible-xxs').addClass('hidden');
		$(document.body).scrollTop($('#form-write-review').offset().top);
	});

	$("#btn-review-cancel").click(function() {
		$('#form-write-review, #error-box').addClass('hidden');
		$('#review-title, #review-comments').val("");
		$('.rating').raty('cancel');
		$('.btn-write-review.visible-xxs').removeClass('hidden');
	});

	$('.link-bookmark').click(function(e) {
		e.preventDefault();

		$.post(baseurl + '/bookmark/' + product_slug, function(response) {
			switch(response.action) {
				case 'add':
					$('#link-remove-bookmark').removeClass('hidden');
					$('#link-bookmark').addClass('hidden');
					break;
				case 'del':
					$('#link-remove-bookmark').addClass('hidden');
					$('#link-bookmark').removeClass('hidden');
			}
		});
	});

	$("#form-write-review form").submit(function(e) {
		e.preventDefault();
		var rating = $('input[name=rating]').val();
		var title = $('#review-title').val();
		var comments = $('#review-comments').code();
		var honeypot = $('input[name=honeypot]').val();
		
		$.post($(this).attr('action'), { rating: rating, title: title, comments: comments, product: product_slug, honeypot: honeypot }, function(response) {
			if(response.success) {
				$('#review-notice').html(response.notice).addClass('alert-success').removeClass('hidden alert-danger');
				$("#btn-review-cancel").click();
				
				if(response.html) {
					$('#review-list').prepend(response.html).children('.review-panel:first-child').find('.star-rating').raty({
						readOnly: true,
						score: function() {
							return $(this).attr('data-score');
						}
					});
				}
			} else {
				var alert = $('#error-box');
				var errors = '';

				$.each(response.errors, function(index, value) {
					errors += '<' + 'li>' + value + '<' + '/li>';
				});

				alert.children('ul').html(errors);
				alert.removeClass('hidden');
			}
		});
	});

	$('.star-rating').raty({
		readOnly: true,
		score: function() {
			return $(this).attr('data-score');
		}
	});

	$('#btn-load-more').click(function() {
		$(this).addClass('hidden');
		$('#review-loader').removeClass('hidden');

		$.post(baseurl + '/review/product/load', { product: product_slug, page: review_page }, function(response) {
			if(response.success) {
				$('#review-list').append(response.html).find('.star-rating').raty({
					readOnly: true,
					score: function() {
						return $(this).attr('data-score');
					}
				});

				if(review_page < review_pages - 1) {
					$('#btn-load-more').removeClass('hidden');
				}
			}

			$('#review-loader').addClass('hidden');
			review_page++;
		});
	});

	$('.rating').raty({
		scoreName: 'rating'
	});

	// check if small viewport, then disable zoom
	changezoom();
});

$(window).resize(function() {
	changezoom();
});

function showReviews() {
	var reviewtab = $('a[href="#reviews"]');

	if(!reviewtab.parent().hasClass('active')) {
		reviewtab.tab('show');
	}
}

function showEditor() {
	$('#review-comments').summernote({
		height: 200,
		toolbar: [
			['style', ['bold', 'italic', 'underline', 'clear']],
		    ['fontsize', ['fontsize']],
		    ['color', ['color']],
		    ['para', ['ul', 'ol', 'paragraph']],
		    ['height', ['height']],
		    ['fontname', ['fontname']]
		]
	});
}

function changezoom() {
	/*
	var ez = $("#img-preview").data('elevateZoom');
	
	if ($(window).width() < 600) {
  		ez.changeState('disable');
	} else {
		ez.changeState('enable');
	}
	*/
}