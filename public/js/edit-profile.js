$(document).ready(function() {
	ps = 0;
	var options = {};

    options.ui = {
    	usernameField: "#profile-firstname",
    	container: "#pw-feedback",
    	showVerdictsInsideProgressBar: true,
    	verdicts: pwlabels,
        viewports: {
            progress: ".pw-strength"
        }
    };

    options.common = {
    	minChar: 8,
    	onKeyUp: function(event, score) {
    		ps = score;
    	}
    };

    options.rules = {
    	activated: {
    		wordTwoCharacterClasses: true,
            wordRepetitions: true
    	}
    };

    $('#profile-password').pwstrength(options);
    $('.glyphicon-info-sign').tooltip();

    $.validator.addMethod('pwStrength', function(value, element, param) {
        if(value.length > 0) {
    	   return ps.score >= 25 ? true : false;
        } else {
            return true;
        }
    }, msgWeakPw);

    $.validator.addMethod('changedCredentials', function(value, element, param) {
        var email = $('#profile-email');
        var username = $('#profile-username');

        if((email.val() != email.data('current') || username.val() != username.data('current') || $('#profile-password').val() || $('#profile-confirm-password').val()) && value.length == 0) {
            return false;
        } else {
            return true;
        }
    }, msgOldPwRequired);

    $('#form-edit-profile').validate({
    	errorClass: 'label label-danger',
    	errorElement: 'div',
        errorPlacement: function(error, element) {
            var fg = $(element).closest('.form-group');
            error.appendTo(fg);
        },
    	highlight: function(element, errorClass, validClass) {
            var fg = $(element).closest('.form-group');
            var tab = $(element).closest('.tab-pane').attr('id');
		    fg.addClass('has-error').removeClass('has-success');
		    fg.children('span').remove();
		    fg.append('<span class="glyphicon glyphicon-remove form-control-feedback"' + (fg.children('.input-group').length ? 'style="right: ' + (fg.find('.input-group-addon').width() + 25) + 'px"' : '') + '></span>');
            $('a[href="#' + tab + '"]').tab('show');
        },
	  	unhighlight: function(element, errorClass, validClass) {
            var fg = $(element).closest('.form-group');
		    fg.removeClass('has-error').addClass('has-success');
		    fg.children('span').remove();
		    fg.append('<span class="glyphicon glyphicon-ok form-control-feedback"' + (fg.children('.input-group').length ? 'style="right: ' + (fg.find('.input-group-addon').width() + 25) + 'px"' : '') + '></span>');
	  	},
	  	rules: {
            address_id: {
                remote: {
                    url: baseurl + "/validate/address-exists",
                    type: "post"
                }
            },
            store_address: {
                remote: {
                    url: baseurl + "/validate/address-exists",
                    type: "post"
                }
            },
	  		email: {
	  			remote: {
	  				url: baseurl + "/validate/email-taken",
	  				type: "post",
                    data: {
                        edit: function() {
                            return '1';
                        }
                    }
	  			}
	  		},
            username: {
                remote: {
                    url: baseurl + "/validate/username-taken",
                    type: "post",
                    data: {
                        edit: function() {
                            return '1';
                        }
                    }
                }
            },
            telephone: {
                digits: true
            },
            fax: {
                digits: true
            },
            store_name: {
               remote: {
                    url: baseurl + "/validate/store-name-taken",
                    type: "post",
                    data: {
                        edit: function() {
                            return '1';
                        }
                    }
                }
            },
            store_url: {
                remote: {
                    url: baseurl + "/validate/store-url-taken",
                    type: "post",
                    data: {
                        edit: function() {
                            return '1';
                        }
                    }
                }
            },
            store_telephone: {
                digits: true
            },
            store_fax: {
                digits: true
            },
            current_password: {
                changedCredentials: true,
                remote: {
                    url: baseurl + "/validate/current-password",
                    type: "post"
                }
            },
	  		password: {
	  			pwStrength: true
	  		},
	  		password_confirmation: {
	      		equalTo: "#profile-password"
		    }
	  	},
        ignore: '[readonly=readonly]'
    });

    $('#profile-email, #profile-username, #profile-password, #profile-confirm-password').change(function() {
        var email = $('#profile-email');
        var username = $('#profile-username');

        if(email.val() != email.data('current') || username.val() != username.data('current') || $('#profile-password').val() || $('#profile-confirm-password').val()) {
            toggleCurrentPassword(true);
        } else {
            toggleCurrentPassword(false);
        }
    });

    $('#btn-unlock-store-fields').click(function() {
        $('.locked').attr('readonly', false);
        $('#profile-store-name').focus();
        $(this).attr('disabled', true).children('i').removeClass('fa-lock').addClass('fa-unlock-alt');
    });

    if($.fn.summernote) {
        $('#store-description').summernote({
            height: 200,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['fontname', ['fontname']]
            ]
        });
    }
});

function toggleCurrentPassword(enable) {
    var cp = $('#profile-current-password');
    var fg = cp.closest('.form-group');

    if(enable) {
        cp.attr('readonly', false);
    } else {
        cp.val('').attr('readonly', true);
        fg.removeClass('has-error').removeClass('has-success');
        fg.children('span').remove();
        fg.children('div.label-danger').remove();
    }
}